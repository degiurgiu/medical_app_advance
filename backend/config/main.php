<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
	'name'=>'Medical Application',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
//	'timeZone' => 'Europe/Bucharest',
    'modules' => [
//    	'ckeditor'=>__DIR__ . '/../../common/widgets/ckeditor/CKEditor.php'
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'clients' => [
            'class' => 'common\models\Clients',
            'enableAutoLogin' => true,
            'loginUrl'=>['/user/login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                'page/<slug>' => 'page/slug',
//                'blog-post/<slug>' => 'blog-post/slug',
	            '<controller:\w+>/<id:\d+>' => '<controller>/view',
	            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
	            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
	            '<controller:\w+>/<action:\w+>/<slug:[A-Za-z0-9 -_.]+>' => '<controller>/<action:\w+>/slug',
	            '<controller:\w+>/<slug:[A-Za-z0-9 -_.]+>' => '<controller>/slug',
            ],
        ],
        'HelperComponent' => [
            'class' => 'common\components\HelperComponent',
        ],
        'TwilioComponent' => [
            'class' => 'common\components\TwilioComponent',
            ['id'],
        ],


    ],
    'params' => $params,
];

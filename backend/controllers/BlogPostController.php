<?php

namespace backend\controllers;

use common\models\BlogCategory;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use Yii;
use common\models\BlogPost;
use common\models\BlogPostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * BlogPostController implements the CRUD actions for BlogPost model.
 */
class BlogPostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update','view','index','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','clientManage'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['/user/login']);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(\Yii::$app->user->can('admin')){
            //$model = Clients::find()->all();
            $model = BlogPost::find()
                ->joinWith(['author' => function (ActiveQuery $query) {
                    return $query
                        ->andWhere(['=', 'user.status', '1']);
                }])->all();
        } else {
            $model = BlogPost::find()->andWhere(['author_id'=>\Yii::$app->user->identity->getId()])->all();
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }

    /**
     * Displays a single BlogPost model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BlogPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BlogPost();
        $model->author_id = \Yii::$app->user->identity->getId();
        $old_img = $model->blog_image;

        if ($model->load(Yii::$app->request->post()) ) {

            $model->created_at = date('Y-m-d h:m:s');

            $model->blog_image = UploadedFile::getInstance($model,'blog_image');

            if (!empty($model->blog_image)) {
                @unlink($old_img);
                $model->blog_image->saveAs('uploads/' . md5(time()) . '.' . $model->blog_image->extension);
                $model->blog_image = 'uploads/' . md5(time()). '.' . $model->blog_image->extension;
            }else {
                $model->blog_image = $old_img;
            }

            if ($model->save()) {

                Yii::$app->session->setFlash('success', "Blog Post Added");
            }else {
                Yii::$app->session->setFlash('error', "Blog Post error.");
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing BlogPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_img = $model->blog_image;

        if ($model->load(Yii::$app->request->post())) {

            $model->blog_image = UploadedFile::getInstance($model,'blog_image');

            if (!empty($model->blog_image)) {
                @unlink($old_img);
                $model->blog_image->saveAs('uploads/' . md5(time()) . '.' . $model->blog_image->extension);
                $model->blog_image = 'uploads/' . md5(time()). '.' . $model->blog_image->extension;
            }else {
                $model->blog_image = $old_img;
            }
            $model->updated_at = date('Y-m-d h:m:s');

            if ($model->save()) {

                Yii::$app->session->setFlash('success', "Blog Post Updated");
            }else {
                Yii::$app->session->setFlash('error', "Blog Post error.");
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionSlug($slug)
    {
        $model = BlogPost::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {

            return $this->render('view', [
                'model' => $model,
            ]);
        }
        else {
            return $this->redirect('index');
        }
    }
    /**
     * Deletes an existing BlogPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogPost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

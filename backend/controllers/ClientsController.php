<?php

namespace backend\controllers;

use common\models\ClientsHistory;
use common\models\QrClients;
use Yii;
use common\models\Clients;
use common\models\ClientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'only' => ['create', 'update','view','index','delete','create-history','qr'],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['admin','clientManage'],
			        ],
		        ],
		        'denyCallback' => function($rule, $action) {
			        return Yii::$app->response->redirect(['/user/login']);
		        },
	        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(\Yii::$app->user->can('admin')){
            //$model = Clients::find()->all();
	        $model = Clients::find()
		        ->joinWith(['user' => function (ActiveQuery $query) {
			        return $query
				        ->andWhere(['=', 'user.status', '1']);
		        }])->all();
        } else {
            $model = Clients::find()->andWhere(['user_id'=>\Yii::$app->user->identity->getId()])->all();
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	    $searchModel = new ClientsSearch();

	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	    $dataProvider->query->andWhere(['uniqid'=>$id]);
        if ($dataProvider->totalCount > 0) {

             return $this->render('view', [
                 'model' => Clients::find()->andWhere(['uniqid' => $id])->One(),
                 'dataProvider' => $dataProvider,
             ]);
         }
         else {
             return $this->render('view');
         }

    }
	/**
	 * Displays a single Clients model.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionQr($id)
	{
		$searchModel = new ClientsSearch();

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->andWhere(['uniqid'=>$id]);
		return $this->render('view', [
			'model' =>  Clients::find()->andWhere(['uniqid'=>$id])->One(),
			'dataProvider' => $dataProvider,
		]);
	}

    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clients();
        $modelsClientsHistory = [new ClientsHistory];
	    $modelQrClients = new QrClients();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->load(Yii::$app->request->post())) {


                $modelsClientsHistory = Model::createMultiple(ClientsHistory::classname());

                Model::loadMultiple($modelsClientsHistory, Yii::$app->request->post());

                // validate all models
                $valid = $model->validate();
	            $valid = $modelQrClients->validate() && $valid;
                $valid = Model::validateMultiple($modelsClientsHistory) && $valid;
	            $model->uniqid = uniqid("",true);
	            $model->created_at = date('Y-m-d h:m:s');
                if ($valid) {

                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                        $qr = $model->creatQrCode($model);
                        if($qr) {
	                        $modelQrClients->client_id = $model->id;
	                        $modelQrClients->qr_client_path = 'uploads/qr_codes_patients/' . md5($model->id) . '_' . $model->id . '.png';
	                        $modelQrClients->created_at = date('Y-m-d h:m:s');
	                        $flag = $modelQrClients->save();
                        }
                            foreach ($modelsClientsHistory as $modelHistory) {
                                $modelHistory->client_id = $model->id;
                                if (! ($flag = $modelHistory->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
        }else {

            return $this->render('create', [
                'model' => $model,
                'modelsClientsHistory' => (empty($modelsClientsHistory)) ? [new ClientsHistory] : $modelsClientsHistory,
            ]);
        }
    }

    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//        print_r($this);
        $modelsClientsHistory = $model->clientHistory;
	    $modelQrClients = $model->qrClients;

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsClientsHistory, 'id', 'id');
            $modelsClientsHistory = Model::createMultiple(ClientsHistory::classname(), $modelsClientsHistory);
            Model::loadMultiple($modelsClientsHistory, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsClientsHistory, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
	        $valid = $modelQrClients->validate() && $valid;
            $valid = Model::validateMultiple($modelsClientsHistory) && $valid;
	        $model->updated_at = date('Y-m-d h:m:s');
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
	                    $qr = $model->creatQrCode($model);
	                    if($qr) {
		                    $modelQrClients->qr_client_path = 'uploads/qr_codes_patients/' . md5($model->id) . '_' . $model->id . '.png';
		                    $modelQrClients->updated_at = date('Y-m-d h:m:s');
		                    $flag = $modelQrClients->save(true);
	                    }
                        if (! empty($deletedIDs)) {
                            ClientsHistory::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsClientsHistory as $modelHistory) {
                            $modelHistory->client_id = $model->id;
                            if (! ($flag = $modelHistory->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsClientsHistory' => (empty($modelsClientsHistory)) ? [new ClientsHistory] : $modelsClientsHistory
        ]);
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::find()->andWhere(['uniqid' => $id])->One()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

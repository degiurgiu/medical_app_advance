<?php

namespace backend\controllers;

use Yii;
use common\models\ClientsHistory;
use common\models\ClientsHistorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Clients;

/**
 * ClientsHistoryController implements the CRUD actions for ClientsHistory model.
 */
class ClientsHistoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
	    return [
		    'access' => [
			    'class' => AccessControl::className(),
			    'only' => ['create', 'update','view','index','delete'],
			    'rules' => [
				    [
					    'allow' => true,
					    'roles' => ['admin','clientManage'],
				    ],
			    ],
			    'denyCallback' => function($rule, $action) {
				    return Yii::$app->response->redirect(['/user/login']);
			    },
		    ],
		    'verbs' => [
			    'class' => VerbFilter::className(),
			    'actions' => [
				    'delete' => ['POST'],
			    ],
		    ],
	    ];
    }

    /**
     * Lists all ClientsHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientsHistory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientsHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ClientsHistory();

	    if ($model->load(Yii::$app->request->post()) ) {
		    $model->client_id =$id;
            $model->created_at = date('Y-m-d h:m:s');
		    $model->save();
            $id_redirect = Clients::find($id)->andWhere(['id' => $id])->One();
            return $this->redirect(['/clients/view?id='.$id_redirect->uniqid]);
	    }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClientsHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $id_redirect = Clients::find()->andWhere(['id' => $model->client_id])->One();
        if ($model->load(Yii::$app->request->post()) ) {

            $model->updated_at = date('Y-m-d h:m:s');
            if ($model->save()) {
                return $this->redirect(['/clients/view?id=' . $id_redirect->uniqid]);
            }

        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClientsHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   $model = $this->findModel($id);
        $id_redirect = Clients::find()->andWhere(['id' => $model->client_id])->One();
        if($model->delete()) {

            return $this->redirect(['/clients/view?id=' . $id_redirect->uniqid]);
        }
    }

    /**
     * Finds the ClientsHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientsHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientsHistory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

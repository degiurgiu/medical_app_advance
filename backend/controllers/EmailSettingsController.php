<?php

namespace backend\controllers;

use Yii;
use common\models\EmailSettings;
use common\models\EmailSettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * EmailSettingsController implements the CRUD actions for EmailSettings model.
 */
class EmailSettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update','view','index','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','clientManage'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['/user/login']);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new EmailSettingsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//        $email_settings =EmailSettings::find()->all();
        $email_settings =EmailSettings::find()->andWhere(['user_id'=>\Yii::$app->user->identity->getId()])->all();
        return $this->render('index', [
            'email_settings' => $email_settings,
        ]);
    }

    /**
     * Displays a single EmailSettings model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailSettings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailSettings();
        $user_id =\Yii::$app->user->identity->getId();
        if ($model->load(Yii::$app->request->post())) {
//            return $this->redirect(['view', 'id' => $model->id]);
            $model->user_id =$user_id;
            $model->save();
            return $this->redirect(['index']);
        }
/*if ($model->load(Yii::$app->request->post()) ) {
		    $model->client_id =$id;
		    $model->save();
		    return $this->redirect(['clients/index']);
	    }*/
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmailSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmailSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailSettings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailSettings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

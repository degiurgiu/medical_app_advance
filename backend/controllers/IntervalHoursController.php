<?php

namespace backend\controllers;

use Yii;
use common\models\IntervalHours;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IntervalHoursController implements the CRUD actions for IntervalHours model.
 */
class IntervalHoursController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IntervalHours models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$interval_houres =IntervalHours::find()->andWhere(['user_interval_id' => \Yii::$app->user->identity->getId()])->all();
        return $this->render('index', [
            'interval_houres' => $interval_houres,
        ]);
    }

    /**
     * Displays a single IntervalHours model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IntervalHours model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IntervalHours();

	    if ($model->load(Yii::$app->request->post())){
		    $model->user_interval_id= \Yii::$app->user->identity->getId();
		    $model->created_at = date('Y-m-d h:m:s');
		    $model->save();
//		    return $this->redirect(['view', 'id' => $model->id]);
		    return $this->redirect(['index']);
	    }

	    return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IntervalHours model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	    $model->updated_at = date('Y-m-d h:m:s');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IntervalHours model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IntervalHours model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IntervalHours the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntervalHours::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

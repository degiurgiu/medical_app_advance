<?php

namespace backend\controllers;

use Yii;
use common\models\Page;
use common\models\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model= Page::find()->all();
        return $this->render('index', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post())) {
	        $model->createdAt = date('Y-m-d h:m:s');
        	if($model->save()) {
		        return $this->redirect(['index']);
		        // return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
	        $model->updatedAt = date('Y-m-d h:m:s');
        	if($model->save()) {
		        return $this->redirect(['index']);
		        // return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

	public function actionSlug($slug)
	{
		$model = Page::find()->where(['slug'=>$slug])->one();

		if (!is_null($model) && $model->status == '1') {
			if($model->page_type == 'full_content_page'){
				return $this->render('page', [
					'model' => $model,
				]);
			}elseif ($model->page_type == 'right_sidebar'){
				return $this->render('right_page', [
					'model' => $model,
				]);
			}
			elseif ($model->page_type == 'left_sidebar'){
				return $this->render('left_page', [
					'model' => $model,
				]);
			}
			elseif ($model->page_type == 'left_right_sidebar'){
				return $this->render('left_right_page', [
					'model' => $model,
				]);
			}
		} else {
			return $this->render('page_not_found', [
				'model' => $model,
			]);
		}
	}
}

<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use common\models\ContactForm;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\Clients;
use common\models\Appointment;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout',],
                'rules' => [
                    [
                        'actions' => ['logout','language'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//    	\Yii::info(User::findByUsername('edward'),'my_category');
//	    \Yii::$app->language = \Yii::$app->getRequest()->getCookies()->getValue('lang');

        if (!Yii::$app->user->isGuest){
            $user_id =\Yii::$app->user->identity->getId();
            $appointment_data = Appointment::find()->where(['user_appointment_id'=>$user_id])->all();
            $client_data = Clients::find()->where(['user_id'=>$user_id])->all();
            return $this->render('analitycs',['appointment_data'=>$appointment_data,'client_data'=>$client_data]);
        }
        return $this->render('index');
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    public function actionLanguage(){
//    	if(isset($_POST['lang'])){
//    		Yii::$app->language = $_POST['lang'];
//    		$cookie = new yii\web\Cookie([
//    			'name'=>'lang',
//			    'value'=>$_POST['lang']
//		    ]);
//    		Yii::$app->getResponse()->getCookies()->add($cookie);
//	    }
//    }
}

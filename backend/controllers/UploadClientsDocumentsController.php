<?php

namespace backend\controllers;

use Yii;
use common\models\UploadClientsDocuments;
use common\models\UploadClientsDocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
/**
 * UploadClientsDocumentsController implements the CRUD actions for UploadClientsDocuments model.
 */
class UploadClientsDocumentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'only' => ['create', 'update','view','index','delete','list-client-documents'],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['admin','clientManage'],
			        ],
		        ],
		        'denyCallback' => function($rule, $action) {
			        return Yii::$app->response->redirect(['/user/login']);
		        },
	        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UploadClientsDocuments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UploadClientsDocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UploadClientsDocuments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UploadClientsDocuments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new UploadClientsDocuments();
	    $model->scenario = 'create';
	    $old_doc = $model->document;
	    $model->client_documents_id = $id;
        if ($model->load(Yii::$app->request->post())) {

	        $model->document = UploadedFile::getInstance($model,'document');
			//$model->clientDocuments->first_name;
	        if (!empty($model->document)) {

		        $model->document->saveAs('uploads/client_documents/'. md5(time()).'_'.$model->client_documents_id . '.' . $model->document->extension);
		        $model->document = 'uploads/client_documents/' . md5(time()).'_'.$model->client_documents_id . '.' . $model->document->extension;
	        }else {
		        $model->document = $old_doc;
	        }
	        $model->save();

            return $this->redirect(['clients/index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UploadClientsDocuments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	    $old_doc = $model->document;
//	    $model->client_documents_id = $id;
        if ($model->load(Yii::$app->request->post())) {

	        $model->document = UploadedFile::getInstance($model,'document');
	        if (!empty($model->document)) {
		        @unlink($old_doc);
                $model->document->saveAs('uploads/client_documents/'. md5(time()).'_'.$model->client_documents_id . '.' . $model->document->extension);
                $model->document = 'uploads/client_documents/'. md5(time()).'_'.$model->client_documents_id . '.' . $model->document->extension;
	        }else {
               $model->document = $old_doc;
           }

           if ($model->save()) {
	           return $this->redirect(['clients/index']);
           }
		}


            //return $this->redirect(['view', 'id' => $model->id]);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UploadClientsDocuments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $delete =$this->findModel($id);
	    @unlink($delete->document);
	    $delete->delete();

	    return $this->redirect(['clients/index']);
    }

    /**
     * Finds the UploadClientsDocuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UploadClientsDocuments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UploadClientsDocuments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	public function actionDownload($path)
	{ //  $path= Yii::getAlias('@webroot').$path;
		//if(file_exists($path)) {
		Yii::$app->session->setFlash('success', "File Downloaded");
			return \Yii::$app->response->SendFile($path);
		//}
//		else{
//			return Yii::$app->session->setFlash('error', "File not Exist.");
//		}
	}
	public function actionListClientDocuments($id)
	{
//		$business_hours_object = BusinessHours::find()->andWhere(['user_business_id' => \Yii::$app->user->identity->getId()])->all();
		$documents_list = UploadClientsDocuments::find()->andWhere(['client_documents_id' => $id])->all();

		return $this->render('client_documents_list', [
			'documents_list' => $documents_list,
		]);
	}
}

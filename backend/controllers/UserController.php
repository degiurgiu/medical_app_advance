<?php

namespace backend\controllers;

use common\models\AddRoleForm;
use common\models\AuthItem;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\AuthAssignment;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\models\EmailSettings;
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','dashboard','update','delete','unlink'],
                'rules' => [
	                [
		                'actions' => ['signup','dashboard','update','delete','unlink'],
		                'allow' => true,
		                'roles' => ['admin'],
	                ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
	                'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionDashboard()
    {
	    $users=  User::find()->andWhere(['status' => '1'])->all();

	    $signup = new SignupForm();
        $email_settings = EmailSettings::find()->one();
	    if ($signup->load(Yii::$app->request->post())) {
		    if ($user = $signup->signup()) {

               if(isset($email_settings->new_user_email) && $email_settings->new_user_email == 1) {
                    $email ="giurgiu_dan94@yahoo.com";
                   $variables = array("first_name"=>$signup->first_name,"last_name"=>$signup->last_name,"email"=>$signup->email,"username"=>$signup->username,"password"=>$signup->password,"role"=>implode(',',$signup->permissions));
                   $message=Yii::$app->HelperComponent->template_substitution($email_settings->new_user_message,$variables);
                   Yii::$app->HelperComponent->sendEmail($email, $signup->email, 'You have a new account at medical app', $message);
                    Yii::$app->session->setFlash('success', "New user created successfully");
                }
			    if (Yii::$app->getUser()->login($user)) {
				    return $this->goHome();
			    }
		    }
	    }
	    $role = new AddRoleForm();
	    if ($role->load(Yii::$app->request->post())) {
		    if ($user = $role->addrole()) {
			   // if (Yii::$app->getUser()->login($role)) {
				    return $this->goHome();
			   // }
		    }
	    }
	    $roles = AuthItem::find()->all();
	    $users_deactivated =  User::find()->andWhere(['status' => '0'])->all();
        return $this->render('dashboard',['users'=>$users,'signup'=>$signup,'role'=>$role,'roles'=>$roles,'users_deactivated'=>$users_deactivated]);
    }

    public function actionUpdate($id)
    {
    	$model = $this->find($id);
	    $current_user_permissions = $model->authAssignments;
	   // $current_user_permissions =new yii\rbac\ManagerInterface::getRolesByUser($id);
	    $all_permissions = AuthItem::find()->all();

		$request = Yii::$app->request->post('User');
	    if ($request){
		    $model->updated_at = date('Y-m-d h:m:s');
		    if($request['username'] !== $model->username) {
			    $model->username = $request['username'];
		    }

            if($request['email'] !== $model->email) {
	            $model->email = $request['email'];
            }
            if(!empty($request['new_password'])) {
	            $model->setPassword($request['new_password']);
	            $model->generateAuthKey();
            }
            if(!empty($request['delete_permissions'])) {
	            foreach ($request['delete_permissions'] as $item2 => $val) {
	            	$this->actionRevoke($val,$id);
	            }
            }
		    if(!empty($request['add_permissions'])) {
			    foreach ($request['add_permissions'] as $val) {
				    $newPermission = new AuthAssignment;
				    $newPermission->user_id = $id;
				    $newPermission->item_name = $val;
				    $newPermission->save();
			    }
		    }
          if($model->save()) {
	          return $this->redirect(['dashboard']);
          }
	    }
	    return $this->render('update', [
		    'model' => $model,
		    'current_user_permissions'=>$current_user_permissions,
		    'all_permissions'=>$all_permissions,
	    ]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
	public function actionRevoke($role, $userid) {
		$auth = Yii::$app->authManager;

		$rol= $auth->getRole($role);
		if (empty($rol)) {throw new NotFoundHttpException("There is no assignment ".$role." for user ".$userid.".");}

		$res = $auth->revoke($rol, $userid);
		if (!$res) {Yii::$app->session->setFlash('danger', Yii::t("yii", "Error"));}
		return $this->redirect(['dashboard']);
		}

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	public function actionSignup()
	{
		$model = new SignupForm();
		$authItems = AuthItem::find()->all();
        $email_settings = EmailSettings::find()->one();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {

                if(isset($email_settings->new_user_email) && $email_settings->new_user_email == 1) {
                    $email ="giurgiu_dan94@yahoo.com";
                    $variables = array("first_name"=>$model->first_name,"last_name"=>$model->last_name,"email"=>$model->email,"username"=>$model->username,"password"=>$model->password,"role"=>implode(',',$model->permissions));
                    $message=Yii::$app->HelperComponent->template_substitution($email_settings->new_user_message,$variables);
                    Yii::$app->HelperComponent->sendEmail($email, $model->email, 'You have a new account at medical app', $message);
                    Yii::$app->session->setFlash('success', "New user created successfully");
                }
				if (Yii::$app->getUser()->login($user)) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
			'authItems'=>$authItems,
		]);
	}
	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} else {
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}
	/**
	 * Resets password.
	 *
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}
	public function actionDelete($id)
	{
		$this->find($id)->delete();

		return $this->redirect(['dashboard']);
	}
	public function actionUnlink($id)
	{
		$model =$this->find($id);
		$model->status = 0;
		if($model->save()) {
			return $this->redirect(['dashboard']);
		}
	}
	public function actionActivate($id)
	{
		$model =$this->find($id);
		$model->status = 1;
		if($model->save()) {
			return $this->redirect(['dashboard']);
		}
	}
    public function find($id){
        $model = User::findOne($id);
        if($model === null){
            throwException('400', 'Model not exists');
        }
        return $model;
    }
}

<?php

namespace backend\controllers;

use common\models\Appointment;
use common\models\Clients;
use common\models\User;
use common\models\UserPersonalInfo;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use yii\web\UploadedFile;

class UserProfileController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','update'],
				'rules' => [
					[
						'actions' => ['index','update'],
						'allow' => true,
						'roles' => ['admin','clientManage']
					],
				],
			],
		];
	}
    public function actionIndex()
    {
    	$user_id =\Yii::$app->user->identity->getId();

        $user_personal_info =  UserPersonalInfo::find()->where(['user_id'=>$user_id])->one();


        return $this->render('index',['model' => $user_personal_info]);
    }

	public function actionAnalitycs()
	{
		$user_id =\Yii::$app->user->identity->getId();
		$appointment_data = Appointment::find()->where(['user_appointment_id'=>$user_id])->all();
		$client_data = Clients::find()->where(['user_id'=>$user_id])->all();

		return $this->render('analitycs',['appointment_data'=>$appointment_data,'client_data'=>$client_data]);
	}

	public function actionUpdate($id)
	{
		$model =  $this->find($id);
		$old_img = $model->avatar;
		$user_id = $model->user->id;
		if ($model->load(Yii::$app->request->post())){

            $model->avatar = UploadedFile::getInstance($model,'avatar');

           if (!empty($model->avatar)) {
	           @unlink($old_img);
                $model->avatar->saveAs('uploads/' . md5(time()) . '.' . $model->avatar->extension);
                $model->avatar = 'uploads/' . md5(time()). '.' . $model->avatar->extension;
            }else {
               $model->avatar = $old_img;
           }

           $user_email = $_POST['User']['email'];
           $user_model = User::find()->where(['id' => $user_id])->one();
           if($user_model->email !== $user_email) {
	           $user_model->email = $user_email;
	           $user_model->save();
           }
           if ($model->save()) {
	           return $this->redirect(['index']);
           }
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function find($id){
		$model = UserPersonalInfo::findOne($id);
		if($model === null){
			throwException('400', 'Model not exists');
		}
		return $model;
	}



}

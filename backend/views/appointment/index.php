<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Appointments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php
        Modal::begin([
            'header'=>'<h4>'.Yii::t('app','Appointment').'</h4>',
            'id'=>'create_appointment',
            'size'=>'modal-lg',
        ]);
        echo "<div id='modalContentCreateAppointment'></div>";
        Modal::end();
	?>
    <div class="">
        <div class="card card-body">
			<?php
			$gridColumns = [
				['class' => 'yii\grid\SerialColumn'],
				'first_name',
				'last_name',
				'phone',
				'email:email',
				'appointment_date_time',
				'user_appointment_ip',
				'privacy_policy_gdpr_agreement',
				'created_at',
				['class' => 'yii\grid\ActionColumn'],
			];
			// Renders a export dropdown menu
			echo ExportMenu::widget([
				'dataProvider' => $dataProvider,
				'columns' => $gridColumns
			]);

			//You can choose to render your own GridView separately
			echo \kartik\grid\GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => $gridColumns
			]);
			?>
        </div>
    </div>

</div>

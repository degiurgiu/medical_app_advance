<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\export\ExportMenu;
use common\models\UserPersonalInfo;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model common\models\Appointment */

//$this->title = $model->id;
$this->title = 'Appointments';
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$user_personal_info = UserPersonalInfo::find()->where(['user_id' => $userId])->one();


Modal::begin([
	'header'=>'<h4>Appointment</h4>',
	'id'=>'create_appointment',
	'size'=>'modal-lg',
]);
echo "<div id='modalContentCreateAppointment'></div>";
Modal::end();

Modal::begin([
	'header'=>'<h4>View Data</h4>',
	'id'=>'view_appointment',
	'size'=>'modal-lg',
]);
echo "<div id='modalContentViewAppointment'></div>";
Modal::end();
?>

<h1> <?php echo $this->title; ?> </h1>
<h2> Appotment for <?php echo $user_personal_info['first_name'] .' '. $user_personal_info['last_name']; ?></h2>
<div class="appointment-view">
    <button class="btn btn-primary" type="button" data-toggle="collapse"  data-target="#exportClients" aria-expanded="false" aria-controls="collapseExample">
        Export Appointment
    </button>
    <div class="collapse" id="exportClients">
        <div class="card card-body">
			<?php
			$gridColumns = [
				['class' => 'yii\grid\SerialColumn'],
				'first_name',
				'last_name',
				'phone',
				'email:email',
				'appointment_date_time',
				'user_appointment_ip',
				'privacy_policy_gdpr_agreement',
				'user_appointment_id',
				'created_at',
				['class' => 'yii\grid\ActionColumn'],
			];
			// Renders a export dropdown menu
			echo ExportMenu::widget([
				'dataProvider' => $dataProvider,
				'columns' => $gridColumns
			]);

			//You can choose to render your own GridView separately
			echo \kartik\grid\GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => $gridColumns
			]);
			?>
        </div>
    </div>
	<?= \common\widgets\FullCalendarCustom::widget(array(
		'id'=>'appointment_calendar',
		'contentHeight'=>750,
		'eventLimit'=>true,
		'header'=>[
			'center'=>'title',
			'left'=>'prev,next today',
			'right'=>'agendaWeek'
		],
		'options' => [
			'lang' => 'en',
			'weekends' => true,
			'constraint'=>['id'=>$userId],
		],
        "eventRender"=>" function (event, element) {
          element
            .attr('title', 'Name: '+event.nonstandard.first_name+' '+event.nonstandard.first_name+''+' Phone: '+event.nonstandard.phone)
            .attr('data-placement', 'bottom')
            .attr('data-toggle', 'tooltip');
        }",
		'eventLimitText'=>'',
        'slotDuration'=>$interval,
		'eventClick' => <<<EOS
            function(calEvent, jsEvent, view) {

            var html='<div class="col-sm-12" style="float: none">'+
            '<div class="buttons-control col-sm-12">'+
                ' <a class="btn btn-primary" href="/appointment/update?id='+calEvent.id+'">UPDATE</a>'+
                ' <a class="btn btn-danger" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" href="/appointment/delete?id='+calEvent.id+'"  data-method="post">Delete</a>'+
            '</div>'
                +'<br><div class="info-appointment col-sm-12" style="float: none;">'+
                '<p><b>First Name: </b>'+calEvent.nonstandard.first_name+'</p>'+
                '<p><b>Last Name: </b>'+calEvent.nonstandard.last_name+'</p>'+
                '<p><b>Phone: </b>'+calEvent.nonstandard.phone+'</p>'+
                '<p><b>Email: </b>'+calEvent.nonstandard.email+'</p>'+
                '<p><b>Appointment Date: </b>'+calEvent.nonstandard.appointment_date+'</p>'+
                '</div>'+
               ' </div>';
              $('body').find('#view_appointment').modal('show').find('#modalContentViewAppointment').empty().append(html);
                $(this).css('border-color', 'red');
        
             }
EOS
        ,
		"dayClick"=>"function(date, jsEvent, view) {
         var constraint = $(this).parents(\"#appointment_calendar\").attr('constraint');
        if (jsEvent.target.classList.contains('fc-nonbusiness')) {
                return;   
            }
            else{
                var pars=JSON.parse(constraint);
//               console.log(pars.id);
//                console.log(date.format());
                $.get('/appointment/create',{'date':date.format(),'userid':pars.id},function (data) {
                    $('#create_appointment').modal('show').find('#modalContentCreateAppointment').html(data);
        
                });
              
            }
     }",
    'businessHours'=> $business_hours,
    'events'=> $events,
	));
	?>
</div>

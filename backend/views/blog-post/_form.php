<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\ckeditor\CKEditor;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-form">

    <?php $form = ActiveForm::begin(
    [ 'id'=>'user-blog-post-form',
        'enableClientValidation' => true,
        'options' => [
            'enctype'=>'multipart/form-data',
            'validateOnBlur' => true,
            'class' => 'form'
        ]]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>

    <?php echo $form->field($model, 'status')->dropDownList($model->getStatusPublish());?>

    <?php echo $form->field($model, 'header_type')->dropDownList($model->getHeaderType());?>

    <?= $form->field($model, 'blog_video_link')->textInput(['maxlength' => true]) ?>

    <img src="<?php if(isset($model->blog_image)){ echo Url::base(true).'/'.$model->blog_image; }?>" width="120px" height="120px"">
    <?= $form->field($model, 'blog_image')->fileInput();  ?>

    <?= $form->field($model, 'author_id')->textInput() ?>

    <?php echo $form->field($model, 'category_id')->dropDownList($model->getCategories(),['prompt'=>'Select...'])?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

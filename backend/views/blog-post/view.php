<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\models\UserPersonalInfo;
/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blog Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="blog-post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            'author_id',
            'header_type',
            'slug',
            'status',
            'blog_image',
            'blog_video_link',
            'category_id',
            'updated_at',
            'created_at',
        ],
    ]) ?>
    <?php

    echo '<br>';
     $user_info = $model->author->personalInfo;
    $full_name = $user_info['first_name'].' '.$user_info['last_name'];
    echo 'BY:'. $full_name;
    echo '<br>';
    echo '<br>';
    if(isset($model->category)) {
        echo 'CATEGORY:' . $model->category->category;
    } else {
        echo 'CATEGORY: Unknown';
    }

    ?>
    <img src="<?php if(isset($model->blog_image)){ echo Url::base(true).'/'.$model->blog_image; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" width="950px" height="450px" style=" margin: 0 auto;
                display: block;
            ">
    <br/>
</div>

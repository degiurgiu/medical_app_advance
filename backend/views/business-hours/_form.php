<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model common\models\BusinessHours */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="business-hours-form">

    <?php $form = ActiveForm::begin([ 'id'=>'business-hours-form',
	    'enableClientValidation' => true,
	    'options' => [
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ]]); ?>

    <?php echo $form->field($model, 'dow')->dropDownList(\common\models\BusinessHours::getDay())?>

    <?= $form->field($model, 'start')->widget(TimePicker::classname(),['pluginOptions' => ['showMeridian' => false,'showSeconds'=>false]]);  ?>

    <?= $form->field($model, 'end')->widget(TimePicker::classname(), ['pluginOptions' => ['showMeridian' => false,'showSeconds'=>false]]);  ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

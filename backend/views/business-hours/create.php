<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BusinessHours */

$this->title = 'Create Business Hours';
$this->params['breadcrumbs'][] = ['label' => 'Business Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-hours-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

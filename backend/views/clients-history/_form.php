<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\ClientsHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-history-form">

    <?php $form = ActiveForm::begin([
	    'id' => 'clients-history-form',
	    'enableClientValidation' => true,
	    'options' => [
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ],
    ]); ?>

    <?= $form->field($model, 'intervention')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observation')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(),[
            'inline'=>false,
	        'options' => ['class' => 'form-control'],
            'clientOptions'=>[
                    'autoclose'=>true,
	            'format'=>'dd-mm-yy',
	            'changeMonth'=>true,
	            'changeYear'=> true,
	            'defaultDate' => date('d-m-y'),
	            'value' => date('d-m-y'),
            ],
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

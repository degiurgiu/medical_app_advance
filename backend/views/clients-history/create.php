<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientsHistory */

$this->title = 'Create Clients History';
$this->params['breadcrumbs'][] = ['label' => 'Clients Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin([
        'id'=>'client-dynamic-form',
        'enableClientValidation' => true,
	    'options' => [
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ]]); ?>
<!--    //birthday_date email address-->
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'type' => 'number']) ?>
	<?= $form->field($model, 'birthday_date')->widget(DatePicker::classname(),[
		'inline'=>false,
		'options' => ['class' => 'form-control'],
		'clientOptions'=>[
			'autoclose'=>true,
			'format'=>'dd-mm-yy',
			'autoSize'=>true,
			'yearRange' => 'c-80:c+0',
			'changeMonth'=>true,
			'changeYear'=> true,
			'defaultDate' => date('d-m-y'),
			'value' => date('d-m-y'),
		],
	]); ?>
	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'user_id')->hiddenInput(['value'=>$model->getUserID()])->label(false); ?>
	<?php
	if (\Yii::$app->user->can('admin')) {
		echo $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'), ['class' => 'form-control']);
	}
	?>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i><?php echo Yii::t('app','Client History');?></h4></div>
            <div class="panel-body">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 999, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsClientsHistory[0],
                    'formId' => 'client-dynamic-form',
                    'formFields' => [
                        'intervention',
                        'observation',
                        'date',
                    ],
                ]); ?>

                <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsClientsHistory as $i => $modelHistory): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left"><?php echo Yii::t('app','Client History');?></h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (! $modelHistory->isNewRecord) {
                                    echo Html::activeHiddenInput($modelHistory, "[{$i}]id");
                                }
                                ?>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]intervention")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]observation")->textarea(['rows' => '6','maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]date")->widget(DatePicker::classname(),[
                                            'inline'=>false,
	                                        'options' => ['class' => 'form-control picker'],
                                            'clientOptions'=>[
                                                'autoclose'=>true,
                                                'format'=>'d-m-y',
	                                            'changeMonth'=>true,
	                                            'changeYear'=> true,
                                                'defaultDate' => date('d-m-y'),
                                                'value' => date('d-m-y'),
                                            ],
                                        ]); ?>
<!--                                        --><?//= $form->field($modelHistory, "[{$i}]date")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div><!-- .row -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create'):Yii::t('app','Update'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(' 
$(function () {
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        $( ".picker" ).each(function() {
           $( this ).datepicker({
              dateFormat : "dd-mm-yy",
              changeMonth: true,
              changeYear: true
           });
      });          
    });
});
$(function () {
    $(".dynamicform_wrapper").on("afterDelete", function(e, item) {
        $( ".picker" ).each(function() {
           $( this ).removeClass("hasDatepicker").datepicker({
              dateFormat : "dd-mm-yy",
              changeMonth: true,
              changeYear: true
           });
      });          
    });
});
');
?>
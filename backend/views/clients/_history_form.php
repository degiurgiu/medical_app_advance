<?php
/**
 * Created by PhpStorm.
 * User: Edward & Denisa
 * Date: 11/13/2018
 * Time: 10:51 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\DatePicker;
?>
<?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Client History</h4></div>
            <div class="panel-body">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 999, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsClientsHistory[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'intervention',
                        'observation',
                        'date',
                    ],
                ]); ?>

                <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsClientsHistory as $i => $modelHistory): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Client History</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (! $modelHistory->isNewRecord) {
                                    echo Html::activeHiddenInput($modelHistory, "[{$i}]id");
                                }
                                ?>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]intervention")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]observation")->textarea(['rows' => '6','maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= $form->field($modelHistory, "[{$i}]date")->widget(DatePicker::classname(),[
                                            'inline'=>false,
                                            'options' => ['class' => 'form-control picker'],
                                            'clientOptions'=>[
                                                'autoclose'=>true,
                                                'format'=>'d-m-y',
                                                'changeMonth'=>true,
                                                'changeYear'=> true,
                                                'defaultDate' => date('d-m-y'),
                                                'value' => date('d-m-y'),
                                            ],
                                        ]); ?>
                                        <!--                                        --><?//= $form->field($modelHistory, "[{$i}]date")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div><!-- .row -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create':'Update', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Clients', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'first_name',
            'last_name',
            'phone',
            'uniqid',
            'created_at:datetime',
	        [
		        'label'=>'Clients History',
		        'format'=>'html',
               // 'filterRowOptions'=>['class'=>'history_client'],
		        'value'=>function ($data) {
			        $str = '';
			        $i=1;
			        foreach($data->clientHistory as $request) {
				        $str .='<div class="history_client">';
				        $str .= '<b>'.$i.' </b><span><span><b>Investigation:</b>' .  $request->intervention.' </span><br> <b>Observation:</b> <span>'. $request->observation.'</span><br> 
                                <b>Date:</b> <span> '. $request->date .'</span> <br/></span><br>';
				        $str .='</div>';
				        $i++;
			        }

			        return $str;
		        },
	        ],
            [
                'label'=>'User Profile',
                'format'=>'html',
                'value'=>function ($data) {
                    $str = '';
                        $str .='<div class="history_client">';
                        $str.=Url::base('http') . '/clients/qr?id=' . $data->uniqid;
                        $str .='</div>';
//                    }

                    return $str;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ];
    ?>
        <button class="btn btn-primary" type="button" data-toggle="collapse"  data-target="#exportClients" aria-expanded="false" aria-controls="collapseExample">
            Export Clients
        </button>
        <div class="collapse" id="exportClients">
            <div class="card card-body">
            <?php
    // Renders a export dropdown menu
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns
            ]);

     //You can choose to render your own GridView separately
            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>
            </div>
        </div>
</div>
<div>
    <div>
    <?php
        foreach ($model as $item) {
	        echo  Html::a('View', ['view', 'id' => $item->uniqid], ['class' => 'btn btn-primary']);
            echo  Html::a('Update', ['update', 'id' => $item->uniqid], ['class' => 'btn btn-primary']);
            echo Html::a('Delete', ['delete', 'id' => $item->uniqid], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
	        echo '<br>';
	        echo '<br>';
	        echo Html::button('Add Documents', ['value' => Url::to('/upload-clients-documents/create?id=' . $item->id), 'class' => 'btn btn-primary documents_create',]);
	        echo Html::a('Documents List', ['/upload-clients-documents/list-client-documents', 'id' => $item->id], [
		        'class' => 'btn btn-primary',
		        'data' => [
			        'method' => 'post',
		        ]
            ]);
	        echo '<br>';
	        echo '<br>';
             if (\Yii::$app->user->can('admin')) {
	             echo "Client ID " . $item->id;
	             echo '<br>';
	             echo "Created by " . $item->user->username;
	             echo '<br>';
             }

            echo "First Name " . $item->first_name;
            echo '<br>';
            echo "Last Name " . $item->last_name;
            echo '<br>';
            echo "Phone " . $item->phone;
            echo '<br>';
	        echo "Created at " . $item->created_at;
	        echo '<br>';
            echo "CLIENT";
            echo '<br>';
            echo $item->uniqid;
	        echo '<br>';
            echo  Url::base('http') . '/clients/qr?id=' . $item->uniqid;
	        echo '<br>';
            //print_r($item->qrClients);
            echo $item->qrClients->qr_client_path;
	        echo '<br>';
	      ?>
<!--            <img src="--><?php //if(isset($item->qrCode)){ echo Url::base(true).'/'.$item->qrCode; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?><!--" width="250px" height="250px" style="-->
<!--                display: block;-->
<!--            ">-->
            <img src="<?php if(isset($item->qrClients->qr_client_path)){ echo Url::base(true).'/'.$item->qrClients->qr_client_path; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" width="150px" height="150px" style="
                display: block;
            ">
            <?php
	        echo '<br>';
            echo '<br>';


	        echo '<br>';

	        echo '<br>';
	        echo '<br>';
    }
    ?>

</div>
<?php
    Modal::begin([
            'header'=>'<h4>Update client history</h4>',
            'id'=>'modalHistory',
            'size'=>'modal-lg',
    ]);
    echo "<div id='modalContentHistory'></div>";
    Modal::end();

    Modal::begin([
        'header'=>'<h4>Documents client</h4>',
        'id'=>'modalDocuments',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modalContentDocuments'></div>";
    Modal::end();
?>
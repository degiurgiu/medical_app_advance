<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $model common\models\Clients */

$this->title = $model->first_name.''.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        .dasboardtable th, .dasboardtable td {
            border: 1px solid #AAAAAA;
            padding: 10px;
        }
    </style>
<div class="clients-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'first_name',
            'last_name',
            'phone',
            'created_at:datetime',
	        [
		        'label'=>'Clients History',
		        'format'=>'html',
               // 'filterRowOptions'=>['class'=>'history_client'],
		        'value'=>function ($data) {
			        $str = '';
			        $i=1;
			        foreach($data->clientHistory as $request) {
				        $str .='<div class="history_client">';
				        $str .= '<b>'.$i.' </b><span><span><b>Investigation:</b>' .  $request->intervention.' </span><br> <b>Observation:</b> <span>'. $request->observation.'</span><br> 
                                <b>Date:</b> <span> '. $request->date .'</span> <br/></span><br>';
				        $str .='</div>';
				        $i++;
			        }

			        return $str;
		        },
	        ],
            [
                'label'=>'User Profile',
                'format'=>'html',
                'value'=>function ($data) {
                    $str = '';
                    $str .='<div class="history_client">';
                    $str.=Url::base('http') . '/clients/qr?id=' . $data->uniqid;
                    $str .='</div>';
//                    }

                    return $str;
                },
            ],
//            ['class' => 'yii\grid\ActionColumn'],
        ];
        //You can choose to render your own GridView separately
    ?>
    <?php 	echo Html::a('Back', ['clients/index'], ['class' => 'btn  btn-primary',]); ?>
        <button class="btn btn-primary" type="button" data-toggle="collapse"  data-target="#exportClients" aria-expanded="false" aria-controls="collapseExample">
            Export Clients
        </button>
        <div class="collapse" id="exportClients">
            <div class="card card-body">
            <?php
    // Renders a export dropdown menu
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns
            ]);
            ?>
            </div>
	        <?php
	        echo \kartik\grid\GridView::widget([
		        'dataProvider' => $dataProvider,
		        //        'filterModel' => $searchModel,
		        'columns' => $gridColumns
	        ]);
	        ?>
        </div>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	        'id',
	        'first_name',
	        'last_name',
	        'phone',
	        'birthday_date',
	        'email',
	        'address',
	        'user_id',
	        'created_at',
        ],
    ]) ?>
   <?php
   echo Html::button('Details History',['class'=>'btn btn-primary','data-toggle'=>'collapse','data-target'=>'.details-history-'.$model->id,'aria-expanded'=>'false','aria-controls'=>'collapseHistory']);
   ?>
    <div class="row collapse details-history-<?php echo $model->id ?>">
        <?php echo Html::button('Create', ['value' => Url::to('/clients-history/create?id=' . $model->id), 'class' => 'btn btn-primary history_create',]); ?>
        <table class="dasboardtable">
            <tr>
                <th>NO</th>
                <th>Intervention</th>
                <th>Observation</th>
                <th>Date</th>
                <?php  if(\Yii::$app->user->can('admin')) {?>
                    <th>History ID</th>
                    <th>Client ID</th>
                <?php } ?>
                <th></th>
                <th></th>
            </tr>
            <?php
        if(!empty($model->clientHistory)) {
            $i = 1;
            foreach ($model->clientHistory as $history) {
                ?>


                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo substr($history->intervention,0,25); ?></td>
                    <td><?php echo substr($history->observation,0,25);?></td>
                    <td><?php echo $history->date; ?></td>
                    <?php  if(\Yii::$app->user->can('admin')) {?>
                        <td><?php echo $history->id ?></td>
                        <td><?php echo $history->client_id; ?></td>
                    <?php } ?>
                    <td><?php echo Html::button('Update', ['value' => Url::to('/clients-history/update?id=' . $history->id), 'class' => 'btn btn-primary history_update',]); ?></td>
                    <td>
                        <?php  echo Html::a('Delete', ['/clients-history/delete', 'id' => $history->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);?></td>
                    </tr>

                <?php
                $i++;
            }
        }
            ?>
        </table>
    </div>
    </div>

</div>
<?php
Modal::begin([
	'header'=>'<h4>Update client history</h4>',
	'id'=>'modalHistory',
	'size'=>'modal-lg',
]);
echo "<div id='modalContentHistory'></div>";
Modal::end();
?>
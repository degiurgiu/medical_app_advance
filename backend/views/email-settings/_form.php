<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\EmailSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-settings-form">


    <?php $form = ActiveForm::begin(); ?>
    <?php echo "first_name, last_name, phone_client, date, doctor_first_name, doctor_last_name "; ?>
    <?= $form->field($model, 'appointment_message_clients')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>
    <?php echo "first_name, last_name, phone_client, date, doctor_first_name, doctor_last_name "; ?>
    <?= $form->field($model, 'new_user_message')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>
    <?php echo "first_name, last_name, phone_client, date, doctor_first_name, doctor_last_name "; ?>
    <?= $form->field($model, 'email_user_appointment_message')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>
    <?= $form->field($model, 'new_user_email')->checkbox() ?>

    <?= $form->field($model, 'appointment_email_clients')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

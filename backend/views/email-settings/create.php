<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmailSettings */

$this->title = 'Create Email Settings';
$this->params['breadcrumbs'][] = ['label' => 'Email Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

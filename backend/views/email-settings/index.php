<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EmailSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  if(!count($email_settings) >= 1){ echo Html::a('Create Email Settings', ['create'], ['class' => 'btn btn-success']);} ?>
    <?php foreach ($email_settings as $item) {
        echo Html::a('Update', ['update', 'id' => $item->id], ['class' => 'btn btn-primary']);
        echo '<br>';
        echo Html::a('Delete', ['delete', 'id' => $item->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]);
        echo '<br>';
        echo $item->appointment_message_clients;
        echo '<br>';
        echo '<br>';
        echo $item->new_user_message;
        echo '<br>';
        echo '<br>';
        echo ' New User Email ';
        echo $item->new_user_email;
        echo '<br>';
        echo '<br>';
        echo 'Appointment Email Clients ';
        echo $item->appointment_email_clients;
        echo '<br>';
        // var_dump($item);
    }
    //    var_dump($interval_houres);
    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model common\models\IntervalHours */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="interval-hours-form">

    <?php $form = ActiveForm::begin( [ 'id'=>'interval-hours-form',
	    'enableClientValidation' => true,
	    'options' => [
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ]]); ?>

	<?= $form->field($model, 'interval_hours')->widget(TimePicker::classname(),['pluginOptions' => ['showMeridian' => false,'showSeconds'=>false]]);  ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

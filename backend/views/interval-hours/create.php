<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IntervalHours */

$this->title = 'Create Interval Hours';
$this->params['breadcrumbs'][] = ['label' => 'Interval Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interval-hours-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Interval Hours';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interval-hours-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<?php  if(!count($interval_houres) >= 1){ echo Html::a('Create Interval Hours', ['create'], ['class' => 'btn btn-success']);} ?>
    <?php foreach ($interval_houres as $item) {
	    echo Html::a('Update', ['update', 'id' => $item->id], ['class' => 'btn btn-primary']);
	    echo '<br>';
	    echo Html::a('Delete', ['delete', 'id' => $item->id], [
		    'class' => 'btn btn-danger',
		    'data' => [
			    'confirm' => 'Are you sure you want to delete this item?',
			    'method' => 'post',
		    ],
	    ]);
	    echo '<br>';
	    echo $item->interval_hours;
	    echo '<br>';
	    echo "Created by " . $item->userInterval->username;
	    echo '<br>';
	    echo "Updated at " . $item->updated_at;
	    echo '<br>';
	    echo "Created at " . $item->created_at;
	    echo '<br>';
	   // var_dump($item);
    }
//    var_dump($interval_houres);
    ?>
</div>

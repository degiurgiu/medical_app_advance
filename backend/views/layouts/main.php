<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AppAsset;
use common\models\UserPersonalInfo;

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
$bundle = yiister\gentelella\assets\Asset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php
if(isset(\Yii::$app->user->identity)) {
	$user_id = \Yii::$app->user->identity->getId();
	$user_personal_info = UserPersonalInfo::find()->where(['user_id' => $user_id])->one();
//	print_r($user_personal_info);
}
?>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">
        <?php
        $id = Yii::$app->user->getId();

//        $userId = UserPersonalInfo::find()->select('unique')->where(['id'=>$id])->one()['unique'];
//        var_dump(Yii::$app->user->identity);
if (!Yii::$app->user->isGuest) {?>
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?php Yii::$app->homeUrl; ?>" class="site_title">
<!--                        <i class="fa fa-paw"></i> -->
                        <span><?php echo Yii::$app->name; ?></span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="<?php if(isset($user_personal_info->avatar)){ echo Url::base(true).'/'.$user_personal_info->avatar; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2><?php echo $user_personal_info['first_name'] .' '. $user_personal_info['last_name']; ?></h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
						<?php
                        if(isset(Yii::$app->authManager->getRolesByUser($id)['admin'])) {
                         echo \yiister\gentelella\widgets\Menu::widget(
                                [
                                    "items" => [
                                        ["label" => "Home", "url" => ['/site/index'], "icon" => "home"],
                                        ['label' => 'Dashboard', 'url' => ['/user/dashboard'], 'icon' => 'tachometer'],
                                        ['label' => 'Clients', 'url' => ['/clients'], 'icon' => 'male'],
                                        ['label' => 'Signup', 'url' => ['/user/signup'], 'icon' => 'pencil-square-o'],
                                        ['label' => 'Profile', 'url' => ['/user-profile/index'], 'icon' => 'user-circle'],
                                        ['label' => 'Analitycs', 'url' => ['/user-profile/analitycs'], 'icon' => 'area-chart'],
                                        ['label' => 'Appointment View', 'url' => ['appointment/view', 'id' => \Yii::$app->user->identity->getId()], 'icon' => 'calendar-check-o'],
                                        ['label' => 'Business Houres', 'url' => ['/business-hours'], 'icon' => 'clock-o'],
                                        ['label' => 'Interval Houres', 'url' => ['/interval-hours'], 'icon' => 'clock-o'],
                                        ['label' => 'Upload Clients Documents', 'url' => ['/upload-clients-documents'], 'icon' => 'file-o'],
                                        ['label' => 'Page', 'url' => ['/page'], 'icon' => 'file-o'],
                                        ['label' => 'Qr-Clients', 'url' => ['/qr-clients'], 'icon' => 'qrcode'],
                                        ["label" => "Layout", "url" => ["site/layout"], "icon" => "file"],
                                        ["label" => "Error page", "url" => ["site/error-page"], "icon" => "close"],
                                        [
                                            "label" => "Settings",
                                            "icon" => "th",
                                            "url" => "#",
                                            "items" => [
                                                ['label' => 'Email Settings', 'url' => ['/email-settings']],
                                                ['label' => 'SmS Settings', 'url' => ['/sms-settings']],
                                                ["label" => "Panel", "url" => ["site/panel"]],
                                            ],
                                        ],
                                        [
                                            "label" => "Blog",
                                            "icon" => "th",
                                            "url" => "#",
                                            "items" => [
                                                ['label' => 'Blog Post', 'url' => ['/blog-post'], 'icon' => 'blog-o'],
                                                ['label' => 'Blog Category', 'url' => ['/blog-category']],
                                            ],
                                        ],
                                        [
                                            "label" => "Badges",
                                            "url" => "#",
                                            "icon" => "table",
                                            "items" => [
                                                [
                                                    "label" => "Default",
                                                    "url" => "#",
                                                    "badge" => "123",
                                                ],
                                                [
                                                    "label" => "Success",
                                                    "url" => "#",
                                                    "badge" => "new",
                                                    "badgeOptions" => ["class" => "label-success"],
                                                ],
                                                [
                                                    "label" => "Danger",
                                                    "url" => "#",
                                                    "badge" => "!",
                                                    "badgeOptions" => ["class" => "label-danger"],
                                                ],
                                            ],
                                        ],
                                        [
                                            "label" => "Multilevel",
                                            "url" => "#",
                                            "icon" => "table",
                                            "items" => [
                                                [
                                                    "label" => "Second level 1",
                                                    "url" => "#",
                                                ],
                                                [
                                                    "label" => "Second level 2",
                                                    "url" => "#",
                                                    "items" => [
                                                        [
                                                            "label" => "Third level 1",
                                                            "url" => "#",
                                                        ],
                                                        [
                                                            "label" => "Third level 2",
                                                            "url" => "#",
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            );
                        } elseif (isset(Yii::$app->authManager->getRolesByUser($id)['clientManage'])){
                         echo \yiister\gentelella\widgets\Menu::widget(
                                [
                                    "items" => [
                                        ["label" => "Home", "url" => ['/site/index'], "icon" => "home"],
//                                        ['label' => 'Dashboard', 'url' => ['/user/dashboard'], 'icon' => 'tachometer'],
                                        ['label' => 'Clients', 'url' => ['/clients'], 'icon' => 'male'],
//                                        ['label' => 'Signup', 'url' => ['/user/signup'], 'icon' => 'pencil-square-o'],
                                        ['label' => 'Profile', 'url' => ['/user-profile/index'], 'icon' => 'user-circle'],
                                        ['label' => 'Analitycs', 'url' => ['/user-profile/analitycs'], 'icon' => 'area-chart'],
                                        ['label' => 'Appointment View', 'url' => ['appointment/view', 'id' => \Yii::$app->user->identity->getId()], 'icon' => 'calendar-check-o'],
                                        ['label' => 'Business Houres', 'url' => ['/business-hours'], 'icon' => 'clock-o'],
                                        ['label' => 'Interval Houres', 'url' => ['/interval-hours'], 'icon' => 'clock-o'],
                                        ['label' => 'Upload Clients Documents', 'url' => ['/upload-clients-documents'], 'icon' => 'file-o'],
                                        ['label' => 'Page', 'url' => ['/page'], 'icon' => 'file-o'],
                                        ['label' => 'Qr-Clients', 'url' => ['/qr-clients'], 'icon' => 'qrcode'],
                                        ["label" => "Layout", "url" => ["site/layout"], "icon" => "file"],
                                        ["label" => "Error page", "url" => ["site/error-page"], "icon" => "close"],
                                        [
                                            "label" => "Settings",
                                            "icon" => "th",
//                                            "url" => "",
                                            "items" => [
                                                ['label' => 'Email Settings', 'url' => ['/email-settings']],
                                                ['label' => 'SmS Settings', 'url' => ['/sms-settings']],
                                                ["label" => "Panel", "url" => ["site/panel"]],
                                            ],
                                        ],
                                        [
                                            "label" => "Blog",
                                            "icon" => "th",
                                            "url" => "#",
                                            "items" => [
                                                ['label' => 'Blog Post', 'url' => ['/blog-post'], 'icon' => 'blog-o'],
                                                ['label' => 'Blog Category', 'url' => ['/blog-category']],
                                            ],
                                        ],
                                        [
                                            "label" => "Badges",
                                            "url" => "#",
                                            "icon" => "table",
                                            "items" => [
                                                [
                                                    "label" => "Default",
                                                    "url" => "#",
                                                    "badge" => "123",
                                                ],
                                                [
                                                    "label" => "Success",
                                                    "url" => "#",
                                                    "badge" => "new",
                                                    "badgeOptions" => ["class" => "label-success"],
                                                ],
                                                [
                                                    "label" => "Danger",
                                                    "url" => "#",
                                                    "badge" => "!",
                                                    "badgeOptions" => ["class" => "label-danger"],
                                                ],
                                            ],
                                        ],
                                        [
                                            "label" => "Multilevel",
                                            "url" => "#",
                                            "icon" => "table",
                                            "items" => [
                                                [
                                                    "label" => "Second level 1",
                                                    "url" => "#",
                                                ],
                                                [
                                                    "label" => "Second level 2",
                                                    "url" => "#",
                                                    "items" => [
                                                        [
                                                            "label" => "Third level 1",
                                                            "url" => "#",
                                                        ],
                                                        [
                                                            "label" => "Third level 2",
                                                            "url" => "#",
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            );
                        }

                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
<!--                    <a data-toggle="tooltip" data-placement="top" title="Settings">-->
<!--                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>-->
<!--                    </a>-->
<!--                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">-->
<!--                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>-->
<!--                    </a>-->
<!--                    <a data-toggle="tooltip" data-placement="top" title="Lock">-->
<!--                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>-->
<!--                    </a>-->
	                <?php echo  Html::a( '<span class="glyphicon glyphicon-off" aria-hidden="true"></span>', ['site/logout'], ['data' => ['method' => 'post']]); ?>
<!--                    <a data-toggle="tooltip" data-placement="top" title="Logout">-->
<!--                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>-->
<!--                    </a>-->
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?php if(isset($user_personal_info->avatar)){ echo Url::base(true).'/'.$user_personal_info->avatar; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" width="128px" height="128px" alt="">
	                            <?php echo $user_personal_info['first_name'] .' '. $user_personal_info['last_name']; ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
<!--                            $user_personal_info-->
<!--                            <img src="--><?php //if(isset($model->avatar)){ echo Url::base(true).'/'.$model->avatar; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?><!--" width="220px" height="220px" style="    margin: 0 auto;-->
<!--                border-radius: 50%;-->
<!--                text-align: center;-->
<!--                display: block;-->
<!--            ">-->
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="<?php echo Url::base(true);?>/user-profile/index">  Profile</a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">Help</a>
                                </li>

                                <li>
	                                <?php echo  Html::a( 'Logout (' . Yii::$app->user->identity->username . ')<i class="fa fa-sign-out pull-right"></i>', ['site/logout'], ['data' => ['method' => 'post']]); ?>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                      <span class="image">
                                        <img src="http://placehold.it/128x128" alt="Profile Image" />
                                    </span>
                                        <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                                        <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                      <span class="image">
                                        <img src="http://placehold.it/128x128" alt="Profile Image" />
                                    </span>
                                        <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                                        <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                      <span class="image">
                                        <img src="http://placehold.it/128x128" alt="Profile Image" />
                                    </span>
                                        <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                                        <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                      <span class="image">
                                        <img src="http://placehold.it/128x128" alt="Profile Image" />
                                    </span>
                                        <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                                        <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a href="/">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <?php } else {?>

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?php Yii::$app->homeUrl; ?>" class="site_title"><i class="fa fa-paw"></i> <span><?php echo Yii::$app->name; ?></span></a>
                </div>
                <div class="clearfix"></div>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => [
                                    ["label" => "Home", "url" => "/", "icon" => "home",],
                                    ['label' => 'Login', 'url' => ['/user/login'],'icon'=>'sign-in'],

                                ],
                            ]
                        )
                        ?>
                    </div>

                </div>
            </div>
        </div>
                <!-- /sidebar menu -->
       <?php } ?>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
			<?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endif; ?>
            <div class="clearfix"></div>

			<?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com" rel="nofollow" target="_blank">Colorlib</a><br />
                Extension for Yii framework 2 by <a href="http://yiister.ru" rel="nofollow" target="_blank">Yiister</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

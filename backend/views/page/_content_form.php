<?php

use common\widgets\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

	<?= $form->field($model, 'content')->widget(CKEditor::className(), [
	    'options' => ['rows' => 6],
	    'preset' => 'full'
    ]); ?>

	<?= $form->field($model, 'left_sidebar')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'full'
	]); ?>

	<?= $form->field($model, 'right_sidebar')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'full'
	]); ?>

</div>

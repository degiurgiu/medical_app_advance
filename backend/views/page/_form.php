<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>
	<?= Tabs::widget([
		'items' => [
			[
				'label' => 'Base Setting page',
				'content' => $this->render('_main_form', ['model' => $model,'form'=>$form]),
				'active' => true
			],

			[
                'label' => 'Content Page',
                'content' => $this->render('_content_form', ['model' => $model,'form'=>$form]),
			],
		],

	]);?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

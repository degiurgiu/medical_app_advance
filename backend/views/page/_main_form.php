
<div class="page-form">


	<?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusPublish()) ?>

    <?php echo $form->field($model, 'page_type')->dropDownList($model->getPageType());?>

    <?= $form->field($model, 'metaTitle')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'metaDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'metaKeywords')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'createdAt')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'updatedAt')->hiddenInput()->label(false) ?>


</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'url',
            'title:html',
            'page_type',
            'content:html',
            'status:boolean',
            'slug:url',
            //'metaTitle:ntext',
            //'metaDescription:ntext',
            //'metaKeywords:ntext',
            'createdAt',
            'updatedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
//    print_r($model);
$siteTree=[];
$test=[];
foreach ($model as $mod) {
  //  print_r($mod);
//    $siteTree[$mod['id']]= $mod['title'];
    $siteTree['title']= $mod['title'];
    $siteTree['id']= $mod['id'];
    $siteTree['url']= $mod['url'];
    $test[] =$siteTree;

}
//print_r($test);
     echo yii2mod\tree\Tree::widget([
        'items' =>$test,


        'clientOptions' => [
            'autoCollapse' => true,
            'clickFolderMode' => 3,
            'activate' => new \yii\web\JsExpression('
                        function(node, data) {
                              node  = data.node;
                              // Log node title
                              console.log(node);
                              console.log(node.title);
                        }
                '),
        ],
    ]);

<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\page */

//use common\dosamigos\ckeditor\CKEditorInline;
use yii\helpers\ArrayHelper;


$this->title = $model->metaTitle;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->metaKeywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->metaDescription]);
?>
<div class="page-wrapper col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="page-content">
        <?php //CKEditorInline::begin(['preset' => 'full']);?>

<!--        --><?php //echo \common\widgets\ckeditor\CKEditor::run_php2(stripslashes($model->content)); ?>
        <?php echo $model->content; ?>

        <?php //CKEditorInline::end();?>

    </div>
</div>

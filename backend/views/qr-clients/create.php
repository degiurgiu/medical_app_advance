<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QrClients */

$this->title = Yii::t('app', 'Create Qr Clients');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Qr Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qr-clients-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

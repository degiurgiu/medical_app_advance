<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\QrClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Url;
$this->title = Yii::t('app', 'Qr Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qr-clients-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Qr Clients'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'qr_client_path',
            [
                'attribute'=>'image',
                'label'=>'Image',
                'format'=>'raw',

                'value' => function ($data) {
                    $url = Url::base(true).'/'.$data->qr_client_path;
                    return Html::img($url, ['alt'=>'myImage','width'=>'100','height'=>'100']);
                }

            ],
            'client_id',
            'updated_at',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

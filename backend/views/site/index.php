<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

            <?= Yii::t('app', 'English text'); ?>
        <?php
    echo        Yii::$app->language;
        ?>
        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
           <?php
//           $user_id =\Yii::$app->user->identity->getId();
//           if(isset($user_id)) {
           if (!Yii::$app->user->isGuest){
               echo Yii::$app->controller->renderPartial('analitycs', array());
           }
           ?>
        </div>

    </div>
</div>

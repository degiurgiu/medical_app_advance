<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SmsSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sms_appointment')->checkbox() ?>
    <?php echo "first_name, last_name, phone_client, date, doctor_first_name, doctor_last_name "; ?>
    <?= $form->field($model, 'sms_text_appointment')->textarea(['rows' => 6]) ?>
    <?php echo " token 8f2b2898d548b85e4245d9726c0cb0dc"; ?>
    <?= $form->field($model, 'sms_token')->textInput() ?>
    <?php echo " SID AC975c242f5a67e367fd6204a700654036"; ?>
    <?= $form->field($model, 'sms_sid')->textInput() ?>
        <?php echo "+19892827658"; ?>
    <?= $form->field($model, 'sms_phone_number')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

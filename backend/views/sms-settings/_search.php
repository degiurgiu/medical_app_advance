<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SmsSettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sms_text_appointment') ?>

    <?= $form->field($model, 'sms_token') ?>

    <?= $form->field($model, 'sms_sid') ?>

    <?= $form->field($model, 'sms_phone_number') ?>

    <?php // echo $form->field($model, 'sms_appointment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

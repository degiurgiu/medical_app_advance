<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SmsSettings */

$this->title = 'Create Sms Settings';
$this->params['breadcrumbs'][] = ['label' => 'Sms Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

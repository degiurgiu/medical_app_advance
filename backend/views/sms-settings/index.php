<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SmsSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-settings-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
<!--    --><?php //// echo $this->render('_search', ['model' => $searchModel]); ?>
<!---->
<!--    <p>-->
<!--        --><?//= Html::a('Create Sms Settings', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
<!---->
<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'sms_text_appointment:ntext',
//            'sms_token:ntext',
//            'sms_sid:ntext',
//            'sms_phone_number:ntext',
//            //'sms_appointment',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php  if(!count($sms_settings) >= 1){ echo Html::a('Create Email Settings', ['create'], ['class' => 'btn btn-success']);} ?>
    <?php foreach ($sms_settings as $item) {
        echo Html::a('Update', ['update', 'id' => $item->id], ['class' => 'btn btn-primary']);
        echo '<br>';
        echo Html::a('Delete', ['delete', 'id' => $item->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]);
        echo '<br>';
        echo 'sms appointment send';
        echo $item->sms_appointment;
        echo '<br>';
        echo 'text for the message';
        echo $item->sms_text_appointment;
        echo '<br>';
        echo ' Sms Token ';
        echo $item->sms_token;
        echo '<br>';
        echo '<br>';
        echo ' Sms Sid ';
        echo $item->sms_sid;
        echo '<br>';
        echo '<br>';
        echo 'Sms phone number ';
        echo $item->sms_phone_number;
        echo '<br>';
        // var_dump($item);
    }
    //    var_dump($interval_houres);
    ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SmsSettings */

$this->title = 'Update Sms Settings: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sms Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

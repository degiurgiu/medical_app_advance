<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UploadClientsDocuments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upload-clients-documents-form">

    <?php $form = ActiveForm::begin(
	    [ 'id'=>'user-profile-update-form',
		    'enableClientValidation' => true,
		    'options' => [
			    'enctype'=>'multipart/form-data',
			    'validateOnBlur' => true,
			    'class' => 'form'
		    ]]
    ); ?>

	<?= $form->field($model, 'document_name')->textInput(); ?>

	<?= $form->field($model, 'document')->fileInput();  ?>

    <?= $form->field($model, 'client_documents_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

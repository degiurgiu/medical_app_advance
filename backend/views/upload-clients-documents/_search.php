<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UploadClientsDocumentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upload-clients-documents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'document') ?>
	<?= $form->field($model, 'document_name') ?>
	<?= $form->field($model, 'client_documents_id') ?>
    <?= $form->field($model->clientDocuments->first_name.' '.$model->clientDocuments->last_name, 'first_name_last_name') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Html;
?>


<div class="client-documents-list">
	<br>
	<br>
	<br>
	<br>

	<?php
if(count($documents_list) > 0) {
    	echo Html::a('Back', ['clients/index'], ['class' => 'btn  btn-primary',]);
	foreach ($documents_list as $item) {
		echo '<br>';
    echo $item->document_name;?>
		<?php
        echo Html::a('Download', ['download', 'path' => $item->document], [
			'class' => 'btn  btn-primary',
			'data' => [
				'method' => 'post',
			],
		]);
		echo Html::a('Update', ['update', 'id' => $item->id], [
			'class' => 'btn  btn-primary',
		]);
		echo Html::a('Delete', ['delete', 'id' => $item->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]);
		?>
<?php	}
}else { ?>
    <h2>No Documents Available for this client</h2>
	<?= Html::a('Back', ['clients/index'], [
		'class' => 'btn  btn-primary',]); ?>
<?php } ?>

</div>
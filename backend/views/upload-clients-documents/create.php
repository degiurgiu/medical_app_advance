<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UploadClientsDocuments */

$this->title = 'Create Upload Clients Documents';
$this->params['breadcrumbs'][] = ['label' => 'Upload Clients Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upload-clients-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UploadClientsDocumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Clients Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upload-clients-documents-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Upload Clients Documents', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'document',
	        [
		        'attribute' => 'first_name_last_name',
		        'label' => 'Name',
		        'value' => function($searchModel) { return $searchModel->clientDocuments->first_name  . " " . $searchModel->clientDocuments->last_name ;},
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

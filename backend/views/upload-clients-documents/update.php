<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UploadClientsDocuments */

$this->title = 'Update Upload Clients Documents: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Upload Clients Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="upload-clients-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

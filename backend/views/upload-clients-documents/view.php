<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UploadClientsDocuments */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Upload Clients Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upload-clients-documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'document',
            'client_documents_id',
        ],
    ])

    ?>
	<?= Html::a('Download', ['download', 'path' => $model->document], [
		'class' => 'btn  btn-primary',
		'data' => [
			'method' => 'post',
		],
	]) ?>

</div>

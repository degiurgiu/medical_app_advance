<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\widgets\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\UserPersonalInfo */
/* @var $form ActiveForm */
?>
<div class="user-profile">
	<?php $form = ActiveForm::begin(
        [ 'id'=>'user-profile-update-form',
		'enableClientValidation' => true,
		'options' => [
			'enctype'=>'multipart/form-data',
			'validateOnBlur' => true,
			'class' => 'form'
		]]); ?>
    <img src="<?php if(isset($model->avatar)){ echo Url::base(true).'/'.$model->avatar; }?>" width="120px" height="120px" style=" border-radius: 50%;">
    <?= $form->field($model, 'avatar')->fileInput();  ?>

	<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['type' => 'number']) ?>
	<?= $form->field($model, 'speciality')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'about')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]); ?>

    <?= $form->field($model, 'appointment_email')->checkbox() ?>

	<?= $form->field($model->user, 'email') ?>




	<div class="form-group">
		<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>

</div><!-- user-profile -->
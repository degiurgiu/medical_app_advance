<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 17.01.2019
 * Time: 12:41
 */
use common\widgets\highcharts\Highcharts;
use common\widgets\highcharts\HighchartsAsset;
HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']);
?>
<h1>
    <?php echo $this->title ='Analytics';
	$this->params['breadcrumbs'][] = $this->title;
?></h1>

<?php
$appointment_dates= array();
foreach ($appointment_data as $item){
    $date = substr($item['appointment_date_time'],0,10);
    if (isset($appointment_dates[$date]))
	    $appointment_dates[$date]++;
    else
	    $appointment_dates[$date]=1;

}
$finale_appointment_dates= array();
foreach ($appointment_dates as $date => $count){
	$finale_appointment_dates[]=[$date,$count];
}


$client_dates= array();
foreach ($client_data as $item){
	$date = substr($item['created_at'],0,10);
	if (isset($client_dates[$date]))
		$client_dates[$date]++;
	else
		$client_dates[$date]=1;

}
$finale_client_dates= array();
foreach ($client_dates as $date => $count){
	$finale_client_dates[]=[$date,$count];
}

echo Highcharts::widget([

	'options' => [
		'chart'=> ['type'=>'area'],
		'title' => ['text' => 'Clients Appointments'],
		'xAxis' => [
			 'categories'=> array_keys($appointment_dates)
		],
		'yAxis' => [
			'title' => ['text' => 'Clients Appointments']
		],
		 'plotOptions' =>[
        'area'=> [
//            'pointStart'=> 1940,
            'marker'=> [
                'enabled'=> false,
                'symbol'=> 'circle',
                'radius'=> 2,
                'states'=> [
                    'hover'=> [
                        'enabled'=> true
                    ]
                ]
            ]
        ]
    ],
		'series' => [
            [   'name'=> 'Clients',
				'data' => $finale_appointment_dates,
            ]

		],
        'credits' => ['enabled' => false],
	],
	'scripts' => [
		'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
		'modules/exporting', // adds Exporting button/menu to chart
		'themes/grid'        // applies global 'grid' theme to all charts
	],

]);

echo '</br>';
echo '</br>';

echo Highcharts::widget([

	'options' => [
		'chart'=> ['type'=>'area'],
		'title' => ['text' => 'Clients Register'],
		'xAxis' => [
			'categories'=> array_keys($client_dates)
		],
		'yAxis' => [
			'title' => ['text' => 'Clients Register']
		],
		'plotOptions' =>[
			'area'=> [
//            'pointStart'=> 1940,
				'marker'=> [
					'enabled'=> false,
					'symbol'=> 'circle',
					'radius'=> 2,
					'states'=> [
						'hover'=> [
							'enabled'=> true
						]
					]
				]
			]
		],
		'series' => [
			[   'name'=> 'Clients',
				'data' => $finale_client_dates,
			]

		],
		'credits' => ['enabled' => false],
	],
	'scripts' => [
		'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
		'modules/exporting', // adds Exporting button/menu to chart
		'themes/grid'        // applies global 'grid' theme to all charts
	],

]);

?>

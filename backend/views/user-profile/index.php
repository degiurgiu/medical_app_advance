<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-contet-profile col-sm-12">
<!--    <div class="col-sm-2"></div>-->
    <div class="col-sm-12" style=" padding: 15px;">

        <div class="avatar-user col-sm-3">
            <img src="<?php if(isset($model->avatar)){ echo Url::base(true).'/'.$model->avatar; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" width="220px" height="220px" style="    margin: 0 auto;
                border-radius: 50%;
                text-align: center;
                display: block;
            ">
            <br/>
            <h4 style="margin-top: 20px;font-weight: bold; text-align: center"><span><?php echo $model->first_name; ?></span>
                <span><?php echo $model->last_name; ?></span></h4>
        </div>

        <div class="info-user col-sm-8">
            <div class="col-sm-12">
                <div class="col-sm-4">
                    <span style="">Email: <?php echo $model->user['email'] ?></span>
                </div>
                <div class="col-sm-4">
                    <span style="">Phone: <?php echo $model->phone; ?></span>
                </div>
                <div class="col-sm-4">
                    <span style="">User Name: <?php echo $model->user['username'] ?></span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4" style="margin-top: 40px;">
                    <span style="margin-right: 100px">Speciality: <?php echo $model->speciality ?></span>
                </div>
            </div>

        </div>
        <div class="col-sm-1" style="position: absolute;top: 0;right: 0;text-align: center; margin-top: 6px;">
	        <?php
	        echo Html::a(' Edit Profile', ['/user-profile/update','id' => $model->id], ['class' => 'btn btn-primary']);
	        ?>
        </div>
        <div class="col-sm-10" style="margin-top: 33px; border: 2px solid #cccccc; padding: 20px; margin-left: 70px;">
            <p> <span>About: <?php echo $model->about; ?></span></p></div>
    </div>
<!--    <div class="col-sm-2"></div>-->
</div>

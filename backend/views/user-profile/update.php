<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPersonalInfo */

$this->title = 'Update Profile User: ' . $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Profile User Update', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

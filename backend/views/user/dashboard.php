<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;
$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .dasboardtable th, .dasboardtable td {
        border: 1px solid #AAAAAA;
        padding: 3px 2px;
    }
</style>
<div class="dashboard-index">

	<div class="body-content">

		<div class="row">
			<h1>DashBoard</h1>
            <h4>Active users</h4>
            <table class="dasboardtable">
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Update</th>
                    <th>Unlink</th>
                    <th>Delete</th>
                </tr>
           <?php

           foreach ($users as $user){
               ?>


                   <tr>
                       <td><?php echo $user->personalInfo['id'];?></td>
                       <td><?php echo $user->personalInfo['first_name'];?></td>
                       <td><?php echo $user->personalInfo['last_name'];?></td>
                       <td><?php  echo $user->username; ?></td>
                       <td><?php  echo $user->email;  ?></td>
                       <td><?php  echo Html::a('Update', ['update', 'id' => $user->id], ['class' => 'btn btn-primary']); ?></td>
                       <td><?php echo Html::a('Unlink', ['unlink', 'id' => $user->id], ['class' => 'btn btn-danger', 'data' => ['confirm' => 'Are you sure you want to deactivate this user?', 'method' => 'post',],]); ?></td>
                       <td><?php  echo Html::a('Delete', ['delete', 'id' => $user->id], ['class' => 'btn btn-danger', 'data' => ['confirm' => 'Are you sure you want to delete this user?', 'method' => 'post',],]); ?></td>
                   </tr>

            <?php
           }
           ?>
            </table>
		</div>
        <div class="row">
         <h4>Deactivated users</h4>
            <table class="dasboardtable">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Update</th>
                    <th>Activate</th>
                    <th>Delete</th>
                </tr>
		<?php
		foreach ($users_deactivated as $user_dez){
        ?>

                   <tr>
                       <td><?php echo $user_dez->personalInfo['first_name'];?></td>
        <td><?php echo $user_dez->personalInfo['last_name'];?></td>
        <td><?php  echo $user_dez->username; ?></td>
        <td><?php  echo $user_dez->email;  ?></td>
        <td><?php  echo Html::a('Update', ['update', 'id' => $user_dez->id], ['class' => 'btn btn-primary']); ?></td>
        <td><?php echo  Html::a('Activate User', ['activate', 'id' => $user_dez->id], ['class' => 'btn btn-danger', 'data' => ['confirm' => 'Are you sure you want to activate this user?', 'method' => 'post',],]); ?></td>
        <td><?php  echo Html::a('Delete', ['delete', 'id' => $user_dez->id], ['class' => 'btn btn-danger', 'data' => ['confirm' => 'Are you sure you want to delete this user?', 'method' => 'post',],]); ?></td>
        </tr>


        <?php } ?>
        </div>
        </table>
        <br/>
        <br/>
        <div>
            <button class="btn btn-primary" type="button" data-toggle="collapse"  data-target="#newuser" aria-expanded="false" aria-controls="collapseExample">
               Add new User
            </button>
            <div class="collapse" id="newuser">
                <div class="card card-body">
                    <?php
                    //Yii::info("here start",'my_category');
                    echo Yii::$app->controller->renderPartial('signup',array('model'=>$signup,'authItems'=>$roles));
                    ?>
                </div>
            </div>
        </div>
        <br/>
        <div>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#newrole" aria-expanded="false" aria-controls="collapseExample">
                Add new Role
            </button>
            <div class="collapse" id="newrole">
                <div class="card card-body">
					<?php
					//Yii::info("here start",'my_category');
					echo Yii::$app->controller->renderPartial('role',array('model'=>$role));
					?>
                </div></div>
        </div>
	</div>
</div>

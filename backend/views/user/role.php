<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/29/2018
 * Time: 9:37 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
?>
<div class="role-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<div class="role-form">

		<?php $form = ActiveForm::begin([
				'id'=>'role-form',
				'enableClientValidation' => true,
				'options' => [
					'validateOnBlur' => true,
					'class' => 'form'
				]
            ]); ?>

		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

<!--		--><?//= $form->field($model, 'user_id')->hiddenInput(['value'=>Yii::$app->user->id])->label(false) ?>
<!--		--><?php
//		if (\Yii::$app->user->can('admin')) {
//			echo $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'), ['class' => 'form-control']);
//		}
		?>
		<div class="form-group">
			<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>

</div>

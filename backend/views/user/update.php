<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update User: ' . $model->personalInfo['first_name'];

?>
<div class="user-update">
	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id'=>'user-update-form',
        'enableClientValidation' => true,
	    'options' => [
            'enctype'=>'multipart/form-data',
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ]]);?>
    <?= $form->field($model, 'username')->textInput(['autofocus' => 'true']);  ?>

    <?= $form->field($model, 'email'); ?>

    <?= $form->field($model, 'new_password')->passwordInput(); ?>
	<?php
        $revokeArray = ArrayHelper::map($current_user_permissions,'item_name','item_name');
	    $allArray =ArrayHelper::map($all_permissions,'name','name');
	echo 'Remove permission s from user';
	echo $form->field($model, 'delete_permissions')->checkboxList($revokeArray)->label(false);
	echo 'Add permission to user';
	echo $form->field($model, 'add_permissions')->checkboxList($allArray)->label(false);
    ?>


    <?= $form->field($model,'updated_at')->hiddenInput(['value'=>date('Y-m-d')])->label(false); ?>
    <div class="form-group">
        <?= Html::submitButton('Update User', ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
    </div>

    <?php ActiveForm::end();?>

</div>

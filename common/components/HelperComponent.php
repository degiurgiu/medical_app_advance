<?php

/**
 * Created by PhpStorm.
 * User: Edward & Denisa
 * Date: 12/26/2018
 * Time: 10:34 PM
 */
namespace common\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
class HelperComponent extends Component
{
    public function sendEmail($email,$to, $subject, $message, $options= array()) {
       // $email ="giurgiu_dan94@yahoo.com";
        \Yii::$app->mailer->htmlLayout = "layouts/html";
//        Yii::$app->params['adminEmail'];
        $emailSend = \Yii::$app->mailer->compose( 'layouts/html',['content'=>$message]);
        $emailSend->setFrom($email);
        $emailSend->setTo($to);
        $emailSend->setHtmlBody($message);
        $emailSend->setSubject($subject);

        return $emailSend->send();
    }
	public static function template_substitution($template, $data) {
		$placeholders = array_keys($data);
		foreach ($placeholders as &$placeholder) {
			$placeholder = strtolower("{{$placeholder}}");
		}
		return str_replace($placeholders, array_values($data), $template);
	}
}
<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
//	'timeZone' => 'Europe/Bucharest',
	'language' => 'en-EN',
//	'language' => 'ro-RO',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	    'i18n' => [
		    'translations' => [
			    'app*' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@common/messages',
			    ],
		    ],
	    ],
	    'authManager' => [
		    'class' => 'yii\rbac\DbManager',
	    ],
        'HelperComponent' => [
            'class' => 'common\components\HelperComponent',
        ],
        'TwilioComponent' => [
            'class' => 'common\components\TwilioComponent',
        ],
    ],
	'modules' => [
		'gridview' =>  [
			'class' => '\kartik\grid\Module'
			]
	],
];

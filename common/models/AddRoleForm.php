<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 05.11.2018
 * Time: 14:08
 */

namespace common\models;

use yii\base\Model;
use Yii;
class AddRoleForm extends Model
{
	public $name;
	public $type;
	public $description;

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['name', 'trim'],
			['name', 'required'],
			['name', 'unique', 'targetClass' => '\common\models\AuthItem', 'message' => 'This name has already been taken.'],
			['name', 'string', 'min' => 2, 'max' => 255],
			[['type'], 'integer'],
			[['description'], 'string'],
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function addrole()
	{
		if (!$this->validate()) {
			return null;
		}
        $auth = Yii::$app->authManager;
        $role = $auth->createRole($this->name);
        $role->description = $this->description;
        $role->createdAt= date('Y-m-d');
        $role->updatedAt= date('Y-m-d');
        $auth->add($role);

		return $auth ? $auth : null;
	}
}

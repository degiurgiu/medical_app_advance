<?php

namespace common\models;

use common\components\TwilioComponent;
use Yii;

/**
 * This is the model class for table "appointment".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $appointment_date_time
 * @property string $user_appointment_ip
 * @property string $privacy_policy_gdpr_agreement
 * @property int $user_appointment_id
 * @property string $created_at
 *
 *
 * @property UserPersonalInfo $userAppointment
 */
class Appointment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_appointment_id'], 'integer'],
	        [['privacy_policy_gdpr_agreement'], 'boolean'],
	        [['user_appointment_ip','first_name','last_name','phone','user_appointment_ip'], 'required'],
            [['user_appointment_ip','first_name','last_name','phone'], 'trim'],
            ['phone','integer', 'integerOnly'=>true],
            ['user_appointment_ip', 'ip'],
            ['email', 'email'],
	        ['privacy_policy_gdpr_agreement', 'required', 'on' => ['register'], 'requiredValue' => 1, 'message' => 'my test message'],
            [['first_name', 'last_name', 'phone', 'email', 'appointment_date_time','created_at'], 'string', 'max' => 255],
            [['user_appointment_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserPersonalInfo::className(), 'targetAttribute' => ['user_appointment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' =>  Yii::t('app','First Name'),
            'last_name' =>  Yii::t('app','Last Name'),
            'phone' =>  Yii::t('app','Phone'),
            'email' =>  Yii::t('app','Email'),
            'appointment_date_time' =>  Yii::t('app','Appointment Date/Time'),
	        'privacy_policy_gdpr_agreement'=> Yii::t('app','Privacy Policy'),
	        'user_appointment_ip'=> Yii::t('app','IP'),
            'user_appointment_id' =>  Yii::t('app','User Appointment ID'),
            'created_at' =>  Yii::t('app','Created At'),
        ];
    }
	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
//		$email_settings = EmailSettings::find()->one();
        $email_settings = EmailSettings::find()->andWhere(['user_id'=>$this->user_appointment_id])->all();

//        $sms_settings = SmsSettings::find()->one();
//        print_r($this->user_appointment_id);
        $sms_settings = SmsSettings::find()->andWhere(['user_id'=>$this->user_appointment_id])->all();

		$email_to_user = $this->userAppointment->appointment_email;
		$model= $this;


			$doctor_first_name=$this->userAppointment->first_name;
			$doctor_last_name=$this->userAppointment->last_name;
		$variables = array("first_name"=>$this->first_name,"last_name"=>$this->last_name,"phone_client"=>$this->phone,"date"=>$this->appointment_date_time,
                    "doctor_first_name"=>$doctor_first_name,"doctor_last_name"=>$doctor_last_name);
			Yii::$app->session->setFlash('success', "Appointment added");
			if(isset($sms_settings[0]->sms_appointment) && $sms_settings[0]->sms_appointment ==1){
                $test = new TwilioComponent(['id'=>$sms_settings[0]->user_id]);
//                $test->sms
				$message_sms  =Yii::$app->HelperComponent->template_substitution($sms_settings[0]->sms_text_appointment,$variables);
				$sms = $test->sms('+1'.$model->phone,$message_sms);

				if(!$sms->errorCode) {
					Yii::$app->session->setFlash('success', "SmS send successfully ... $sms->sid");
				}else {
					print_r($sms->errorCode);
					print_r($sms->errorMessage);
					print_r($sms->status);
				}
			}
			if(isset($email_settings[0]->appointment_email_clients) && $email_settings[0]->appointment_email_clients == 1) {
				$email ="giurgiu_dan94@yahoo.com";
				$message_email = Yii::$app->HelperComponent->template_substitution($email_settings[0]->appointment_message_clients,$variables);
				$message = '<p>'.$message_email.' </p>';
				Yii::$app->HelperComponent->sendEmail($email, $model->email, 'Your appointment test', $message);
                Yii::$app->session->setFlash('success', "Email Appointment confirmation send successfully");
			}
			if(isset($email_to_user) && $email_to_user == 1 && isset($email_settings[0])) {
				$email ="giurgiu_dan94@yahoo.com";
				$doctor_email = $model->userAppointment->user->email;
				$doctor_message= Yii::$app->HelperComponent->template_substitution($email_settings[0]->email_user_appointment_message,$variables);
				$message_doctor = '<p>'. $doctor_message.' </p>';
				Yii::$app->HelperComponent->sendEmail($email, $doctor_email, 'You have a new appointment have been added', $message_doctor);
			}
	}
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAppointment()
    {
        return $this->hasOne(UserPersonalInfo::className(), ['id' => 'user_appointment_id']);
    }
	public function getcheckTime(){
		//var_dump(Appointment::find()->andWhere(['user_appointment_id' => \Yii::$app->user->identity->getId()])->all());


    	//return array('07:00:00'=>'07:00:00','07:30:00'=>'07:30:00');
	}
}

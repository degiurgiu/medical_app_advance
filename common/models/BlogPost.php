<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

use yii\behaviors\SluggableBehavior;
/**
 * This is the model class for table "blog_post".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $blog_image
 * @property string $slug
 * @property string $header_type
 * @property string $blog_video_link
 * @property int $status
 * @property int $author_id
 * @property int $category_id
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $author
 * @property BlogCategory $category
 */
class BlogPost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_post';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'category_id'], 'required'],
            [['content','blog_image','slug','blog_video_link','header_type'], 'string'],
            [['author_id', 'category_id','status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'blog_image' => Yii::t('app', 'Blog Image'),
            'blog_video_link' => Yii::t('app', 'Blog Video Link'),
            'header_type' => Yii::t('app', 'Header Type'),
            'author_id' => Yii::t('app', 'Author ID'),
            'category_id' => Yii::t('app', 'Category'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function getStatusPublish(){
        return [
            "1"=>Yii::t('app',"Yes"),
            "0"=>Yii::t('app',"No"),
        ];
    }
    public function getHeaderType(){
        return [
            "image"=>Yii::t('app',"Image"),
            "video"=>Yii::t('app',"Video"),
            "image_video"=>Yii::t('app',"Image and Video"),

        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        $category = BlogCategory::find()->all();
        return ArrayHelper::map($category, 'id', 'category');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "business_hours".
 *
 * @property int $id
 * @property int $dow
 * @property string $start
 * @property string $end
 * @property int $user_business_id
 * @property string $created_at
 *
 * @property User $userBusiness
 */
class BusinessHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dow', 'user_business_id'], 'integer'],
            [['start', 'end', 'created_at'], 'safe'],
            [['user_business_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_business_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dow' => Yii::t('app','Dow'),
            'start' => Yii::t('app','Start'),
            'end' => Yii::t('app','End'),
            'user_business_id' => Yii::t('app','User Business ID'),
            'created_at' => Yii::t('app','Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBusiness()
    {
        return $this->hasOne(User::className(), ['id' => 'user_business_id']);
    }
    public static function getDay(){
        return array(
            '1'=>Yii::t('app','Monday'),
            '2'=>Yii::t('app','Tuesday'),
            '3'=>Yii::t('app','Wednesday'),
            '4'=>Yii::t('app','Thursday'),
            '5'=>Yii::t('app','Friday'),
            );
    }
}

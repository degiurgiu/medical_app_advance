<?php

namespace common\models;;

use Yii;
use yii\db\ActiveRecord;
use common\widgets\qrcode\QrCode;
use common\widgets\qrcode\Format\MeCardFormat;
use yii\helpers\Url;
/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $birthday_date
 * @property string $email
 * @property string $address
 * @property string $uniqid
 * @property int $user_id
 */
class Clients extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['user_id', 'first_name','last_name','phone','birthday_date'], 'required'],
	        ['email', 'email'],
	        ['phone','integer', 'integerOnly'=>true],
            [['user_id'], 'integer'],
            [['uniqid'], 'unique'],
	        [['updated_at'], 'safe'],
            [['first_name', 'last_name', 'phone','birthday_date','email','address','uniqid'], 'string', 'max' => 255],
	        [['user_id'], 'exist', 'targetClass'=>'common\models\User','targetAttribute'=>'id','message'=>Yii::t('app','This User dose not exist'),],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' =>  Yii::t('app','First Name'),
            'last_name' =>  Yii::t('app','Last Name'),
            'phone' =>  Yii::t('app','Phone'),
            'uniqid' =>  Yii::t('app','UniqID'),
            'birthday_date' =>  Yii::t('app','Birthday'),
            'email' =>  Yii::t('app','Email'),
            'address' =>  Yii::t('app','Address'),
            'user_id' =>  Yii::t('app','User ID'),
	        'created_at'=> Yii::t('app','Created At')
        ];
    }
	public function getUser(){
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
	public function getpersonalInfo($id){
		return  UserPersonalInfo::find()->where(['user_id'=>$id])->one();
	}
    public function getClients(){
    	return Clients::find()->all();
    }
	public function getqrClients(){
		return $this->hasOne(QrClients::className(), ['client_id' => 'id']);
	}
    public function getUserID (){
    	return \Yii::$app->user->identity->getId();
    }
    public function creatQrCode($model_param){
		//print_r($model_param);
	    $user_first_name =$this->getpersonalInfo($model_param['user_id'])->first_name;
	    $user_last_name =$this->getpersonalInfo($model_param['user_id'])->first_name;
	    $format = new MeCardFormat();
	    $format->firstName = $model_param['first_name'];
	    $format->lastName = $model_param['last_name'];
	    $format->phone = $model_param['phone'];
	    $format->email = $model_param['email'];
	    $format->userName = $user_first_name.' '.$user_last_name;
	    $format->birthday = $model_param['birthday_date'];
	    $format->address = $model_param['address'];
	    $format->url = Url::base('http') . '/clients/qr?id=' . $model_param['uniqid'];
//			$format->nickName = 'tonydspaniard';
	    $qrCode = (new QrCode($format))
		    //$qrCode = (new QrCode(Url::base('http').'/clients/' . $this->id))
		    ->setSize(250)
		    ->setMargin(5)
		    ->useForegroundColor(0, 0, 0);

	    // now we can display the qrcode in many ways
	    // saving the result to a file:
	    //header('Content-Type: ' . $qrCode->getContentType());
	    $qrCode->writeFile('uploads/qr_codes_patients/' . md5($model_param['id']) . '_' .$model_param['id'].'.png'); // writer defaults to PNG when none is specified

	    if ($qrCode){
	    	return true;
	    }
	    return false;
    }

    public function getclientHistory()
    {
        return $this->hasMany(ClientsHistory::className(), ['client_id' => 'id'])->orderBy(['created_at'=>SORT_ASC]);
    }
}

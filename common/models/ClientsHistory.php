<?php

namespace common\models;;

use Yii;

/**
 * This is the model class for table "clients_history".
 *
 * @property int $id
 * @property string $intervention
 * @property string $observation
 * @property string $date
 * @property string $updated_at
 * @property string $created_at
 * @property int $client_id
 *
 * @property Clients $client
 */
class ClientsHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {//created_at updated_at
        return [
            [['client_id'], 'integer'],
            [['intervention', 'observation', 'date','created_at','updated_at'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'intervention' => Yii::t('app','Intervention'),
            'observation' => Yii::t('app','Observation'),
            'date' => Yii::t('app','Date'),
            'client_id' => Yii::t('app','Client ID'),
	        'updated_at'=> Yii::t('app','Updated At'),
	        'created_at'=> Yii::t('app','Created At')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
}

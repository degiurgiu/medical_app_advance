<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_settings".
 *
 * @property int $id
 * @property string $appointment_message_clients
 * @property string $new_user_message
 * @property int $new_user_email
 * @property int $appointment_email_clients
 * @property int $user_id
 * @property string $email_user_appointment_message
 */
class EmailSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appointment_message_clients', 'new_user_message','email_user_appointment_message'], 'string'],
            [['new_user_email', 'appointment_email_clients','user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'appointment_message_clients' => 'Appointment Message Clients',
            'new_user_message' => 'New User Message',
            'new_user_email' => 'New User Email',
            'appointment_email_clients' => 'Appointment Email Clients',
            'email_user_appointment_message'=>'User notification appointment message',
            'user_id'=>'user id settings'
        ];
    }
}

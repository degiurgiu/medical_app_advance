<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EmailSettings;

/**
 * EmailSettingsSearch represents the model behind the search form of `common\models\EmailSettings`.
 */
class EmailSettingsSearch extends EmailSettings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'new_user_email', 'appointment_email_clients'], 'integer'],
            [['appointment_message_clients', 'new_user_message','email_user_appointment_message'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmailSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'new_user_email' => $this->new_user_email,
            'email_user_appointment_message'=> $this->email_user_appointment_message,
            'appointment_email_clients' => $this->appointment_email_clients,
        ]);

        $query->andFilterWhere(['like', 'appointment_message_clients', $this->appointment_message_clients])
            ->andFilterWhere(['like', 'new_user_message', $this->new_user_message]);

        return $dataProvider;
    }
}

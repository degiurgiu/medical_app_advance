<?php

namespace common\models;
use Yii;

/**
 * This is the model class for table "interval_hours".
 *
 * @property int $id
 * @property string $interval_hours
 * @property int $user_interval_id
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $userInterval
 */
class IntervalHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'interval_hours';
    }

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['user_interval_id'], 'integer'],
			[['updated_at', 'created_at'], 'safe'],
			[['interval_hours'], 'string', 'max' => 255],
			[['user_interval_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_interval_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'interval_hours' => Yii::t('app','Interval Hours'),
			'user_interval_id' => Yii::t('app','User Interval ID'),
			'updated_at' => Yii::t('app','Updated At'),
			'created_at' => Yii::t('app','Created At'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserInterval()
	{
		return $this->hasOne(User::className(), ['id' => 'user_interval_id']);
	}
}

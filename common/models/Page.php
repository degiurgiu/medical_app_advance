<?php

namespace common\models;


use yii\behaviors\SluggableBehavior;
use Yii;
/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $url
 * @property string $title
 * @property string $content
 * @property int $status
 * @property string $page_type
 * @property string $left_sidebar
 * @property string $right_sidebar
 * @property string $metaTitle
 * @property string $metaDescription
 * @property string $metaKeywords
 * @property string $createdAt
 * @property string $updatedAt
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }
	public function behaviors()
	{
		return [
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'url',
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'title', 'content', 'metaTitle'], 'required'],
            [['content', 'metaTitle','slug', 'left_sidebar','right_sidebar','page_type','metaDescription', 'metaKeywords'], 'string'],
            [['status'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['url', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => Yii::t('app','Url'),
            'title' => Yii::t('app','Title'),
            'content' => Yii::t('app','Content'),
            'status' => Yii::t('app','Publish'),
	        'page_type' => Yii::t('app','Page Type'),
	        'left_sidebar' => Yii::t('app','Left Sidebar Content'),
	        'right_sidebar' => Yii::t('app','Right Sidebar Content'),
            'metaTitle' => Yii::t('app','Meta Title'),
            'metaDescription' => Yii::t('app','Meta Description'),
            'metaKeywords' => Yii::t('app','Meta Keywords'),
            'createdAt' => Yii::t('app','Created At'),
            'updatedAt' => Yii::t('app','Updated At'),
        ];
    }
	public function getPageType(){
    	return [
    		"full_content_page"=>Yii::t('app',"Full Content Page"),
		    "right_sidebar"=>Yii::t('app',"Right Sidebar Page"),
		    "left_sidebar"=>Yii::t('app',"Left Sidebar Page"),
		    "left_right_sidebar"=>Yii::t('app',"Left/Right Sidebar Page"),
	    ];
	}
	public function getStatusPublish(){
		return [
			"1"=>Yii::t('app',"Yes"),
			"0"=>Yii::t('app',"No"),
		];
	}
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "qr_clients".
 *
 * @property int $id
 * @property string $qr_client_path
 * @property int $client_id
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Clients $client
 */
class QrClients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qr_clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['qr_client_path'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'qr_client_path' => Yii::t('app', 'Qr Client Path'),
            'client_id' => Yii::t('app', 'Client ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
}

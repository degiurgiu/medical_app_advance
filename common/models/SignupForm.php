<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public $permissions;
	public $first_name;
	public $last_name;
	public $phone;
	public $speciality;
	public $avatar;
	public $about;
	public $user_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
	        [['permissions','first_name','last_name','speciality'],'required'],
	        [['phone','about','avatar'],'safe'],
        ];
    }
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' =>  Yii::t('app','Username'),
			'password' =>  Yii::t('app','Password'),
			'permission' =>  Yii::t('app','Permissions'),
			'first_name' =>  Yii::t('app','First Name'),
			'last_name' =>  Yii::t('app','Last Name'),
			'phone' =>  Yii::t('app','Phone'),
			'email' =>  Yii::t('app','Email'),
			'speciality' =>  Yii::t('app','Speciality'),
			'avatar'=> Yii::t('app','Avatar'),
			'about'=> Yii::t('app','About'),
			'user_appointment_id' =>  Yii::t('app','User Appointment ID'),
			'created_at' =>  Yii::t('app','Created At'),
		];
	}
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
	    $user->created_at = date('Y-m-d h:m:s');
        $user->setPassword($this->password);
        $user->generateAuthKey();
		$user->save();

        $permissionList =$_POST['SignupForm']['permissions'];
        foreach ($permissionList as $permissions) {
	        $newPermission = new AuthAssignment;
	        $newPermission->user_id = "$user->id";
	        $newPermission->item_name = $permissions;
	        $newPermission->save();
        }
		$personal_info = new UserPersonalInfo;
	    $old_img = $personal_info->avatar;
	    $personal_info->first_name = $this->first_name;
	    $personal_info->last_name = $this->last_name;
	    $personal_info->phone = $this->phone;
	    $personal_info->about = $this->about;
	    $personal_info->speciality = $this->speciality;
        $personal_info->unique = uniqid("",true);

            $this->avatar = UploadedFile::getInstance($this, 'avatar');
	    if (!empty($this->avatar)) {
                $this->avatar->saveAs('uploads/' .md5(time()). '.' . $this->avatar->extension);
                $personal_info->avatar = 'uploads/' . md5(time()) . '.' . $this->avatar->extension;
            }else {
		        $personal_info->avatar = $old_img;
			}



	    $personal_info->user_id =$user->id;
	    $personal_info->save();


        return $user ? $user : null;
    }
}

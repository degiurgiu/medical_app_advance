<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_settings".
 *
 * @property int $id
 * @property string $sms_text_appointment
 * @property string $sms_token
 * @property string $sms_sid
 * @property string $sms_phone_number
 * @property int $sms_appointment
 * @property int $user_id
 */
class SmsSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sms_text_appointment', 'sms_token', 'sms_sid', 'sms_phone_number'], 'string'],
            [['sms_appointment','user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_text_appointment' => 'Sms Text Appointment',
            'sms_token' => 'Sms Token',
            'sms_sid' => 'Sms Sid',
            'sms_phone_number' => 'Sms Phone Number',
            'sms_appointment' => 'Sms Appointment',
            'user_id' => 'User id sms settings',
        ];
    }
}

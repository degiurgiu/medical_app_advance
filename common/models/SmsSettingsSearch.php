<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SmsSettings;

/**
 * SmsSettingsSearch represents the model behind the search form of `common\models\SmsSettings`.
 */
class SmsSettingsSearch extends SmsSettings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sms_appointment'], 'integer'],
            [['sms_text_appointment', 'sms_token', 'sms_sid', 'sms_phone_number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmsSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sms_appointment' => $this->sms_appointment,
        ]);

        $query->andFilterWhere(['like', 'sms_text_appointment', $this->sms_text_appointment])
            ->andFilterWhere(['like', 'sms_token', $this->sms_token])
            ->andFilterWhere(['like', 'sms_sid', $this->sms_sid])
            ->andFilterWhere(['like', 'sms_phone_number', $this->sms_phone_number]);

        return $dataProvider;
    }
}

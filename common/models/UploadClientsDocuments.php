<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "upload_clients_documents".
 *
 * @property int $id
 * @property string $document
 * @property int $client_documents_id
 * @property string $document_name
 * @property Clients $clientDocuments
 */
class UploadClientsDocuments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upload_clients_documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_documents_id'], 'integer'],
            [['document'], 'string', 'max' => 255],
	        [['document_name'], 'string', 'max' => 255],
	        [['document_name'], 'required'],
	        [['document'],'required', 'on' => 'create'],
	        [['document'],'file','extensions' => 'png, jpg,docx,pdf'],
	        [['client_documents_id'],'required'],
            [['client_documents_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_documents_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document' => 'Document',
	        'document_name'=>'Document Name',
            'client_documents_id' => 'Client Documents ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientDocuments()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_documents_id']);
    }
}

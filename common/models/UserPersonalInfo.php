<?php

namespace common\models;;

use Yii;

/**
 * This is the model class for table "user_personal_info".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $speciality
 * @property string $avatar
 * @property string $about
 * @property int $user_id
 * @property boolean $appointment_email
 * @property User $unique
 */
class UserPersonalInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file_upload;

    public static function tableName()
    {
        return 'user_personal_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['about'], 'string'],
            [['user_id'], 'integer'],
            [['appointment_email'],'boolean'],
            [['unique'], 'unique'],
	        [['file_upload'], 'file', 'extensions' => ['png', 'jpg']],
            [['first_name', 'last_name', 'phone', 'speciality', 'avatar'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' =>  Yii::t('app','First Name'),
            'last_name' =>  Yii::t('app','Last Name'),
            'phone' =>  Yii::t('app','Phone'),
            'speciality' =>  Yii::t('app','Speciality'),
            'avatar' =>  Yii::t('app','Photo'),
            'about' =>  Yii::t('app','About'),
            'appointment_email'=> Yii::t('app','Receive email new appointment'),
            'unique' =>  Yii::t('app','Unique ID'),
            'user_id' =>  Yii::t('app','User ID'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->
       andWhere(['status' => '1']);
    }
}

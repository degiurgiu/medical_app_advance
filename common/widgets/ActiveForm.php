<?php
/**
 * @copyright     einsundnull
 * @date          4/29/16
 */

namespace common\widgets;

use yii\base\Security;
use yii\web\JsExpression;

/**
 * Class ActiveForm
 * @package       common\widgets
 * @author        Anna Nosova
 */
class ActiveForm extends \yii\widgets\ActiveForm
{
	/**
	 * Initializes the widget.
	 * This renders the open tags needed by the portlet.
	 * It also renders the decoration, if any.
	 */
	public function init()
	{
		$security = new Security();
		$this->setId(hash('crc32', $security->generateRandomString()));
		parent::init();
	}
}

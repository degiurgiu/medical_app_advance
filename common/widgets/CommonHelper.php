<?php
/**
 * @author        Anna Nosova
 * @copyright     einsundnull
 * @date          5/5/16
 */

namespace common\widgets;

use yii\base\Security;

class CommonHelper
{

	public static function getUniqueId()
	{
		$security = new Security();
		return hash('crc32', $security->generateRandomString());
	}

	public static function dateToMySql($datetime)
	{
		$date = new \DateTime($datetime);
		return $date->format('Y-m-d');
	}

}
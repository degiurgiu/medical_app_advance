<?php
/**
 * @copyright     einsundnull
 * @date          4/26/16
 */

namespace common\widgets;

use yii\base\Security;

/**
 * Class GridView
 * @package       common\widgets
 * @author        Anna Nosova
 */
class GridView extends \yii\grid\GridView
{
	/**
	 * @var bool
	 */
	public $hideFilters = true;

	/**
	 * @var string
	 */
	public $layout = "{items}\n{pager}";

	/**
	 * @var array
	 */
	public $tableOptions = ['class' => 'table'];

	/**
	 * @var array
	 */
	public $pager = [
		'class' => '\common\widgets\LinkPager',
	];

	/**
	 * @throws \yii\base\InvalidConfigException
	 */
	public function init()
	{
		if($this->dataProvider !== null && $this->dataProvider->pagination) {
			$this->setDefaultPageSize();
			$this->filterSelector = "input[name='" . $this->dataProvider->pagination->pageParam . "']";
		}
		if (!$this->id) {
			$this->setId(CommonHelper::getUniqueId());
		}
		if ($this->hideFilters) {
			$this->filterRowOptions['style'] = 'display:none;';
		}
		parent::init();
	}

	private function setDefaultPageSize()
	{
		if ($this->dataProvider->pagination && !$this->dataProvider->pagination->defaultPageSize) {
			$this->dataProvider->pagination->defaultPageSize = \Yii::$app->params['default']['pageSize'];
		}
	}

}
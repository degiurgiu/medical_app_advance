<?php
/**
 * @copyright     einsundnull
 * @date          4/26/16
 */

namespace common\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class LinkPager
 * @package       common\widgets
 * @author        Anna Nosova
 */
class LinkPager extends \yii\widgets\LinkPager
{

	/**
	 * {pageButtons} {customPage} {pageSize}
	 */
	public $template = '{pageSize}{pageButtons}{customPage}';

	/**
	 * @var array
	 */
	public $options = ['class' => 'pagination per-page'];

	/**
	 * @var bool
	 */
	public $prevPageLabel = false;

	/**
	 * @var string
	 */
	public $lastPageLabel = 'fa fa-angle-double-right';

	/**\
	 * @var string
	 */
	public $nextPageLabel = 'fa fa-angle-right';

	/**
	 * pageSize list
	 */
	public $pageSizeList = [10, 20, 30, 50];

	/**
	 * Margin style for the  pageSize control
	 */
	public $pageSizeMargin = "margin-left:5px;margin-right:5px;";

	/**
	 * customPage width
	 */
	public $customPageWidth = 50;

	/**
	 * Margin style for the  customPage control
	 */
	public $customPageMargin = "";

	/**
	 * Jump
	 */
	public $customPageBefore = null;

	/**
	 * Page
	 */
	public $customPageAfter = "";

	/**
	 * pageSize style
	 */
	public $pageSizeOptions = ['class' => 'form-control',
							   'style' => 'display: inline-block;width:auto;margin-top:0px;'];

	/**
	 * customPage style
	 */
	public $customPageOptions = [
		'input' => ['class' => '', 'style' => 'height:21px;    text-align: center;    margin: 0 5px;'],
		'block' => ['class' => 'custom-page-go'],
	];

	/**
	 * @throws \yii\base\InvalidConfigException
	 */
	public function init()
	{
		parent::init();
		if ($this->pageSizeMargin) {
			Html::addCssStyle($this->pageSizeOptions, $this->pageSizeMargin);
		}
		if ($this->customPageWidth) {
			Html::addCssStyle($this->customPageOptions['input'], 'width:' . $this->customPageWidth . 'px;');
		}
		if ($this->customPageMargin) {
			Html::addCssStyle($this->customPageOptions['input'], $this->customPageMargin);
		}
	}

	/**
	 * Executes the widget.
	 * This overrides the parent implementation by displaying the generated page buttons.
	 */
	public function run()
	{
		if ($this->registerLinkTags) {
			$this->registerLinkTags();
		}
		echo $this->renderPageContent();
	}

	/**
	 * @return mixed
	 */
	protected function renderPageContent()
	{
		$result = '';
		$content = preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) {
			$name = $matches[1];
			if ('customPage' == $name) {
				return $this->renderCustomPage();
			} elseif ('pageSize' == $name) {
				return $this->renderPageSizer();
			} elseif ('pageButtons' == $name) {
				return $this->renderPageButtons();

			}
			return "";
		}, $this->template);

		if (!empty($content)) {
			$result = Html::tag('div', $content, ['class' => 'grid-pager']);
		}
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderCustomPage()
	{

		$pageCount = $this->pagination->getPageCount();
		if ($pageCount < 2 && $this->hideOnSinglePage) {
			return '';
		}

		$page = 1;
		$params = Yii::$app->getRequest()->queryParams;
		if (isset($params[$this->pagination->pageParam])) {
			$page = intval($params[$this->pagination->pageParam]);
			if ($page < 1) {
				$page = 1;
			} else {
				if ($page > $this->pagination->getPageCount()) {
					$page = $this->pagination->getPageCount();
				}
			}
		}

		$customPage = $this->getCustomPageBefore() .
			Html::textInput($this->pagination->pageParam, $page, $this->customPageOptions['input']) .
			$this->customPageAfter;

		return Html::tag('span', $customPage, $this->customPageOptions['block']);
	}

	/**
	 * @return null|string
	 */
	protected function getCustomPageBefore()
	{
		if (is_null($this->customPageBefore)) {
			$this->customPageBefore = Yii::t('app.widgets', 'Jump to page');
		}
		return $this->customPageBefore;
	}

	/**
	 * Renders the toggle data button
	 * @return string
	 */
	public function renderPageSizer()
	{
		if ($this->pagination->totalCount <= $this->pageSizeList[0]) {
			return;
		}

		$result = Html::beginTag('ul', ['class' => 'pagination']);
		foreach ($this->pageSizeList as $pageSize) {
			$urlData = $this->pagination->params;
			$urlData[$this->pagination->pageSizeParam] = $pageSize;
			$link = Html::a($pageSize, Url::current($urlData));
			$result .= Html::tag('li', $link,
				['class' => ($this->pagination->getPageSize() == $pageSize ? 'active' : '')]);
			if ($this->pagination->totalCount <= $pageSize) {
				break;
			}
		}
		$result .= Html::tag('li', Yii::t('app.widgets', '&nbsp;of {n,number} {n,plural,=1{result} other{results}}.',
			['n' => $this->pagination->totalCount]));
		$result .= Html::endTag('ul');
		return $result;
	}

	/**
	 * Renders a page button.
	 * You may override this method to customize the generation of page buttons.
	 *
	 * @param string  $label    the text label for the button
	 * @param integer $page     the page number
	 * @param string  $class    the CSS class for the page button.
	 * @param boolean $disabled whether this page button is disabled
	 * @param boolean $active   whether this page button is active
	 *
	 * @return string the rendering result
	 */
	protected function renderPageButton($label, $page, $class, $disabled, $active)
	{
		if (in_array($label, [$this->lastPageLabel, $this->nextPageLabel])) {
			$label = Html::tag('i', '', ['class' => $label]);
		}
		$options = ['class' => empty($class) ? $this->pageCssClass : $class];
		if ($active) {
			Html::addCssClass($options, $this->activePageCssClass);
		}
		if ($disabled) {
			Html::addCssClass($options, $this->disabledPageCssClass);

			return Html::tag('li', Html::tag('span', $label), $options);
		}
		$linkOptions = $this->linkOptions;
		$linkOptions['data-page'] = $page;

		return Html::tag('li', Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
	}

}
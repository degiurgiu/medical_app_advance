<?php
/**
 * @copyright     einsundnull
 * @date          5/4/16
 */

namespace common\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Menu2
 * @package       common\widgets
 * @author        Anna Nosova
 */
class Menu extends \yii\widgets\Menu
{

	public $brandLogo = '';

	public $mobileLogo = '';

	public $staticBottom = true;

	public $linkTemplate = '<a href="{url}">{icon}{label}</a>';

	public $userLink = '/user/index';

	public $bottomItems = [
		[
			'icon'  => 'fa-user',
			'url'   => '/user/index',
			'label' => 'User',
		],
		[
			'icon'              => 'fa-flag',
			'url'               => '#',
		],
		[
			'icon'  => 'fa-sign-out',
			'url'   => '/site/logout',
			'label' => 'Logout',
		],
	];

	public $options = ['class' => 'nav-side-menu', 'id' => 'menu-scrooser'];

	public $listOptions = ['id' => 'menu-content', 'class' => 'menu-content'];

	public $listBlockOptions = ['class' => 'menu-list'];

	public $brandOptions = ['class' => 'brand'];

	public $toggleButtonOptions = [];

	public $brandImageOptions = [];

	public $bottomItemsOptions = [
		'big'   => [
			'class' => 'bottom-section',
		],
	];

	protected $content = '';

	/**
	 * Renders the menu.
	 */
	public function run()
	{
		if ($this->route === null && Yii::$app->controller !== null) {
			$this->route = Yii::$app->controller->getRoute();
		}
		if ($this->params === null) {
			$this->params = Yii::$app->request->getQueryParams();
		}
		$this->setDefaults();
		$items = $this->normalizeItems($this->items, $hasActiveChild);
		if (!empty($items)) {
			$options = $this->options;
			$tag = ArrayHelper::remove($options, 'tag', 'nav');

			$this->renderBrand();
//			$this->renderUser();
			$this->renderContentList($items);
			$this->renderBottomSectionWrapped();

			$menu = $this->renderToggleButton();
			$menu .= Html::tag($tag, $this->content, $options);
			echo $menu;
		}
	}

	protected function setDefaults()
	{
		// Brand logo section
		if (!empty($this->brandLogo)) {
			$defaultBrandImageOptions = [
				'alt' => Yii::t('app', 'Logo'),
			];
			$this->brandImageOptions = ArrayHelper::merge($defaultBrandImageOptions, $this->brandImageOptions);
		}
	}

	protected function renderBrand()
	{
		if (!empty($this->brandLogo)) {
			$logo = Html::img($this->brandLogo, $this->brandImageOptions);
			$this->content .= Html::a( $logo, Url::home(), $this->brandOptions);
		}
	}

	protected function renderUser()
	{
		if (Yii::$app->user->identity) {
			$userName = Html::tag('div', Yii::$app->user->identity->getFullName(), ['class' => 'user-full-name']);
			$userName .= Html::tag('div', Yii::$app->user->identity->email, ['class' => 'user-email']);
			$item = [];
			$item['url'] = [$this->userLink];
			$user = Html::a($userName, $item['url'], ['class' => $this->isItemActive($item) ? 'active' : '']);
			$this->content .= Html::tag('div', $user, ['class' => 'user-data']);
		}
	}

	protected function renderToggleButton()
	{
		return Html::tag('i', '', $this->toggleButtonOptions);
	}

	/**
	 * Renders the content of a menu item.
	 * Note that the container and the sub-menus are not rendered here.
	 *
	 * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
	 *
	 * @return string the rendering result
	 */
	protected function renderItem($item)
	{
		$template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

		return strtr($template, [
			'{url}'            => $this->getItemUrl($item),
			'{label}'          => $this->getItemLabel($item),
			'{icon}'           => $this->getItemIcon($item),
			'{showChildArrow}' => $this->getItemArrow($item),
		]);
	}

	/**
	 * Recursively renders the menu items (without the container tag).
	 *
	 * @param array $items the menu items to be rendered recursively
	 *
	 * @return string the rendering result
	 */
	protected function renderItems($items)
	{
		$n = count($items);
		$lines = [];
		foreach ($items as $i => $item) {
			$options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
			$tag = ArrayHelper::remove($options, 'tag', 'li');
			$class = [];
			if ($item['active']) {
				$class[] = $this->activeCssClass;
			}
			if ($i === 0 && $this->firstItemCssClass !== null) {
				$class[] = $this->firstItemCssClass;
			}
			if ($i === $n - 1 && $this->lastItemCssClass !== null) {
				$class[] = $this->lastItemCssClass;
			}
			if (!empty($class)) {
				if (empty($options['class'])) {
					$options['class'] = implode(' ', $class);
				} else {
					$options['class'] .= ' ' . implode(' ', $class);
				}
			}

			$menu = $this->renderItem($item);

			$lines[] = Html::tag($tag, $menu, $options);

		}

		$lines = implode("\n", $lines);

		return $lines;
	}

	protected function getItemIcon($item)
	{
		$result = '';
		if (isset($item['icon'])) {
			$result = Html::tag('i', '', ['class' => 'fa fa-lg ' . $item['icon']]);
		}
		return $result;
	}

	protected function getItemUrl($item)
	{
		$result = '';
		if (isset($item['url'])) {
			$result = Html::encode(Url::to($item['url']));
		}
		return $result;
	}

	protected function getItemArrow($item)
	{
		$result = '';
		if (isset($item['items'])) {
			$result = Html::tag('span', '', ['class' => 'arrow']);
		}
		return $result;
	}

	protected function getItemLabel($item)
	{
		$result = '';
		if (isset($item['label'])) {
			$result = Html::tag('span', Yii::t('app', $item['label']));
		}
		return $result;
	}

	protected function renderBottomSection()
	{
//		$items = '';
//		if (!empty($this->bottomItems)) {
//			foreach ($this->bottomItems as $item) {
//				if (isset($item['hideOnSmallScreen'])
//					&& $item['hideOnSmallScreen'] && $isSmallScreen
//				) {
//					continue;
//				}
//				if ($isSmallScreen) {
//					$items .= Html::tag('li', $this->renderItem($item), $this->bottomItemsOptions['small']);
//				} else {
//					$items .= $this->renderItem($item);
//				}
//			}
//		}

		$items = $this->renderItem($this->bottomItems[0]);

		$items .= \lajax\languagepicker\widgets\LanguagePicker::widget([
			'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
			'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
		]);

		$items .= $this->renderItem($this->bottomItems[2]);

		return $items;
	}

	protected function renderContentList($items)
	{
		$list = $this->renderItems($items);
		$result = Html::tag('ul', $list, $this->listOptions);
		$this->content .= Html::tag('div', $result, $this->listBlockOptions);
	}

	protected function renderBottomSectionWrapped()
	{
		$this->bottomItemsOptions['big']['style'] = YII_ENV_DEV ? 'bottom:25px;' : '' ;
		$this->content .= Html::tag('div', $this->renderBottomSection(), $this->bottomItemsOptions['big']);
	}

}
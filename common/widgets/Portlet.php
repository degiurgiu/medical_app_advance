<?php
/**
 * @author        Anna Nosova
 * @copyright     einsundnull
 * @date          4/21/16
 */

namespace common\widgets;

use yii\base\Security;
use \yii\helpers\Html;
use \yii\base\Widget;

/**
 * Portlet is the base class for portlet widgets.
 * A portlet displays a fragment of content, usually in terms of a block
 * on the side bars of a Web page.
 * To specify the content of the portlet, override the {@link renderContent}
 * method, or insert the content code between the {@link CController::beginWidget}
 * and {@link CController::endWidget} calls. For example,
 * A portlet also has an optional {@link title}. One may also override {@link renderDecoration}
 * to further customize the decorative display of a portlet (e.g. adding min/max buttons).
 * Class Portlet
 * @package       common\widgets
 * @author        Anna Nosova
 */
class Portlet extends Widget
{

	/**
	 * @var array the HTML attributes for the portlet container tag.
	 */
	public $htmlOptions = ['class' => 'panel panel-default'];
	/**
	 * @var string the title of the portlet. Defaults to null.
	 * When this is not set, Decoration will not be displayed.
	 * Note that the title will not be HTML-encoded when rendering.
	 */
	public $title;

	public $buttonsLayout = '';

	public $showFilters = false;

	/**
	 * @var string the CSS class for the decoration container tag. Defaults to 'portlet-decoration'.
	 */
	public $decorationCssClass = '';
	/**
	 * @var string the CSS class for the portlet title tag. Defaults to 'portlet-title'.
	 */
	public $titleCssClass = 'portlet-title';
	/**
	 * @var string the CSS class for the content container tag. Defaults to 'portlet-content'.
	 */
	public $contentCssClass = 'panel-body';

	/**
	 * @var string
	 */
	public $buttonsCssClass = 'portlet-buttons';

	/**
	 * @var string
	 */
	public $buttonAddText = '';

    /**
     * @var string
     */
    public $batchDeleteValue = '';

    /**
     * @var string
     */
    public $batchDeleteTitle = '';

    /**
     * @var string
     */
    public $searchValue = '';

    /**
     * @var string
     */
    public $searchTitle = '';

    /**
     * @var string
     */
    public $batchUpdateValue = '';

    /**
     * @var string
     */
    public $batchUpdateTitle = '';

    /**
     * @var string
     */
    public $importValue = '';


    /**
     * @var string
     */
    public $importTitle = '';

	/**
	 * @var string
	 */
	public $rawData = '';

	/**
	 * @var string
	 */
	public $buttonSaveAction = '';

	/**
	 * @var string
	 */
	public $buttonResetAction = '';

	/**
	 * @var string
	 */
	public $buttonAddAction = '';

	/**
	 * @var boolean whether to hide the portlet when the body content is empty. Defaults to true.
	 * @since 1.1.4
	 */
	public $hideOnEmpty = true;

	/**
	 * @var
	 */
	private $_beginTag;

	/**
	 * @var
	 */
	private $_buttonHideId;

	/**
	 * @var
	 */
	private $_buttonFilterId;

	/**
	 * @var
	 */
	private $_buttonAddId;

	/**
	 * @var
	 */
	private $_buttonSaveId;

	/**
	 * @var
	 */
	private $_buttonResetId;

	/**
	 * Initializes the widget.
	 * This renders the open tags needed by the portlet.
	 * It also renders the decoration, if any.
	 */
	public function init()
	{
		$security = new Security();
		$this->setId(hash('crc32', $security->generateRandomString()));
		ob_start();
		ob_implicit_flush(false);
		$this->htmlOptions['id'] = $this->getId();
		$this->_buttonHideId = $this->getId() . '_button_hide';
		$this->_buttonFilterId = $this->getId() . '_button_filter';
		$this->_buttonAddId = $this->getId() . '_button_add';
		$this->_buttonSaveId = $this->getId() . '_button_save';
		$this->_buttonResetId = $this->getId() . '_button_reset';
		echo Html::beginTag('div', $this->htmlOptions) . "\n";
		$this->renderLoader();
		$this->renderHeader();
		echo "<div class=\"{$this->contentCssClass}\">\n";
		$this->_beginTag = ob_get_contents();
		ob_clean();
	}

	/**
	 * Renders the content of the portlet.
	 */
	public function run()
	{
		$this->renderContent();
		$content = ob_get_clean();
		if ($this->hideOnEmpty && trim($content) === '') {
			return;
		}
		if (!$this->showFilters) {

		}
		$this->registerAssets();
		echo $this->_beginTag;
		echo $content;
		echo '</div>';
		echo Html::endTag('div');
	}

	/**
	 * Renders the decoration for the portlet.
	 * The default implementation will render the title if it is set.
	 */
	protected function renderHeader()
	{
		echo Html::beginTag('div', ['class' => 'panel-heading']);
		if ($this->title !== null) {
			echo Html::tag('div', $this->title, ['class' => $this->titleCssClass]);
		}
		$this->renderButtons();
		echo Html::endTag('div');
	}

	/**
	 *
	 */
	protected function renderLoader()
	{
		echo Html::beginTag('div', ['class' => 'portlet-loader']);
		echo Html::endTag('div');
	}

	/**
	 * Renders the decoration for the portlet.
	 * The default implementation will render the title if it is set.
	 */
	protected function renderButtons()
	{
		$buttons = preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) {
			$name = $matches[1];
			if ('hide' == $name) {
				return $this->renderHideButton();
			} elseif ('filter' == $name) {
				return $this->renderFilterButton();
			} elseif ('add' == $name) {
				return $this->renderAddButton();
			} elseif ('save' == $name) {
				return $this->renderSaveButton();
			} elseif ('raw' == $name) {
				return $this->renderRaw();
			} elseif ('reset-filters' == $name) {
				return $this->renderResetFiltersButton();
			} elseif ('batch-delete' == $name) {
				return $this->renderBatchDeleteButton();
			} elseif ('search' == $name) {
				return $this->renderSearchButton();
			} elseif ('batch-update' == $name) {
				return $this->renderBatchUpdateButton();
			} elseif ('import' == $name) {
				return $this->renderImportButton();
			}

			return "";
		}, $this->buttonsLayout);

		echo Html::tag('div', $buttons, ['class' => $this->buttonsCssClass]);
	}

	/**
	 * Renders the content of the portlet.
	 * Child classes should override this method to render the actual content.
	 */
	protected function renderContent()
	{
	}

	/**
	 * @return string
	 */
	protected function renderHideButton()
	{
		$result = Html::beginTag('a', ['id' => $this->_buttonHideId, 'class' => 'button-box']);
		$result .= Html::tag('i', '', ['class' => 'fa fa-minus']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderFilterButton()
	{
		$result = Html::beginTag('a', ['id' => $this->_buttonFilterId, 'class' => 'button-box']);
		$result .= Html::tag('i', '', ['class' => 'fa fa-filter']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderAddButton()
	{
		$result = Html::beginTag('a',
			['id' => $this->_buttonAddId, 'class' => 'button-box', 'style' => 'width: 115px;']);
		$result .= Html::tag('i', '', ['class' => 'fa fa-plus']);
		$result .= Html::tag('span', '&nbsp;&nbsp' . $this->buttonAddText);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderBatchDeleteButton()
	{
		$result = Html::beginTag('a',
			['class' => 'button-box showModalButton', 'style' => 'width: 115px;', 'value' => $this->batchDeleteValue, 'title' => $this->batchDeleteTitle]);
		$result .= Html::tag('i', '', ['class' => 'fa fa-minus-square-o']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderSearchButton()
	{
		$result = Html::beginTag('a',
			['class' => 'button-box showModalButton', 'style' => 'width: 115px;', 'value' => $this->searchValue, 'title' => $this->searchTitle]);
		$result .= Html::tag('i', '', ['class' => 'fa fa-search']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderBatchUpdateButton()
	{
		$result = Html::beginTag('a',
			['class' => 'button-box showModalButton', 'style' => 'width: 115px;', 'value' => $this->batchUpdateValue, 'title' => $this->batchUpdateTitle]);
		$result .= Html::tag('i', '', ['class' => 'fa fa-truck']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderImportButton()
	{
		$result = Html::beginTag('a',
			['class' => 'button-box showModalButton', 'style' => 'width: 115px;', 'value' => $this->importValue, 'title' => $this->importTitle]);
		$result .= Html::tag('i', '', ['class' => 'fa fa-file']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderSaveButton()
	{
		$result = Html::beginTag('a', ['id' => $this->_buttonSaveId, 'class' => 'button-box']);
		$result .= Html::tag('i', '', ['class' => 'fa fa-save']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * @return string
	 */
	protected function renderRaw()
	{
		return $this->rawData;
	}

	/**
	 * @return string
	 */
	protected function renderResetFiltersButton()
	{
		$result = Html::beginTag('a', ['id' => $this->_buttonResetId, 'class' => 'button-box reset-button']);
		$result .= Html::tag('i', '', ['class' => 'fa fa-refresh']);
		$result .= Html::endTag('a');
		return $result;
	}

	/**
	 * Generate toggle data validation client script
	 * @return string
	 */
	protected function getHideButtonScript()
	{
		$event = 'click';
		return "\$('#{$this->getId()} #{$this->_buttonHideId}').on('{$event}',function(e){
            $('#{$this->getId()} .panel-body').slideToggle();
            $(this).find('i').toggleClass('fa-minus');
            $(this).find('i').toggleClass('fa-plus');
        });";
	}

	/**
	 * Generate toggle data validation client script
	 * @return string
	 */
	protected function getFilterButtonScript()
	{
		$event = 'click';
		$script = '';
//		if (!$this->showFilters) {
////			$script .= "\$('#{$this->getId()} .filters').hide();";
//		}
		$script .= "\$('#{$this->getId()} #{$this->_buttonFilterId}').on('{$event}',function(e){
            $('#{$this->getId()} .filters').slideToggle();
        });";
		return $script;
	}

	/**
	 * Generate toggle data validation client script
	 * @return string
	 */
	protected function getAddButtonScript()
	{
		$event = 'click';
		return "\$('#{$this->getId()} #{$this->_buttonAddId}').on('{$event}',function(e){
            {$this->buttonAddAction};
        });";
	}

	/**
	 * Generate toggle data validation client script
	 * @return string
	 */
	protected function getSaveButtonScript()
	{
		$event = 'click';
		return "\$('#{$this->getId()} #{$this->_buttonSaveId}').on('{$event}',function(e){
            e.preventDefault();
            {$this->buttonSaveAction};
        });";
	}

	protected function getResetFiltersButtonScript()
	{
		$event = 'click';
		return "\$('#{$this->getId()} #{$this->_buttonResetId}').on('{$event}',function(e){
            e.preventDefault();
            {$this->buttonResetAction};
        });";
	}

	/**
	 * Registers client assets
	 */
	protected function registerAssets()
	{
		$view = $this->getView();
		$script = '';
		$script .= $this->getHideButtonScript();
		$script .= $this->getFilterButtonScript();
		$script .= $this->getAddButtonScript();
		$script .= $this->getSaveButtonScript();
		$script .= $this->getResetFiltersButtonScript();
		$gridClientFunc = 'kvGridInit_' . hash('crc32', $script);
		$view->registerJs("var {$gridClientFunc}=function(){\n{$script}\n};\n{$gridClientFunc}();");
	}

	/**
	 * Sets a default css value if not set
	 *
	 * @param array  $options
	 * @param string $css
	 */
	protected static function initCss(&$options, $css)
	{
		if (!isset($options['class'])) {
			$options['class'] = $css;
		}
	}
}
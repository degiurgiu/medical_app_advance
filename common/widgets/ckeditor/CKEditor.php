<?php
/**
 * @copyright Copyright (c) 2013-2016 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace common\widgets\ckeditor;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * CKEditor renders a CKEditor js plugin for classic editing.
 * @see http://docs.ckeditor.com/
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\ckeditor
 */
class CKEditor extends InputWidget
{
    use CKEditorTrait;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->initOptions();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }
        $this->registerPlugin();
    }

    /**
     * Registers CKEditor plugin
     * @codeCoverageIgnore
     */
    protected function registerPlugin()
    {
        $js = [];

        $view = $this->getView();

        CKEditorAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';

        $js[] = "CKEDITOR.replace('$id', $options);";
        //$js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";
//
//        if (isset($this->clientOptions['filebrowserUploadUrl']) || isset($this->clientOptions['filebrowserImageUploadUrl'])) {
//            $js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
//        }

        $view->registerJs(implode("\n", $js));
    }
  public static function run_php2($contents) // allow for use of PHP code with CKeditor instances
    {
        $pattern = '#<phpcode>(.*)</phpcode>#i';
        $total_matches = preg_match_all($pattern, $contents, $matches, PREG_OFFSET_CAPTURE, 3);
        $i = 0;
        $code = array();
        $replacement = array();
        while($i<$total_matches){
            $code[] = $matches[1][$i][0];
            ob_start();
            eval(strip_tags($code[$i]));
            $replacement[] = ob_get_clean();
            $i++;
        }
        $i = 0;
        $final_contents = preg_replace($pattern, $replacement[$i], $contents, 1);
        $i++;
        while($i<$total_matches){
            $final_contents = preg_replace($pattern, $replacement[$i], $final_contents, 1);
            $i++;
        }
        return $final_contents;
    }
}

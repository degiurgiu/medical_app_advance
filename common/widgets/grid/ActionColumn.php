<?php
/**
 * @author        Anna Nosova
 * @copyright     einsundnull
 * @date          5/5/16
 */

namespace common\widgets\grid;

use \Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
	public $dataPjax = '';

	/**
	 * Initializes the default button rendering callbacks.
	 */
	protected function initDefaultButtons()
	{
		if (!isset($this->buttons['update'])) {
			$this->buttons['update'] = function ($url, $model, $key) {
				$options = array_merge([
//					'title' => Yii::t('yii', 'Edit'),
//					'aria-label' => Yii::t('yii', 'Delete'),
//					'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//					'data-method' => 'post',
//					'data-pjax' => $this->dataPjax,
				], $this->buttonOptions);
				return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
			};
		}

		if (!isset($this->buttons['delete'])) {
			$this->buttons['delete'] = function ($url, $model, $key) {
				$options = array_merge([
					'title'        => Yii::t('yii', 'Delete'),
					'aria-label'   => Yii::t('yii', 'Delete'),
					'data-confirm' => [
						'message' => Yii::t('yii', 'Are you sure you want to delete this item?'),
						'cancel'  => Yii::t('yii', 'Cancel'),
						'confirm' => Yii::t('yii', 'Ok'),
					],
					'data-method'  => 'post',
					'data-pjax'    => $this->dataPjax,
				], $this->buttonOptions);
				return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
			};
		}

		if (!isset($this->buttons['delete-customer'])) {
			$this->buttons['delete-customer'] = function ($url, $model, $key) {
				$options = array_merge([
					'title'        => Yii::t('yii', 'Delete'),
					'aria-label'   => Yii::t('yii', 'Delete'),
					'data-confirm' => [
						'message' => Yii::t('app.customer', 'Are you sure you want to delete this client?'),
						'cancel'  => Yii::t('app.customer', 'Cancel'),
						'confirm' => Yii::t('app.customer', 'Ok'),
					],
					'data-method'  => 'post',
					'data-pjax'    => $this->dataPjax,
				], $this->buttonOptions);
				return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
			};
		}

		parent::initDefaultButtons();
	}
}
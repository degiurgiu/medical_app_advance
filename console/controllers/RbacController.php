<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\User;
use Yii;
class RbacController extends Controller
{
	public function actionInit()
	{
		if (!$this->confirm("Are you sure? It will re-create permissions tree.")) {
			return self::EXIT_CODE_NORMAL;
		}
		$auth = Yii::$app->authManager;
		$auth->removeAll();

		$manageClients = $auth->createPermission('manageClients');
		$manageClients->description = 'Manage Clients';
		$auth->add($manageClients);

		$manageAdministrator = $auth->createPermission('Administrator');
		$manageAdministrator->description = 'Administrator';
		$auth->add($manageAdministrator);

		$moderator = $auth->createRole('clientManage');
		$moderator->description = 'Client Manager';
		$auth->add($moderator);
		$auth->addChild($moderator, $manageClients);

		$admin = $auth->createRole('admin');
		$admin->description = 'Administrator';
		$auth->add($admin);
		$auth->addChild($admin, $moderator);
		$auth->addChild($admin, $manageAdministrator);
		$auth->addChild($admin, $manageClients);
	}
	public function actionAssign($role, $username)
	{
		$user = User::find()->where(['username' => $username])->one();
		if (!$user) {
			throw new InvalidParamException("There is no user \"$username\".");
		}

		$auth = Yii::$app->authManager;
		$roleObject = $auth->getRole($role);
		if (!$roleObject) {
			throw new InvalidParamException("There is no role \"$role\".");
		}

		$auth->assign($roleObject, $user->id);
	}
}
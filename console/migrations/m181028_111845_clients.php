<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m181028_111845_clients
 */
class m181028_111845_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
	    $this->createTable('clients', [
		    'id' => Schema::TYPE_PK,
		    'first_name' => Schema::TYPE_STRING,
		    'last_name' => Schema::TYPE_STRING,
		    'phone' => Schema::TYPE_STRING,
		    'birthday_date'=>Schema::TYPE_STRING,
		    'email'=>Schema::TYPE_STRING,
		    'address'=>Schema::TYPE_STRING,
		    'uniqid'=>Schema::TYPE_STRING,
		    'user_id' => Schema::TYPE_INTEGER,
		    'updated_at' => Schema::TYPE_DATETIME,
		    'created_at' => Schema::TYPE_DATETIME,
	    ], $tableOptions);
	    $this->addForeignKey('user_id',  'clients', 'user_id',   'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('clients');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181028_111845_clients cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m181105_082110_defaultset
 */
class m181105_082110_defaultset extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
	    $table_auth_rule = Yii::$app->db->schema->getTableSchema('auth_rule');
	    if ($table_auth_rule === null) {
		    $this->createTable('auth_rule', [
			    'name' => $this->string()->notNull(),
			    'data' => $this->text()->notNull(),
			    'created_at' => $this->date(),
			    'updated_at' => $this->date(),
		    ], $tableOptions);
		    $this->addPrimaryKey('name', 'auth_rule', 'name');
	    }


	    $table_auth_item = Yii::$app->db->schema->getTableSchema('auth_item');
	    if ($table_auth_item === null) {
		    $this->createTable('auth_item', [
			    'name' => $this->string()->notNull(),
			    'type' => $this->integer()->notNull(),
			    'description' => $this->text(),
			    'rule_name' => $this->string(),
			    'data' => $this->text(),
			    'created_at' => $this->date(),
			    'updated_at' => $this->date(),

		    ], $tableOptions);

		    $this->addPrimaryKey('name', 'auth_item', 'name');
		    $this->addForeignKey('type',  'auth_item', 'rule_name',   'auth_rule', 'name', 'CASCADE', 'CASCADE');
	    }

	    $table_auth_item_child = Yii::$app->db->schema->getTableSchema('auth_item_child');
	    if ($table_auth_item_child === null) {
		    $this->createTable('auth_item_child', [
			    'parent' => $this->string()->notNull(),
			    'child' => $this->string()->notNull(),
		    ], $tableOptions);
		    $this->addPrimaryKey('parent_child', 'auth_item_child', ['parent','child']);
		    $this->addForeignKey('child_name',  'auth_item_child', 'child',   'auth_item', 'name', 'CASCADE', 'CASCADE');
		    $this->addForeignKey('parent_name',  'auth_item_child', 'parent',   'auth_item', 'name', 'CASCADE', 'CASCADE');
	    }


	    $table_auth_assignment= Yii::$app->db->schema->getTableSchema('auth_assignment');
	    if ($table_auth_assignment === null) {
		    $this->createTable('auth_assignment', [
			    'item_name' =>  $this->string()->notNull(),
			    'user_id'=> $this->integer()->notNull(),
			    'created_at' => $this->date(),
		    ], $tableOptions);
		    $this->addPrimaryKey('item_user', 'auth_assignment', ['item_name','user_id']);
		    $this->addForeignKey('auth_assignment_user_id_idx',  'auth_assignment', 'item_name',   'auth_item', 'name', 'CASCADE', 'CASCADE');
		    $this->addForeignKey('user_id_assignment',  'auth_assignment', 'user_id',   'user', 'id', 'CASCADE', 'CASCADE');
	    }
        $this->insert('user', array(
            'id' => '1',
            'username' => "admin",
            'auth_key'=>"",
            'password_hash' => "$2y$13$9eXIH.jT3cQMXRmmLrbiQuUFptYmxgg7wjS1mQ4MUQ/1pG4.jKuES",
            'email' => "admin@admin.com",
            'status' => "1",
            'created_at'=>date('Y-m-d h:m:s'),
            'updated_at'=>date('Y-m-d h:m:s'),
        ));
//	    $user= \app\models\User::find()->where(['id'=>'1'])->one();
//	    if(!$user) {
//
//	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181105_082110_defaultset cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_082110_defaultset cannot be reverted.\n";

        return false;
    }
    */
}

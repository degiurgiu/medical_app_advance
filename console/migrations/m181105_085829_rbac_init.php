<?php

use yii\db\Migration;

/**
 * Class m181105_085829_rbac_init
 */
class m181105_085829_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

	    $auth = Yii::$app->authManager;

	    $auth->removeAll();

        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $admin->createdAt= date('Y-m-d');
        $admin->updatedAt= date('Y-m-d');
        $auth->add($admin);

	    $moderator = $auth->createRole('clientManage');
	    $moderator->description = 'Client Manager';
	    $moderator->createdAt= date('Y-m-d');
	    $moderator->updatedAt= date('Y-m-d');
	    $auth->add($moderator);
        $auth->addChild($admin, $moderator);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    Yii::$app->authManager->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_085829_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}

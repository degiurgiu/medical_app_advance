<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181108_105936_user_personal_info
 */
class m181108_105936_user_personal_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {      $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
	    $this->createTable('user_personal_info', [
		    'id' => Schema::TYPE_PK,
		    'first_name' => Schema::TYPE_STRING,
		    'last_name' => Schema::TYPE_STRING,
		    'phone' => Schema::TYPE_STRING,
		    'speciality'=>Schema::TYPE_STRING,
		    'unique'=>Schema::TYPE_STRING,
		    'avatar'=>Schema::TYPE_STRING,
		    'about'=> $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'appointment_email'=>Schema::TYPE_BOOLEAN,
		    'user_id' => Schema::TYPE_INTEGER,
	    ], $tableOptions);
	    $this->addForeignKey('user_id_personal',  'user_personal_info', 'user_id',   'user', 'id', 'CASCADE', 'CASCADE');
	    $this->insert('user_personal_info', array(
		    'id' => '1',
		    'first_name' => "admin",
		    'last_name' => "admin",
		    'phone' => "",
		    'speciality' => "admin",
		    'about' => "admin",
		    'unique'=>"5cba482ec7a529.89775821",
		    'appointment_email'=>'1',
		    'user_id'=>'1'
	    ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_105936_user_personal_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_105936_user_personal_info cannot be reverted.\n";

        return false;
    }
    */
}

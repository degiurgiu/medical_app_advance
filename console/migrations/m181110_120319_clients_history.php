<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181110_120319_clients_history
 */
class m181110_120319_clients_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('clients_history', [
            'id' => Schema::TYPE_PK,
            'intervention' => Schema::TYPE_STRING,
            'observation' => Schema::TYPE_STRING,
            'date'=>Schema::TYPE_STRING,
            'client_id' => Schema::TYPE_INTEGER,
	        'updated_at' => Schema::TYPE_DATETIME,
	        'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        $this->addForeignKey('client_history_info',  'clients_history', 'client_id',   'clients', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181110_120319_clients_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181110_120319_clients_history cannot be reverted.\n";

        return false;
    }
    */
}

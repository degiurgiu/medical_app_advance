<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181120_094827_appoiment
 */
class m181120_094827_appointment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
	    $table_auth_assignment= Yii::$app->db->schema->getTableSchema('appointment');
	    if ($table_auth_assignment === null) {
		    $this->createTable('appointment', [
			    'id' => Schema::TYPE_PK,
			    'first_name' => Schema::TYPE_STRING,
			    'last_name' => Schema::TYPE_STRING,
			    'phone' => Schema::TYPE_STRING,
			    'email' => Schema::TYPE_STRING,
			    'appointment_date_time' => Schema::TYPE_DATETIME,
			    'user_appointment_ip'=>Schema::TYPE_STRING,
			    'privacy_policy_gdpr_agreement'=>Schema::TYPE_BOOLEAN,
			    'user_appointment_id' => Schema::TYPE_INTEGER,
			    'created_at' => Schema::TYPE_DATETIME,
		    ], $tableOptions);
		    $this->addForeignKey('user_appointment_id', 'appointment', 'user_appointment_id', 'user_personal_info', 'id', 'CASCADE', 'CASCADE');
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181120_094827_appoiment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181120_094827_appoiment cannot be reverted.\n";

        return false;
    }
    */
}

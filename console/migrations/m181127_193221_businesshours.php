<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181127_193221_businesshours
 */
class m181127_193221_businesshours extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $table_business_hours= Yii::$app->db->schema->getTableSchema('business_hours');
        if ($table_business_hours === null) {
            $this->createTable('business_hours', [
                'id' => Schema::TYPE_PK,
                'dow'=> Schema::TYPE_INTEGER,
                'start' => Schema::TYPE_TIME,
                'end' => Schema::TYPE_TIME,
                'user_business_id' => Schema::TYPE_INTEGER,
                'created_at' => Schema::TYPE_DATETIME,
            ], $tableOptions);
            $this->addForeignKey('user_business_id', 'business_hours', 'user_business_id', 'user', 'id', 'CASCADE', 'CASCADE');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_193221_businesshours cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_193221_businesshours cannot be reverted.\n";

        return false;
    }
    */
}

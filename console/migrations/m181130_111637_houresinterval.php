<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181130_111637_houresinterval
 */
class m181130_111637_houresinterval extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }
	    $table_interval_hours= Yii::$app->db->schema->getTableSchema('interval_hours');
	    if ($table_interval_hours === null) {
		    $this->createTable('interval_hours', [
			    'id' => Schema::TYPE_PK,
			    'interval_hours' => Schema::TYPE_STRING,
			    'user_interval_id' => Schema::TYPE_INTEGER,
			    'updated_at' => Schema::TYPE_DATETIME,
			    'created_at' => Schema::TYPE_DATETIME,
		    ], $tableOptions);
		    $this->addForeignKey('user_interval_id', 'interval_hours', 'user_interval_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181130_111637_houresinterval cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181130_111637_houresinterval cannot be reverted.\n";

        return false;
    }
    */
}

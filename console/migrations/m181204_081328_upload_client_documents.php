<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181204_081328_upload_client_documents
 */
class m181204_081328_upload_client_documents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }
	    $table_upload_clients_documents = Yii::$app->db->schema->getTableSchema('upload_clients_documents');
	    if ($table_upload_clients_documents === null) {
		    $this->createTable('upload_clients_documents', [
			    'id' => Schema::TYPE_PK,
			    'document_name' => Schema::TYPE_STRING,
			    'document' => Schema::TYPE_STRING,
			    'client_documents_id' => Schema::TYPE_INTEGER,
		    ], $tableOptions);
		    $this->addForeignKey('client_document_upload', 'upload_clients_documents', 'client_documents_id', 'clients', 'id', 'CASCADE', 'CASCADE');
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181204_081328_upload_client_documents cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181204_081328_upload_client_documents cannot be reverted.\n";

        return false;
    }
    */
}

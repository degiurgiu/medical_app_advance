<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181207_130113_init_page
 */
class m181207_130113_init_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;

	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }

	    $this->createTable('{{%page}}', [
		    'id' => $this->primaryKey(),
		    'url' => $this->string()->notNull(),
		    'title' => $this->string()->notNull(),
		    'content' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
		    'left_sidebar' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
		    'right_sidebar' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
		    'status' => $this->smallInteger()->notNull()->defaultValue(1),
		    'page_type' => $this->string()->notNull(),
		    'metaTitle' => $this->text()->notNull(),
		    'metaDescription' => $this->text(),
		    'metaKeywords' => $this->text(),
		    'slug' => Schema::TYPE_STRING.' NOT NULL',
		    'createdAt' =>  Schema::TYPE_DATETIME,
		    'updatedAt' =>  Schema::TYPE_DATETIME,
	    ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('{{%page}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181207_130113_init_cms cannot be reverted.\n";

        return false;
    }
    */
}

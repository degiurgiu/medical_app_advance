<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181228_090535_email_settings
 */
class m181228_090535_email_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('email_settings', [
            'id' => Schema::TYPE_PK,
            'appointment_message_clients' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'new_user_message' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'email_user_appointment_message' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'new_user_email' => Schema::TYPE_BOOLEAN,
            'appointment_email_clients' => Schema::TYPE_BOOLEAN,
            'user_id'=> Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->addForeignKey('user_email_setting',  'email_settings', 'user_id',   'user', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181228_090535_email_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181228_090535_email_settings cannot be reverted.\n";

        return false;
    }
    */
}

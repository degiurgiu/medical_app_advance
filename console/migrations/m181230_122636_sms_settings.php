<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m181230_122636_sms_settings
 */
class m181230_122636_sms_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('sms_settings', [
            'id' => Schema::TYPE_PK,
            'sms_text_appointment' => Schema::TYPE_TEXT,
            'sms_token' => Schema::TYPE_STRING,
            'sms_sid' => Schema::TYPE_STRING,
            'sms_phone_number' => Schema::TYPE_STRING,
            'sms_appointment' => Schema::TYPE_BOOLEAN,
            'user_id'=> Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->addForeignKey('user_sms_setting',  'sms_settings', 'user_id',   'user', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181230_122636_sms_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181230_122636_sms_settings cannot be reverted.\n";

        return false;
    }
    */
}

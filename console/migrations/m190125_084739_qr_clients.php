<?php

use yii\db\Migration;

use yii\db\Schema;
/**
 * Class m190125_084739_qr_clients
 */
class m190125_084739_qr_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }
	    $this->createTable('qr_clients', [
		    'id' => Schema::TYPE_PK,
		    'qr_client_path' => Schema::TYPE_STRING,
		    'client_id' => Schema::TYPE_INTEGER,
		    'updated_at' => Schema::TYPE_DATETIME,
		    'created_at' => Schema::TYPE_DATETIME,
	    ], $tableOptions);
	    $this->addForeignKey('qr_clients_path',  'qr_clients', 'client_id',   'clients', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190125_084739_qr_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190125_084739_qr_clients cannot be reverted.\n";

        return false;
    }
    */
}

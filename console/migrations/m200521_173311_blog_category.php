<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m200521_173311_blog_category
 */
class m200521_173311_blog_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('blog_category', [
            'id' => Schema::TYPE_PK,
            'category' => Schema::TYPE_STRING,
            'updated_at' => Schema::TYPE_DATETIME,
            'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200521_173311_blog_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200521_173311_blog_category cannot be reverted.\n";

        return false;
    }
    */
}

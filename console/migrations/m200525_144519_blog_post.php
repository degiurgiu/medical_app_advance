<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m200525_144519_blog_post
 */
class m200525_144519_blog_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('blog_post', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'content' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'blog_image' => Schema::TYPE_STRING,
            'author_id' => Schema::TYPE_INTEGER,
            'category_id' =>Schema::TYPE_INTEGER,
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'slug' => Schema::TYPE_STRING.' NOT NULL',
            'header_type' => Schema::TYPE_STRING,
            'blog_video_link' => Schema::TYPE_STRING,
            'updated_at' => Schema::TYPE_DATETIME,
            'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        $this->addForeignKey('blog_post_author_id',  'blog_post', 'author_id',   'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('blog_post_category_id',  'blog_post', 'category_id',   'blog_category', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200525_144519_blog_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200525_144519_blog_post cannot be reverted.\n";

        return false;
    }
    */
}

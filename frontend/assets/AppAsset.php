<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
	    'css/style.css',
        'css/animate.css',
'css/flaticon.css',
'css/jquery.datetimepicker.min.css',
'css/magnific-popup.css',
'css/owl.carousel.min.css',
'css/owl.theme.default.min.css',
    ];
    public $js = [
        'js/main.js',
        'js/custom.js',
        'js/menu.js',
//        'js/sticky.js',
        'js/jquery.ajaxchimp.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.appear.js',
        'js/jquery.validate.min.js',
        'js/jquery.stellar.min.js',
        'js/isotope.pkgd.min.js',
        'js/jquery.easing.js',
        'js/imagesloaded.pkgd.min.js',
        'js/jquery.scrollto.js',
//        'js/materialize.js',
        'js/modernizr.custom.js',
        'js/owl.carousel.min.js',
        'js/wow.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}

<?php

namespace frontend\controllers;

use common\models\AppointmentSearch;
use common\models\UserPersonalInfo;
use Yii;
use common\models\Appointment;
use common\models\EmailSettings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use common\models\Event;
use common\models\BusinessHours;
use common\models\IntervalHours;
use yii\filters\AccessControl;
use common\models\SmsSettings;
/**
 * AppointmentController implements the CRUD actions for Appointment model.
 */
class AppointmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'only' => ['update','view','delete'],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['admin','clientManage'],
			        ],
		        ],
		        'denyCallback' => function($rule, $action) {
			        return Yii::$app->response->redirect(['/site/login']);
		        },
	        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Appointment models.
     * @return mixed
     */
	public function actionIndex()
	{
		//var_dump(Appointment::getCheckTime());
		$users = UserPersonalInfo::find()
			->joinWith(['user' => function (ActiveQuery $query) {
				return $query
					->andWhere(['=', 'user.status', '1']);
			}])->all();
		return $this->render('index', [
			'users'=>$users
		]);
	}
	public function actionAppointment($id){
		$appointment = Appointment::find()->andWhere(['user_appointment_id' => $id])->all();
		$business_hours_object = BusinessHours::find()->andWhere(['user_business_id' =>$id])->all();
		$interval_houres = IntervalHours::find()->andWhere(['user_interval_id' => $id])->one();
		$hours_array = [];
		$business_hours = [];
		foreach ($business_hours_object as $hours){
			$hours_array['dow'] = [$hours->dow];
			$hours_array['start'] = $hours->start;
			$hours_array['end'] = $hours->end;
			$business_hours[] = $hours_array;
		}

		$events = array();
		foreach ($appointment as $appoint) {
			$event = new Event();
			$event->id = $appoint->id;
//			$event->title = $appoint->first_name . ' ' . $appoint->last_name;
			$event->title = 'occupied';
			$event->start = $appoint->appointment_date_time;
			$event->end =  date('Y-m-d H:i:s',strtotime('-'.$interval_houres['interval_hours'],strtotime($appoint->appointment_date_time)));
//			$event->durationEditable = true;
//			$event->allDay = false;
			$event->backgroundColor="red";
			$event->className = 'danger';
			$events[] = $event;

		}

		return $this->render('addappointment', [
			'appointment' => $appointment,
			'events' => $events,
			'userId'=>$id,
			'interval'=>$interval_houres['interval_hours'],
			'business_hours' => json_encode($business_hours),
		]);
	}

    /**
     * Displays a single Appointment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Appointment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date,$userid)
    {
        $model = new Appointment();
		$model->appointment_date_time =$date;
	    $model->user_appointment_id = $userid;
//	    $model->appointment_date = $date;
	    $model->user_appointment_ip = $this->get_ip_address();

        if ($model->load(Yii::$app->request->post()) ) {

//	        $model->appointment_date_time =  '.$model->appointment_time;
	        $model->created_at = date('Y-m-d h:m:s');
		        if ($model->save()) {
			        Yii::$app->session->setFlash('success', "Appointment added");
                }else {
			        Yii::$app->session->setFlash('error', "Appointment error.");
		        }
	         return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Appointment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Finds the Appointment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Appointment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Appointment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public static function template_substitution($template, $data) {
        $placeholders = array_keys($data);
        foreach ($placeholders as &$placeholder) {
            $placeholder = strtoupper("{{$placeholder}}");
        }
        return str_replace($placeholders, array_values($data), $template);
    }
//	function getRealIpAddr()
//	{
//		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
//		{
//			$ip=$_SERVER['HTTP_CLIENT_IP'];
//		}
//		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
//		{
//			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
//		}
//		else
//		{
//			$ip=$_SERVER['REMOTE_ADDR'];
//		}
//		return $ip;
//	}
	public function get_ip_address() {
		// check for shared internet/ISP IP
		if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
			return $_SERVER['HTTP_CLIENT_IP'];
		}

		// check for IPs passing through proxies
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			// check if multiple ips exist in var
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
				$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				foreach ($iplist as $ip) {
					if ($this->validate_ip($ip))
						return $ip;
				}
			} else {
				if ($this->validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_X_FORWARDED']))
			return $_SERVER['HTTP_X_FORWARDED'];
		if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
			return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
			return $_SERVER['HTTP_FORWARDED_FOR'];
		if (!empty($_SERVER['HTTP_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_FORWARDED']))
			return $_SERVER['HTTP_FORWARDED'];

		// return unreliable ip since all else failed
		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	 * Ensures an ip address is both a valid IP and does not fall within
	 * a private network range.
	 */
	public function validate_ip($ip) {
		if (strtolower($ip) === 'unknown')
			return false;

		// generate ipv4 network address
		$ip = ip2long($ip);

		// if the ip is set and not equivalent to 255.255.255.255
		if ($ip !== false && $ip !== -1) {
			// make sure to get unsigned long representation of ip
			// due to discrepancies between 32 and 64 bit OSes and
			// signed numbers (ints default to signed in PHP)
			$ip = sprintf('%u', $ip);
			// do private network range checking
			if ($ip >= 0 && $ip <= 50331647) return false;
			if ($ip >= 167772160 && $ip <= 184549375) return false;
			if ($ip >= 2130706432 && $ip <= 2147483647) return false;
			if ($ip >= 2851995648 && $ip <= 2852061183) return false;
			if ($ip >= 2886729728 && $ip <= 2887778303) return false;
			if ($ip >= 3221225984 && $ip <= 3221226239) return false;
			if ($ip >= 3232235520 && $ip <= 3232301055) return false;
			if ($ip >= 4294967040) return false;
		}
		return true;
	}
}

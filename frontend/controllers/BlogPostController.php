<?php

namespace frontend\controllers;


use Yii;
use common\models\BlogPost;
use common\models\BlogPostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * BlogPostController implements the CRUD actions for BlogPost model.
 */
class BlogPostController extends Controller
{

    /**
     * Lists all BlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = BlogPost::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }
    public function actionSlug($slug)
    {
        $model = BlogPost::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {

            return $this->render('blog', [
                'model' => $model,
            ]);
        }
        else {
            return $this->redirect('index');
        }
    }
    /**
     * Displays a single BlogPost model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $all_blogs = BlogPost::find()->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'all_blogs'=>$all_blogs,
        ]);
    }
    public function actionBlog($id)
    {
        $all_blogs = BlogPost::find()->all();
        return $this->render('blog', [
            'model' => $this->findModel($id),
            'all_blogs'=>$all_blogs,
        ]);
    }

    /**
     * Finds the BlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogPost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

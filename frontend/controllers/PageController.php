<?php

namespace frontend\controllers;

use Yii;
use common\models\Page;
use common\models\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actionIndex()
	{
		$searchModel = new PageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

	public function actionSlug($slug)
	{
        $model = Page::find()->where(['slug'=>$slug])->one();

        if (!is_null($model) && $model->status == '1') {
            if($model->page_type == 'full_content_page'){
                return $this->render('page', [
                    'model' => $model,
                ]);
            }elseif ($model->page_type == 'right_sidebar'){
                return $this->render('right_page', [
                    'model' => $model,
                ]);
            }
            elseif ($model->page_type == 'left_sidebar'){
                return $this->render('left_page', [
                    'model' => $model,
                ]);
            }
            elseif ($model->page_type == 'left_right_sidebar'){
                return $this->render('left_right_page', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('page_not_found', [
                'model' => $model,
            ]);
        }
	}
}

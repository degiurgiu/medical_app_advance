<?php

namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use common\models\ContactForm;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\AuthItem;
use common\models\EmailSettings;
use common\models\UserPersonalInfo;
use common\models\Appointment;
use common\models\BusinessHours;
use common\models\IntervalHours;
use common\models\Event;
use yii\db\ActiveQuery;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout',],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//    	\Yii::info(User::findByUsername('edward'),'my_category');
        return $this->render('index');
    }
    public function actionAppointment($id){
        $id_real= UserPersonalInfo::find()->select('id')->where(['unique'=>$id])->one()['id'];
        $appointment = Appointment::find()->andWhere(['user_appointment_id' => $id_real])->all();
        $business_hours_object = BusinessHours::find()->andWhere(['user_business_id' =>$id_real])->all();
        $interval_houres = IntervalHours::find()->andWhere(['user_interval_id' => $id_real])->one();
        $hours_array = [];
        $business_hours = [];
        foreach ($business_hours_object as $hours){
            $hours_array['dow'] = [$hours->dow];
            $hours_array['start'] = $hours->start;
            $hours_array['end'] = $hours->end;
            $business_hours[] = $hours_array;
        }

        $events = array();
        foreach ($appointment as $appoint) {
            $event = new Event();
            $event->id = $appoint->id;
//			$event->title = $appoint->first_name . ' ' . $appoint->last_name;
            $event->title = 'occupied';
            $event->start = $appoint->appointment_date_time;
            $event->end =  date('Y-m-d H:i:s',strtotime('-'.$interval_houres['interval_hours'],strtotime($appoint->appointment_date_time)));
//			$event->durationEditable = true;
//			$event->allDay = false;
            $event->backgroundColor="red";
            $event->className = 'danger';
            $events[] = $event;

        }

        return $this->render('appointment', [
            'appointment' => $appointment,
            'events' => $events,
            'userId'=>$id_real,
            'interval'=>$interval_houres['interval_hours'],
            'business_hours' => json_encode($business_hours),
        ]);
    }
    public function actionCreateAppointment($date,$userid)
    {
        $model = new Appointment();
        $model->appointment_date_time =$date;
        $model->user_appointment_id = $userid;
//	    $model->appointment_date = $date;
        $model->user_appointment_ip = $this->get_ip_address();


        if ($model->load(Yii::$app->request->post()) ) {

//	        $model->appointment_date_time =  '.$model->appointment_time;
            $model->created_at = date('Y-m-d h:m:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Appointment added");
            }else {
                Yii::$app->session->setFlash('error', "Appointment error.");
            }
            $id_unique= UserPersonalInfo::find()->select('unique')->where(['id'=>$userid])->one()['unique'];
            return $this->redirect(['appointment', 'id' => $id_unique]);
        }

        return $this->renderAjax('create_appointment', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Appointment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
//	        $model->appointment_date_time = ;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => \Yii::$app->user->identity->getId()]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public static function template_substitution($template, $data) {
        $placeholders = array_keys($data);
        foreach ($placeholders as &$placeholder) {
            $placeholder = strtoupper("{{$placeholder}}");
        }
        return str_replace($placeholders, array_values($data), $template);
    }
    public function get_ip_address() {
        // check for shared internet/ISP IP
        if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        // check for IPs passing through proxies
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // check if multiple ips exist in var
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    if ($this->validate_ip($ip))
                        return $ip;
                }
            } else {
                if ($this->validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];
        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];
        if (!empty($_SERVER['HTTP_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];

        // return unreliable ip since all else failed
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     */
    public function validate_ip($ip) {
        if (strtolower($ip) === 'unknown')
            return false;

        // generate ipv4 network address
        $ip = ip2long($ip);

        // if the ip is set and not equivalent to 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // make sure to get unsigned long representation of ip
            // due to discrepancies between 32 and 64 bit OSes and
            // signed numbers (ints default to signed in PHP)
            $ip = sprintf('%u', $ip);
            // do private network range checking
            if ($ip >= 0 && $ip <= 50331647) return false;
            if ($ip >= 167772160 && $ip <= 184549375) return false;
            if ($ip >= 2130706432 && $ip <= 2147483647) return false;
            if ($ip >= 2851995648 && $ip <= 2852061183) return false;
            if ($ip >= 2886729728 && $ip <= 2887778303) return false;
            if ($ip >= 3221225984 && $ip <= 3221226239) return false;
            if ($ip >= 3232235520 && $ip <= 3232301055) return false;
            if ($ip >= 4294967040) return false;
        }
        return true;
    }


    public function actionHome1()
    {

        return $this->render('home1');
    }
    public function actionHome2()
    {

        return $this->render('home2');
    }
    public function actionHome3()
    {

        return $this->render('home3');
    }
    public function actionHome4()
    {

        return $this->render('home4');
    }
    public function actionHome5()
    {

        return $this->render('home5');
    }
    public function actionHome6()
    {

        return $this->render('home6');
    }
    public function actionHome7()
    {

        return $this->render('home7');
    }
    public function actionHome8()
    {

        return $this->render('home8');
    }
    public function actionHome9()
    {

        return $this->render('home9');
    }
    public function actionHome10()
    {

        return $this->render('home10');
    }
    public function actionHome11()
    {

        return $this->render('home11');
    }
    public function actionHome12()
    {

        return $this->render('home12');
    }
    public function actionAllDoctors()
    {
        $users = UserPersonalInfo::find()
            ->joinWith(['user' => function (ActiveQuery $query) {
                return $query
                    ->andWhere(['=', 'user.status', '1']);
            }])->all();

        return $this->render('allDoctors', [
            'users'=>$users
        ]);

    }
    public function actionAllDepartments()
    {

        return $this->render('allDepartments');
    }
    public function actionAllServices()
    {

        return $this->render('allServices');
    }
    public function actionBlogListing()
    {

        return $this->render('blogListing');
    }
    public function actionContact1()
    {

        return $this->render('contact1');
    }
    public function actionContact2()
    {

        return $this->render('contact2');
    }
    public function actionDepartmentSingle()
    {

        return $this->render('departmentSingle');
    }
    public function actionDoctor1()
    {

        return $this->render('doctor1');
    }

    public function actionDoctor2($id)
    {
        $model = UserPersonalInfo::find()->where(['unique'=>$id])->one();
        return $this->render('doctor2', [
            'model'=>$model
        ]);
    }
    public function actionFaq()
    {

        return $this->render('faqs');
    }
    public function actionGallery()
    {

        return $this->render('gallery');
    }
    public function actionPriceing1()
    {

        return $this->render('priceing1');
    }
    public function actionPriceing2()
    {

        return $this->render('priceing2');
    }
    public function actionService1()
    {

        return $this->render('service1');
    }
    public function actionService2()
    {

        return $this->render('service2');
    }
    public function actionSinglePost()
    {

        return $this->render('singlePost');
    }
    public function actionTerms()
    {

        return $this->render('terms');
    }
    public function actionWhoWeAre()
    {

        return $this->render('whoWeAre');
    }



    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        //Array ( [_csrf-frontend] => 7bYPZilnVpF74y4qBVUEjUCyt_Ttc---EhdzuVQYvCGC_G5UGi1n9T7XbXpyM2zgN_TOxcBE3vNYUj2KHXX6dg== [ContactForm] => Array ( [name] => Dan Giurgiu
        // [email] => giurgiu_dan94@yahoo.com [subject] => test subject [body] => test [verifyCode] => sesxyo ) [contact-button] => )
	    $model = new ContactForm();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	        $request = Yii::$app->request->post()['ContactForm'];

	        $email = 'giurgiu_dan94@yahoo.com';
           $email_send= Yii::$app->HelperComponent->sendEmail($email, $request['email'], $request['subject'], $request['name'].' '.$request['body']);
		    if ($email_send) {
			    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
		    } else {
			    Yii::$app->session->setFlash('error', 'There was an error sending your message.');
		    }

		    return $this->refresh();
	    } else {
		    return $this->render('contact', [
			    'model' => $model,
		    ]);
	    };
    }
    public function actionSignup()
    {
        $model = new SignupForm();
        $authItems = AuthItem::find()->all();
        $email_settings = EmailSettings::find()->one();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if(isset($email_settings->new_user_email) && $email_settings->new_user_email == 1) {
                    $email ="giurgiu_dan94@yahoo.com";
                    $variables = array("first_name"=>$model->first_name,"last_name"=>$model->last_name,"email"=>$model->email,"username"=>$model->username,"password"=>$model->password,"role"=>implode(',',$model->permissions));
                    $message=Yii::$app->HelperComponent->template_substitution($email_settings->new_user_message,$variables);
                    Yii::$app->HelperComponent->sendEmail($email, $model->email, 'You have a new account at medical app', $message);
                    Yii::$app->session->setFlash('success', "New user created successfully");
                }
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'authItems'=>$authItems,
        ]);
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: edward
 * Date: 20.11.2018
 * Time: 15:42
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use common\models\UserPersonalInfo;

        Modal::begin([
	        'header'=>'<h4>Appointment</h4>',
	        'id'=>'create_appointment',
	        'size'=>'modal-lg',
        ]);
        echo "<div id='modalContentCreateAppointment'></div>";
        Modal::end();
    $user_personal_info = UserPersonalInfo::find()->where(['user_id' => $userId])->one();
	?>
    <h2> Appotment for <?php echo $user_personal_info['first_name'] .' '. $user_personal_info['last_name']; ?></h2>
<?= \common\widgets\FullCalendarCustom::widget(array(
	'id'=>'appointment_calendar',
	'contentHeight'=>650,
	'eventLimit'=>true,
	'header'=>[
		'center'=>'title',
		'left'=>'next today',
		'right'=>''
	],
	'options' => [

		'lang' => 'en',
		'weekends' => true,
		'constraint'=>['id'=>$userId,'duration'=>'15'],

	],
	'slotDuration'=>$interval,
	"dayClick"=>"function(date, jsEvent, view) {
        var constraint = $(this).parents(\"#appointment_calendar\").attr('constraint');
        if (jsEvent.target.classList.contains('fc-nonbusiness')) {
//        var test = '$userId';
//        console.log(test);
                return;
                  
            }
            else{
               var pars=JSON.parse(constraint);
             //  console.log(pars.id);
               //console.log(pars.duration);
                $.get('/appointment/create',{'date':date.format(),'userid':pars.id},function (data) {
                    $('#create_appointment').modal('show').find('#modalContentCreateAppointment').html(data);
        
                });
              
            }
     }",
	'businessHours'=> $business_hours,

	'events'=> $events,
));
	?>
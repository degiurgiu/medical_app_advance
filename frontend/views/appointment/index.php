<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appointments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php
        Modal::begin([
	        'header'=>'<h4>'.Yii::t('app','Appointment').'</h4>',
            'id'=>'create_appointment',
            'size'=>'modal-lg',
        ]);
        echo "<div id='modalContentCreateAppointment'></div>";
        Modal::end();
	?>
    <?php
        //var_dump($events);
		foreach ($users as $user){?>

        <div class="profile col-sm-12" style=" padding: 15px;">
            <div class="info-user col-sm-8">
            <div class="avatar-user col-sm-3">

                <img src="<?php if(isset($user->avatar)){'../../'.$user->avatar; } else { echo Url::base(true).'/uploads/user_default.jpg';} ?>" width="120px" height="120px" style="margin: 0 auto;
                border-radius: 50%;
                text-align: center;
                display: block;">
                <br/>
                <h4 style="margin-top: 20px;font-weight: bold; text-align: center"><span><?php echo $user->first_name; ?></span>
                    <span><?php echo $user->last_name; ?></span></h4>
            </div>
            <div class="col-sm-2" style="">
                <?php
                echo Html::a('Add Appointment', ['appointment','id' => $user->id], ['class' => 'btn btn-primary']);
                ?>
            </div>
                <div class="info-user col-sm-9">
                    <p><span>First Name: <?php echo $user->first_name; ?></span></p>
                    <p><span>Last Name: <?php echo $user->last_name; ?></span></p>
                    <p><span>Phone: <?php echo $user->phone; ?></span></p>
                    <p><span>About: <?php echo $user->about; ?></span></p>
                    <p><span>Email: <?php echo $user->user['email'] ?></span></p>

                </div>

            </div>
         </div>

		<?php } ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use common\models\UserPersonalInfo;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPost */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blog Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="blog-post-view">

</div>
<!-- BLOG PAGE CONTENT
============================================= -->
<div id="single-blog-page" class="wide-100 blog-page-section division">
    <div class="container">
        <div class="row">


            <!-- SINGLE POST -->
            <div class="col-lg-8">
                <div class="single-blog-post pr-30">


                    <!-- BLOG POST IMAGE -->
                    <?php if ($model->header_type == 'image') { ?>
                        <!-- BLOG POST IMAGE -->
                        <div class="blog-post-img mb-40">
                            <img class="img-fluid"
                                 src="<?php echo Yii::getAlias('@imagebackendurl') . '/' . $model->blog_image; ?>"
                                 width="950px" height="450px">
                        </div>
                    <?php } elseif ($model->header_type == 'image_video') { ?>
                        <!-- BLOG POST IMAGE -->
                        <div class="blog-post-img mb-40">
                            <div class="video-preview text-center">

                                <!-- Change the link HERE!!! -->
                                <a class="video-popup1" href="<?php echo $model->blog_video_link; ?>">

                                    <!-- Play Icon -->
                                    <div class="video-btn play-icon-blue">
                                        <div class="video-block-wrapper">
                                            <i class="fas fa-play"></i>
                                        </div>
                                    </div>

                                    <!-- Preview Image -->
                                    <img class="img-fluid"
                                         src="<?php echo Yii::getAlias('@imagebackendurl') . '/' . $model->blog_image; ?>"
                                         width="950px" height="450px">
                                </a>
                            </div>
                        </div>
                    <?php } elseif ($model->header_type == "video") { ?>
                        <div class="blog-post-img mb-40">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="730" height="450"
                                        src="<?php echo $model->blog_video_link; ?>"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- BLOG POST TEXT -->
                    <div class="sblog-post-txt">

                        <!-- Post Title -->
                        <h4 class="h4-lg steelblue-color"><?php $model->title; ?></h4>

                        <!-- Post Data -->
                        <?php
                        $user_info = $model->author->personalInfo;

                        $full_name = $user_info['first_name'] . ' ' . $user_info['last_name'];
                        $about = $user_info['about'];
                        ?>
                        <span>Posted <?php echo $model->created_at ?> by <span><?php echo $full_name; ?></span>
                            <br/>
                        <span>Category:
                            <?php
                                if(isset($model->category)) {
                                    echo  $model->category->category;
                                } else {
                                    echo 'Unknown';
                                }?>
                        </span>

                        <!-- Post Text -->
                        <?php echo $model->content; ?>



                        <!-- BLOG POST SHARE LINKS -->
                        <div class="post-share-links">

                            <!-- POST TAGS -->
                            <div class="post-tags-list">
                                <span><a href="#">Effective Treatment</a></span>
                                <span><a href="#">Research</a></span>
                                <span><a href="#">Diagnostic</a></span>
                            </div>

                            <!-- POST SHARE ICONS -->
                            <div class="post-share-list">
                                <ul class="share-social-icons clearfix">
                                    <li><a href="#" class="share-ico ico-like"><i class="far fa-thumbs-up"></i> Like</a></li>
                                    <li><a href="#" class="share-ico ico-facebook"><i class="fab fa-facebook-f"></i> Share</a></li>
                                    <li><a href="#" class="share-ico ico-twitter"><i class="fab fa-twitter"></i> Tweet</a></li>
                                    <li><a href="#" class="share-ico ico-google-plus"><i class="fab fa-pinterest-p"></i> Pin It</a></li>
                                    <li><a href="#" class="share-ico ico-google-plus"><i class="fab fa-google-plus-g"></i> Share</a></li>
                                </ul>
                            </div>

                        </div>	<!-- END BLOG POST SHARE -->


                    </div>	<!-- END BLOG POST TEXT -->


                    <!-- ABOUT POST AUTHOR -->
                    <div class="author-senoff">

                        <!-- Avatar -->
                        <img src="<?php if(isset($user_info->avatar)){ echo Yii::getAlias('@imagebackendurl') . '/' .$user_info->avatar; } else { echo Yii::getAlias('@imagebackendurl') . '/uploads/user_default.jpg';} ?>" alt="author-avatar">

                        <!-- Text -->
                        <div class="author-senoff-txt">

                            <!-- Title -->
                            <h5 class="h5-sm steelblue-color">Published by <?php echo $full_name ?></h5>

                            <?php if(!empty($about)){echo substr($about, 0, 100);} else { echo '<p>No Description</p>'; }?>

                        </div>

                    </div>	<!-- END ABOUT POST AUTHOR -->


                    <!-- RELATED POSTS -->
                    <div class="related-posts">

                        <!-- Title -->
                        <h5 class="h5-md steelblue-color">Other Posts</h5>


                        <div class="row">
                            <?php
                            if(!empty($all_blogs)) {
                                $count = 0;
                                foreach ($all_blogs as $blog) {
                                    if ($blog->status == 1) {
                                        if ($count !== 3 && $blog->id != $model->id) {
                                            ?>

                                            <!-- BLOG POST #1 -->
                                            <div class="col-md-6">
                                                <div class="blog-post">

                                                    <!-- BLOG POST IMAGE -->
                                                    <?php if ($blog->header_type == 'image') { ?>
                                                        <!-- BLOG POST IMAGE -->
                                                        <div class="blog-post-img">
                                                            <img class="img-fluid"
                                                                 src="<?php echo Yii::getAlias('@imagebackendurl') . '/' . $blog->blog_image; ?>"
                                                                 width="345px" height="258px">
                                                        </div>
                                                    <?php } elseif ($blog->header_type == 'image_video') { ?>
                                                        <!-- BLOG POST IMAGE -->
                                                        <div class="blog-post-img">
                                                            <div class="video-preview text-center">

                                                                <!-- Change the link HERE!!! -->
                                                                <a class="video-popup1" href="<?php echo $blog->blog_video_link; ?>">

                                                                    <!-- Play Icon -->
                                                                    <div class="video-btn play-icon-blue">
                                                                        <div class="video-block-wrapper">
                                                                            <i class="fas fa-play"></i>
                                                                        </div>
                                                                    </div>

                                                                    <!-- Preview Image -->
                                                                    <img class="img-fluid"
                                                                         src="<?php echo Yii::getAlias('@imagebackendurl') . '/' . $blog->blog_image; ?>"
                                                                         width="345px" height="258px">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php } elseif ($blog->header_type == "video") { ?>
                                                        <div class="blog-post-img">
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <iframe width="345px" height="258px"
                                                                        src="<?php echo $blog->blog_video_link; ?>"
                                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                        allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    <?php } ?>

                                                    <!-- BLOG POST TEXT -->
                                                    <div class="blog-post-txt">

                                                        <!-- Post Title -->
                                                        <h5 class="h5-sm steelblue-color"><a href="/blog-post/view/?id=<?php echo $blog->id; ?>"> <?php echo $blog->title ?> </a></h5>
                                                        <h5 class="h5-sm steelblue-color"><a href="/blog-post/view/?id=<?php echo $blog->id; ?>"> <?php echo $blog->title ?> </a></h5>
                                                        <?php
                                                        $user_info = $blog->author->personalInfo;
                                                        $full_name = $user_info['first_name'] . ' ' . $user_info['last_name'];
                                                        ?>
                                                        <!-- Post Data -->
                                                        <span><?php $blog->created_at ?> by </span> <span class="blue-color"><?php echo $full_name; ?></span>

                                                        <!-- Post Text -->
                                                        <?php echo substr($blog->content, 0, 50) ?>


                                                    </div>

                                                </div>
                                            </div>    <!-- END  BLOG POST #1 -->

                                            <?php
                                        }
                                        $count = $count + 1;
                                    }
                                }
                            }
                            ?>
                        </div>	<!-- End row -->

                    </div>	<!-- END RELATED POSTS -->

        </div>	<!-- End row -->
    </div>	 <!-- End container -->
</div>	<!-- END BLOG PAGE CONTENT -->




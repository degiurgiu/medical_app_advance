<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet" crossorigin="anonymous">

    <!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
    <!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body>
<!-- PRELOADER SPINNER
============================================= -->
<!--<div id="loader-wrapper">-->
<!--    <div id="loader"><div class="loader-inner"></div></div>-->
<!--</div>-->

<div id="page" class="page">
<?php $this->beginBody() ?>

<div class="frontend-wrap col-12 col-sm-12 col-md-12 col-lg-12">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        [
            "label" => "Home",
            "icon" => "th",
            "url" => "#",
            "items" => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Home1', 'url' => ['/site/home1']],
                ['label' => 'Home2', 'url' => ['/site/home2']],
                ['label' => 'Home3', 'url' => ['/site/home3']],
                ['label' => 'Home4', 'url' => ['/site/home4']],
                ['label' => 'Home5', 'url' => ['/site/home5']],
                ['label' => 'Home6', 'url' => ['/site/home6']],
                ['label' => 'Home7', 'url' => ['/site/home7']],
                ['label' => 'Home8', 'url' => ['/site/home8']],
                ['label' => 'Home9', 'url' => ['/site/home9']],
                ['label' => 'Home10', 'url' => ['/site/home10']],
                ['label' => 'Home11', 'url' => ['/site/home11']],
                ['label' => 'Home12', 'url' => ['/site/home12']],
            ],
        ],
        [
            "label" => "Need to be Build",
            "icon" => "th",
            "url" => "#",
            "items" => [
                ['label' => 'All Doctors', 'url' => ['/site/all-doctors']],
                ['label' => 'All Departments', 'url' => ['/site/all-departments']],
                ['label' => 'All Services', 'url' => ['/site/all-services']],
                ['label' => 'Blog Listing', 'url' => ['/site/blog-listing']],
                ['label' => 'Department Single', 'url' => ['/site/department-single']],
                ['label' => 'Doctor1', 'url' => ['/site/doctor1']],
                ['label' => 'Doctor2', 'url' => ['/site/doctor2']],
                ['label' => 'FAQ', 'url' => ['/site/faq']],
                ['label' => 'Gallery', 'url' => ['/site/gallery']],
                ['label' => 'Priceing1', 'url' => ['/site/priceing1']],
                ['label' => 'Priceing2', 'url' => ['/site/priceing2']],
                ['label' => 'Service1', 'url' => ['/site/service1']],
                ['label' => 'Service2', 'url' => ['/site/service2']],
                ['label' => 'Single Post', 'url' => ['/site/single-post']],
                ['label' => 'terms', 'url' => ['/site/terms']],
                ['label' => 'Who We Are', 'url' => ['/site/who-we-are']],
                ]
        ],
        ['label' => 'Blog', 'url' => ['/blog-post']],
        ['label' => 'About', 'url' => ['/site/about']],
        [
            "label" => "Contact",
            "icon" => "th",
            "url" => "#",
            "items" => [
                ['label' => 'Contact', 'url' => ['/site/contact']],
                ['label' => 'Contact 1', 'url' => ['/site/contact1']],
                ['label' => 'Contact 2', 'url' => ['/site/contact2']],
            ]
        ],
        ['label' => 'Appointment', 'url' => ['/appointment']],
    ];
    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    </header>
    <div id="breadcrumb" class="division">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="breadcrumb-holder">
                        <nav aria-label="breadcrumb">
                            <?php
                            echo Breadcrumbs::widget([
                                'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n",
                                'homeLink' => [
                                    'label' => Yii::t('yii', 'Home'),
                                    'url' => Yii::$app->homeUrl,
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]);
                            ?>
                        </nav>
                        <!-- Title -->
                        <h4 class="h4-sm steelblue-color"><?php echo $this->title; ?></h4>

                    </div>
                </div>
            </div>  <!-- End row -->
        </div>	<!-- End container -->
    </div>	<!-- END BREADCRUMB -->
        <?= Alert::widget() ?>

        <?= $content ?>
<!--    </div>-->
<!--</div>-->

<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; --><?//= Html::encode(Yii::$app->name) ?><!-- --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->
    <!-- FOOTER-1
   1============================================= -->
    <footer id="footer-1" class="wide-40 footer division">
        <div class="container">


            <!-- FOOTER CONTENT -->
            <div class="row">


                <!-- FOOTER INFO -->
                <div class="col-md-6 col-lg-3">
                    <div class="footer-info mb-40">

                        <!-- Footer Logo -->
                        <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 80  pixels) -->
                        <img src="<?php echo Url::base(true) ?>/images/footer-logo.png" width="180" height="40" alt="footer-logo">

                        <!-- Text -->
                        <p class="p-sm mt-20">Aliquam orci nullam tempor sapien gravida donec an enim ipsum porta
                            justo at velna auctor congue
                        </p>

                        <!-- Social Icons -->
                        <div class="footer-socials-links mt-20">
                            <ul class="foo-socials text-center clearfix">

                                <li><a href="#" class="ico-facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="ico-twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="ico-google-plus"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href="#" class="ico-tumblr"><i class="fab fa-tumblr"></i></a></li>

                                <!--
                                <li><a href="#" class="ico-behance"><i class="fab fa-behance"></i></a></li>
                                <li><a href="#" class="ico-dribbble"><i class="fab fa-dribbble"></i></a></li>
                                <li><a href="#" class="ico-instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="ico-linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#" class="ico-pinterest"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="ico-youtube"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#" class="ico-vk"><i class="fab fa-vk"></i></a></li>
                                <li><a href="#" class="ico-yelp"><i class="fab fa-yelp"></i></a></li>
                                <li><a href="#" class="ico-yahoo"><i class="fab fa-yahoo"></i></a></li>
                                -->

                            </ul>
                        </div>

                    </div>
                </div>


                <!-- FOOTER CONTACTS -->
                <div class="col-md-6 col-lg-3">
                    <div class="footer-box mb-40">

                        <!-- Title -->
                        <h5 class="h5-xs">Our Location</h5>

                        <!-- Address -->
                        <p>121 King Street, Melbourne,</p>
                        <p>Victoria 3000 Australia</p>

                        <!-- Email -->
                        <p class="foo-email mt-20">E: <a href="mailto:yourdomain@mail.com">hello@yourdomain.com</a></p>

                        <!-- Phone -->
                        <p>P: +12 9 8765 4321</p>

                    </div>
                </div>


                <!-- FOOTER WORKING HOURS -->
                <div class="col-md-6 col-lg-3">
                    <div class="footer-box mb-40">

                        <!-- Title -->
                        <h5 class="h5-xs">Working Time</h5>

                        <!-- Working Hours -->
                        <p class="p-sm">Mon - Wed - <span>9:00 AM - 7:00 PM</span></p>
                        <p class="p-sm">Thursday - <span>9:00 AM - 6:30 PM</span></p>
                        <p class="p-sm">Friday - <span>9:00 AM - 6:00 PM</span></p>
                        <p class="p-sm">Sat - Sun - <span>Closed</span></p>

                    </div>
                </div>


                <!-- FOOTER PHONE NUMBER -->
                <div class="col-md-6 col-lg-3">
                    <div class="footer-box mb-40">

                        <!-- Title -->
                        <h5 class="h5-xs">Emergency Cases</h5>

                        <!-- Footer List -->
                        <h5 class="h5-xl blue-color">1-800-123-4560</h5>

                        <!-- Text -->
                        <p class="p-sm mt-15">Aliquam orci nullam undo tempor sapien gravida donec enim ipsum porta
                            justo velna aucto magna
                        </p>

                    </div>
                </div>


            </div>	  <!-- END FOOTER CONTENT -->


            <!-- FOOTER COPYRIGHT -->
            <div class="bottom-footer">
                <div class="row">
                    <div class="col-md-12">
<!--                        <p class="footer-copyright">&copy; 2019 <span>MedService</span>. All Rights Reserved</p>-->
                    </div>
                </div>
            </div>


        </div>	   <!-- End container -->
    </footer>	<!-- END FOOTER-1 -->


</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

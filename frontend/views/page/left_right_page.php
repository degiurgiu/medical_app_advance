<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\page */

//use common\dosamigos\ckeditor\CKEditorInline;
use yii\helpers\ArrayHelper;


$this->title = $model->metaTitle;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->metaKeywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->metaDescription]);
?>
<div class="page-wrapper col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="sidebar-left col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <?php echo $model->left_sidebar; ?>
    </div>
    <div class="page-content col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <?php //CKEditorInline::begin(['preset' => 'full']);?>

        <?php echo $model->content; ?>

        <?php //CKEditorInline::end();?>

    </div>
    <div class="sidebar-right col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <?php echo $model->right_sidebar; ?>
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\HtmlPurifier;
/* @var $this yii\web\View */
/* @var $model common\models\Appointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appointment-form">

    <?php $form = ActiveForm::begin([
	    'id' => 'appointment-form',
	    'enableClientValidation' => true,
	    'options' => [
		    'validateOnBlur' => true,
		    'class' => 'form'
	    ],
        ]); ?>

	<?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	<?php echo $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

	<?php echo $form->field($model, 'phone')->textInput(['type' => 'number']) ?>

	<?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'privacy_policy_gdpr_agreement')->checkbox(['checked' => false, 'required' => true]);
	?>

	<?php echo $form->field($model, 'appointment_date_time')->hiddenInput()->label(false) ?>

<?php //echo $form->field($model, 'appointment_time')->dropDownList(\common\models\Appointment::getTime())?>

	<?php echo $form->field($model, 'user_appointment_id')->hiddenInput()->label(false) ?>


    <div class="form-group">
	    <?php echo Html::submitButton('Save', ['class' => 'btn btn-lime']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

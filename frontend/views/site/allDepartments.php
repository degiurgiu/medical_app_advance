<?php
/**
 * Created by PhpStorm.
 * User: Edward & Denisa
 * Date: 5/18/2020
 * Time: 12:30 PM
 */
use yii\helpers\Url;
?>
<!-- ABOUT-5
============================================= -->
<section id="about-5" class="pt-100 about-section division">
    <div class="container">
        <div class="row d-flex align-items-center">


            <!-- IMAGE BLOCK -->
            <div class="col-lg-6">
                <div class="about-img text-center wow fadeInUp" data-wow-delay="0.6s">
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/image-03.png" alt="about-image">
                </div>
            </div>


            <!-- TEXT BLOCK -->
            <div class="col-lg-6">
                <div class="txt-block pc-30 wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Section ID -->
                    <span class="section-id blue-color">Welcome to MedService</span>

                    <!-- Title -->
                    <h3 class="h3-md steelblue-color">Complete Medical Solutions in One Place</h3>

                    <!-- Text -->
                    <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor tempus feugiat
                        dolor lacinia cubilia curae integer congue leo metus, eu mollislorem primis in orci integer
                        metus mollis faucibus. An enim nullam tempor sapien gravida donec pretium and ipsum porta
                        justo integer at velna vitae auctor integer congue
                    </p>

                    <!-- Singnature -->
                    <div class="singnature mt-35">

                        <!-- Text -->
                        <p class="p-small mb-15">Randon Pexon, Head of Clinic</p>

                        <!-- Singnature Image -->
                        <!-- Recommended sizes for Retina Ready displays is 400x68px; -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/signature.png" width="200" height="34" alt="signature-image" />

                    </div>

                </div>
            </div>	<!-- END TEXT BLOCK -->


        </div>    <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END ABOUT-5 -->




<!-- SERVICES-7
============================================= -->
<section id="services-7" class="bg-lightgrey wide-70 servicess-section division">
    <div class="container">
        <div class="row">


            <!-- SERVICE BOXES -->
            <div class="col-lg-8">
                <div class="row">


                    <!-- SERVICE BOX #1 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.4s">
                            <a href="service-1.html">

                                <!-- Icon -->
                                <span class="flaticon-083-stethoscope blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Pediatrics</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #1 -->


                    <!-- SERVICE BOX #2 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.6s">
                            <a href="service-2.html">

                                <!-- Icon -->
                                <span class="flaticon-047-head blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Neurology</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #2 -->


                    <!-- SERVICE BOX #3 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.4s">
                            <a href="service-1.html">

                                <!-- Icon -->
                                <span class="flaticon-015-blood-donation-1 blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Haematology</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #3-->


                    <!-- SERVICE BOX #4 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.6s">
                            <a href="service-2.html">

                                <!-- Icon -->
                                <span class="flaticon-048-lungs blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">X-Ray Diagnostic</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #4 -->


                    <!-- SERVICE BOX #5 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.4s">
                            <a href="service-1.html">

                                <!-- Icon -->
                                <span class="flaticon-060-cardiogram-4 blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Cardiology</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #5 -->


                    <!-- SERVICE BOX #6 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.6s">
                            <a href="service-2.html">

                                <!-- Icon -->
                                <span class="flaticon-031-scanner blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">MRI Diagnostics</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #6 -->


                    <!-- SERVICE BOX #7 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.4s">
                            <a href="service-1.html">

                                <!-- Icon -->
                                <span class="flaticon-076-microscope blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Laboratory Services</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #7 -->


                    <!-- SERVICE BOX #8 -->
                    <div class="col-md-6">
                        <div class="sbox-7 icon-xs wow fadeInUp" data-wow-delay="0.6s">
                            <a href="service-2.html">

                                <!-- Icon -->
                                <span class="flaticon-068-ambulance-3 blue-color"></span>

                                <!-- Text -->
                                <div class="sbox-7-txt">

                                    <!-- Title -->
                                    <h5 class="h5-sm steelblue-color">Emergency Help</h5>

                                    <!-- Text -->
                                    <p class="p-sm">Porta semper lacus at cursus primis ultrice in ligula risus an
                                        auctor tempus feugiat dolor
                                    </p>

                                </div>

                            </a>
                        </div>
                    </div>  <!-- END SERVICE BOX #8 -->


                </div>
            </div>	<!-- END SERVICE BOXES -->


            <!-- INFO TABLE -->
            <div class="col-lg-4">
                <div class="services-7-table blue-table mb-30 wow fadeInUp" data-wow-delay="0.6s">

                    <!-- Title -->
                    <h4 class="h4-xs">Opening Hours:</h4>

                    <!-- Text -->
                    <p class="p-sm">Porta semper lacus cursus and feugiat primis ultrice ligula risus auctor
                        tempus feugiat and dolor lacinia cursus
                    </p>

                    <!-- Table -->
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Mon – Wed</td>
                            <td> - </td>
                            <td class="text-right">9:00 AM - 7:00 PM</td>
                        </tr>
                        <tr>
                            <td>Thursday</td>
                            <td> - </td>
                            <td class="text-right">9:00 AM - 6:30 PM</td>
                        </tr>
                        <tr>
                            <td>Friday</td>
                            <td> - </td>
                            <td class="text-right">9:00 AM - 6:00 PM</td>
                        </tr>
                        <tr class="last-tr">
                            <td>Sun - Sun</td>
                            <td>-</td>
                            <td class="text-right">CLOSED</td>
                        </tr>
                        </tbody>
                    </table>

                    <!-- Title -->
                    <h5 class="h5-sm">Need a personal health plan?</h5>

                    <!-- Text -->
                    <p class="p-sm">Porta semper lacus cursus, and feugiat primis ultrice ligula at risus auctor</p>

                </div>
            </div>	<!-- END INFO TABLE -->


        </div>    <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END SERVICES-7 -->




<!-- BANNER-5
============================================= -->
<section id="banner-5" class="pt-100 banner-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1 section-title">

                <!-- Title 	-->
                <h3 class="h3-md steelblue-color">Certified and Experienced Doctors</h3>

                <!-- Text -->
                <p>Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis libero at tempus,
                    blandit posuere ligula varius congue cursus porta feugiat
                </p>

            </div>
        </div>


        <!-- BANNER IMAGE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="banner-5-img wow fadeInUp" data-wow-delay="0.4s">
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/image-07.png" alt="banner-image" />
                </div>
            </div>
        </div>


    </div>	   <!-- End container -->
</section>	<!-- END BANNER-5 -->




<?php
/**
 * Created by PhpStorm.
 * User: Edward & Denisa
 * Date: 5/18/2020
 * Time: 11:41 AM
 */
use yii\helpers\Url;
?>

<!-- HERO-8
============================================= -->
<section id="hero-8" class="hero-section division">


    <!-- SLIDER -->
    <div class="slider blue-nav">
        <ul class="slides">


            <!-- SLIDE #1 -->
            <li id="slide-1">

                <!-- Background Image -->
                <img src="<?php echo Url::base(true) ?>/images/slider/slide-14.jpg" alt="slide-background">

                <!-- Image Caption -->
                <div class="caption d-flex align-items-center left-align">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-9 col-md-8 col-lg-7">
                                <div class="caption-txt">

                                    <!-- Title -->
                                    <h5 class="steelblue-color">Sculpt your body</h5>
                                    <h2 class="steelblue-color">Discover Confidence</h2>

                                    <!-- Text -->
                                    <p class="p-md">Feugiat primis ligula risus auctor egestas augue mauri viverra
                                        tortor in iaculis placerat eugiat mauris ipsum an auctor purus euismod pretium
                                        purus dolor impedit magna purus feugiat dolor impedit magna
                                    </p>

                                    <!-- Button -->
                                    <a href="about-us.html" class="btn btn-lime black-hover">More About Clinic</a>

                                </div>
                            </div>
                        </div>  <!-- End row -->
                    </div>  <!-- End container -->
                </div>	<!-- End Image Caption -->

            </li>	<!-- END SLIDE #1 -->


            <!-- SLIDE #2 -->
            <li id="slide-2">

                <!-- Background Image -->
                <img src="<?php echo Url::base(true) ?>/images/slider/slide-15.jpg" alt="slide-background">

                <!-- Image Caption -->
                <div class="caption d-flex align-items-center right-align">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-9 col-md-8 col-lg-7 offset-sm-3 offset-md-4 offset-lg-5">
                                <div class="caption-txt">

                                    <!-- Title -->
                                    <h5 class="steelblue-color">Reduce fat without surgery</h5>
                                    <h2 class="steelblue-color">Discover Your Beauty</h2>

                                    <!-- CONTENT BOX #1 -->
                                    <div class="box-list">
                                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                        <p class="p-md">Quaerat sodales sapien undo euismod purus</p>
                                    </div>

                                    <!-- CONTENT BOX #2 -->
                                    <div class="box-list mb-15">
                                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                        <p class="p-md">Nemo ipsam egestas volute turpis dolores quaerat sodales
                                            sapien undo pretium purus at feugiat dolor impedit
                                        </p>
                                    </div>

                                    <!-- Button -->
                                    <a href="all-services.html" class="btn btn-lime black-hover">Our Core Services</a>

                                </div>
                            </div>
                        </div>  <!-- End row -->
                    </div>  <!-- End container -->
                </div>	<!-- End Image Caption -->

            </li>	<!-- END SLIDE #2 -->


            <!-- SLIDE #3 -->
            <li id="slide-3">

                <!-- Background Image -->
                <img src="<?php echo Url::base(true) ?>/images/slider/slide-16.jpg" alt="slide-background">

                <!-- Image Caption -->
                <div class="caption d-flex align-items-center left-align">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-9 col-md-8 col-lg-7">
                                <div class="caption-txt">

                                    <!-- Title -->
                                    <h5 class="steelblue-color">Enhance Intimacy</h5>
                                    <h2 class="steelblue-color">Boost your self esteem</h2>

                                    <!-- CONTENT BOX #1 -->
                                    <div class="box-list">
                                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                        <p class="p-md">Quaerat sodales sapien undo euismod purus</p>
                                    </div>

                                    <!-- CONTENT BOX #2 -->
                                    <div class="box-list mb-15">
                                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                                        <p class="p-md">Nemo ipsam egestas volute turpis dolores quaerat sodales
                                            sapien undo pretium purus at feugiat dolor impedit
                                        </p>
                                    </div>

                                    <!-- Button -->
                                    <a href="all-doctors.html" class="btn btn-lime black-hover">Meet the Doctors</a>

                                </div>
                            </div>
                        </div>  <!-- End row -->
                    </div>  <!-- End container -->
                </div>	<!-- End Image Caption -->

            </li>	<!-- END SLIDE #3 -->

        </ul>
    </div>	<!-- END SLIDER -->


</section>	<!-- END HERO-8 -->




<!-- INFO-4
============================================= -->
<section id="info-4" class="wide-60 info-section division">
    <div class="container">
        <div class="row d-flex align-items-center">


            <!-- INFO IMAGE -->
            <div class="col-lg-6">
                <div class="info-4-img mb-40 text-center wow fadeInUp" data-wow-delay="0.6s">
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/chief_doctor_700x800.jpg" alt="info-image">
                </div>
            </div>


            <!-- INFO TEXT -->
            <div class="col-lg-6">
                <div class="txt-block mb-40 pc-30 wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Section ID -->
                    <span class="section-id lime-color">Welcome to MedService</span>

                    <!-- Title -->
                    <h3 class="h3-md steelblue-color">Clinic with Innovative Approach to Treatment</h3>

                    <!-- Text -->
                    <p>An enim nullam tempor sapien gravida donec pretium ipsum  porta justo integer at  odio
                        velna vitae auctor integer congue magna purus pretium ligula rutrum luctus ultrice aliquam
                        a augue suscipit
                    </p>

                    <!-- Text -->
                    <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor tempus feugiat
                        dolor lacinia cubilia curae integer congue leo metus, eu mollislorem primis in orci integer
                        metus mollis faucibus. An enim nullam tempor sapien gravida donec pretium and ipsum porta
                        justo integer at velna vitae auctor integer congue
                    </p>

                    <!-- Singnature -->
                    <div class="singnature mt-35">

                        <!-- Text -->
                        <p class="p-small mb-15">Randon Pexon, Head of Clinic</p>

                        <!-- Singnature Image -->
                        <!-- Recommended sizes for Retina Ready displays is 400x68px; -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/signature.png" width="200" height="34" alt="signature-image" />

                    </div>

                </div>
            </div>	<!-- END TEXT BLOCK -->


        </div>    <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END INFO-4 -->




<!-- SERVICES-4
============================================= -->
<section id="services-4" class="bg-lightgrey wide-60 services-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1 section-title">

                <!-- Section ID -->
                <span class="section-id lime-color">The Areas of Practice</span>

                <!-- Title 	-->
                <h3 class="h3-md steelblue-color">Cosmetic & Reconstructive Procedures</h3>

                <!-- Text -->
                <p>Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis libero at tempus,
                    blandit posuere ligula varius congue cursus porta feugiat
                </p>

            </div>
        </div>


        <div class="row">


            <!-- SERVICE BOX #1 -->
            <div class="col-md-6 col-lg-3">
                <div class="sbox-4 wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Image -->
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/breast_procedure_700x800.jpg" alt="content-image" />

                    <!-- Text -->
                    <div class="sbox-4-txt">
                        <h5 class="h5-md lime-color"><a href="service-1.html">Breast Procedures</a></h5>
                        <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor
                            at pretium feugiat dolor integer
                        </p>
                    </div>

                </div>
            </div>


            <!-- SERVICE BOX #2 -->
            <div class="col-md-6 col-lg-3">
                <div class="sbox-4 wow fadeInUp" data-wow-delay="0.6s">

                    <!-- Image -->
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/body_procedure_700x800.jpg" alt="content-image" />

                    <!-- Text -->
                    <div class="sbox-4-txt">
                        <h5 class="h5-md lime-color"><a href="service-1.html">Body Procedures</a></h5>
                        <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor
                            at pretium feugiat dolor integer
                        </p>
                    </div>

                </div>
            </div>


            <!-- SERVICE BOX #3 -->
            <div class="col-md-6 col-lg-3">
                <div class="sbox-4 wow fadeInUp" data-wow-delay="0.8s">

                    <!-- Image -->
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/face_procedure_700x800.jpg" alt="content-image" />

                    <!-- Text -->
                    <div class="sbox-4-txt">
                        <h5 class="h5-md lime-color"><a href="service-1.html">Face Procedures</a></h5>
                        <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor
                            at pretium feugiat dolor integer
                        </p>
                    </div>

                </div>
            </div>


            <!-- SERVICE BOX #4 -->
            <div class="col-md-6 col-lg-3">
                <div class="sbox-4 wow fadeInUp" data-wow-delay="1s">

                    <!-- Image -->
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/rec_procedure_700x800.jpg" alt="content-image" />

                    <!-- Text -->
                    <div class="sbox-4-txt">
                        <h5 class="h5-md lime-color"><a href="service-1.html">Reconstructive</a></h5>
                        <p>Porta semper lacus cursus, feugiat primis ultrice in ligula risus auctor
                            at pretium feugiat dolor integer
                        </p>
                    </div>

                </div>
            </div>


        </div>	   <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END SERVICES-4 -->




<!-- INFO-2
============================================= -->
<section id="info-2" class="wide-60 info-section division">
    <div class="container">
        <div class="row d-flex align-items-center">


            <!-- TEXT BLOCK -->
            <div class="col-lg-6">
                <div class="txt-block pc-30 mb-40 wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Section ID -->
                    <span class="section-id lime-color">Best Practices</span>

                    <!-- Title -->
                    <h3 class="h3-md steelblue-color">Restore Your Beauty with MedServices Clinic</h3>

                    <!-- Text -->
                    <p class="mb-30">An enim nullam tempor sapien gravida donec enim ipsum blandit
                        porta justo integer odio velna vitae auctor integer congue magna at pretium
                        purus pretium ligula rutrum itae laoreet augue posuere and curae integer
                        congue leo metus mollis primis and mauris
                    </p>

                    <!-- Options List -->
                    <div class="row">

                        <div class="col-xl-6">

                            <!-- Option #1 -->
                            <div class="box-list m-top-15">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">Nemo ipsam egestas volute and turpis dolores quaerat</p>
                            </div>

                            <!-- Option #2 -->
                            <div class="box-list">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">Magna luctus tempor</p>
                            </div>

                            <!-- Option #3 -->
                            <div class="box-list">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">An enim nullam tempor at pretium purus blandit</p>
                            </div>

                        </div>

                        <div class="col-xl-6">

                            <!-- Option #4 -->
                            <div class="box-list">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">Magna luctus tempor blandit a vitae suscipit mollis</p>
                            </div>

                            <!-- Option #5 -->
                            <div class="box-list m-top-15">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">Nemo ipsam egestas volute turpis dolores quaerat</p>
                            </div>

                            <!-- Option #6 -->
                            <div class="box-list">
                                <div class="box-list-icon lime-color"><i class="fas fa-angle-double-right"></i></div>
                                <p class="p-sm">An enim nullam tempor</p>
                            </div>

                        </div>

                    </div>	<!-- End Options List -->

                </div>
            </div>	<!-- END TEXT BLOCK -->


            <!-- IMAGE BLOCK -->
            <div class="col-lg-6">
                <div class="info-2-img wow fadeInUp" data-wow-delay="0.6s">
                    <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/image-10.png" alt="info-image">
                </div>
            </div>


        </div>    <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END INFO-2 -->




<!-- INFO-5
============================================= -->
<section id="info-5" class="bg-scroll wide-100 info-section division">
    <div class="container">
        <div class="row d-flex align-items-center">


            <!-- TEXT BLOCK -->
            <div class="col-md-8 col-lg-7 col-xl-6 offset-md-4 offset-lg-5 offset-xl-6">
                <div class="txt-block pc-30 white-color">

                    <!-- Section ID -->
                    <span class="section-id id-color">Highest Quality Care</span>

                    <!-- Title -->
                    <h3 class="h3-md">Solutions to Complex Medical Problems</h3>

                    <!-- CONTENT BOX #1 -->
                    <div class="box-list">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Maecenas gravida porttitor nunc, quis vehicula magna luctus tempor. Quisque vel laoreet
                            turpis urna augue, viverra a augue eget, dictum tempor diam pulvinar massa purus nulla
                        </p>
                    </div>

                    <!-- CONTENT BOX #2 -->
                    <div class="box-list">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales sapien undo pretium
                            purus feugiat dolor impedit
                        </p>
                    </div>

                    <!-- CONTENT BOX #3 -->
                    <div class="box-list">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales sapien undo pretium
                            purus feugiat dolor impedit magna purus pretium gravida donec ligula massa in faucibus
                        </p>
                    </div>

                    <!-- Button -->
                    <a href="all-doctors.html" class="btn btn-tra-white blue-hover mt-25">Meet The Doctors</a>

                </div>
            </div>	<!-- END TEXT BLOCK -->


        </div>    <!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END INFO-5 -->




<!-- DOCTORS-1
============================================= -->
<section id="doctors-1" class="wide-40 doctors-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1 section-title">

                <!-- Section ID -->
                <span class="section-id lime-color">Our Medical Specialists</span>

                <!-- Title 	-->
                <h3 class="h3-md steelblue-color">Our Medical Specialists</h3>

                <!-- Text -->
                <p>Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis libero at tempus,
                    blandit posuere ligula varius congue cursus porta feugiat
                </p>

            </div>
        </div>	 <!-- END SECTION TITLE -->


        <div class="row">


            <!-- DOCTOR #1 -->
            <div class="col-md-6 col-lg-3">
                <div class="doctor-1">

                    <!-- Doctor Photo -->
                    <div class="hover-overlay text-center">

                        <!-- Photo -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/doctor-1.jpg" alt="doctor-foto">
                        <div class="item-overlay"></div>

                        <!-- Profile Link -->
                        <div class="profile-link">
                            <a class="btn btn-sm btn-tra-white black-hover" href="doctor-1.html" title="">View More Info</a>
                        </div>

                    </div>

                    <!-- Doctor Meta -->
                    <div class="doctor-meta">

                        <h5 class="h5-sm steelblue-color">Jonathan Barnes D.M.</h5>
                        <span class="lime-color">Plastic Surgery, Cosmetic Surgery</span>

                        <p class="p-sm grey-color">Donec vel sapien augue integer turpis cursus porta, mauris sed
                            augue luctus magna dolor luctus ipsum neque
                        </p>

                    </div>

                </div>
            </div>	<!-- END DOCTOR #1 -->


            <!-- DOCTOR #2 -->
            <div class="col-md-6 col-lg-3">
                <div class="doctor-1">

                    <!-- Doctor Photo -->
                    <div class="hover-overlay text-center">

                        <!-- Photo -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/doctor-2.jpg" alt="doctor-foto">
                        <div class="item-overlay"></div>

                        <!-- Profile Link -->
                        <div class="profile-link">
                            <a class="btn btn-sm btn-tra-white black-hover" href="doctor-2.html" title="">View More Info</a>
                        </div>

                    </div>

                    <!-- Doctor Meta -->
                    <div class="doctor-meta">

                        <h5 class="h5-sm steelblue-color">Hannah Harper D.M.</h5>
                        <span class="lime-color">Plastic Surgery</span>

                        <p class="p-sm grey-color">Donec vel sapien augue integer turpis cursus porta, mauris sed
                            augue luctus magna dolor luctus ipsum neque
                        </p>

                    </div>

                </div>
            </div>	<!-- END DOCTOR #2 -->


            <!-- DOCTOR #3 -->
            <div class="col-md-6 col-lg-3">
                <div class="doctor-1">

                    <!-- Doctor Photo -->
                    <div class="hover-overlay text-center">

                        <!-- Photo -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/doctor-3.jpg" alt="doctor-foto">
                        <div class="item-overlay"></div>

                        <!-- Profile Link -->
                        <div class="profile-link">
                            <a class="btn btn-sm btn-tra-white black-hover" href="doctor-1.html" title="">View More Info</a>
                        </div>

                    </div>

                    <!-- Doctor Meta -->
                    <div class="doctor-meta">

                        <h5 class="h5-sm steelblue-color">Matthew Anderson D.M.</h5>
                        <span class="lime-color">Cosmetic Surgery</span>

                        <p class="p-sm grey-color">Donec vel sapien augue integer turpis cursus porta, mauris sed
                            augue luctus magna dolor luctus ipsum neque
                        </p>

                    </div>

                </div>
            </div>	<!-- END DOCTOR #3 -->


            <!-- DOCTOR #4 -->
            <div class="col-md-6 col-lg-3">
                <div class="doctor-1">

                    <!-- Doctor Photo -->
                    <div class="hover-overlay text-center">

                        <!-- Photo -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/doctor-4.jpg" alt="doctor-foto">
                        <div class="item-overlay"></div>

                        <!-- Profile Link -->
                        <div class="profile-link">
                            <a class="btn btn-sm btn-tra-white black-hover" href="doctor-2.html" title="">View More Info</a>
                        </div>

                    </div>

                    <!-- Doctor Meta -->
                    <div class="doctor-meta">

                        <h5 class="h5-sm steelblue-color">Megan Coleman D.M.</h5>
                        <span class="lime-color">Nurse</span>

                        <p class="p-sm grey-color">Donec vel sapien augue integer turpis cursus porta, mauris sed
                            augue luctus magna dolor luctus ipsum neque
                        </p>

                    </div>

                </div>
            </div>	<!-- END DOCTOR #4 -->


        </div>	    <!-- End row -->


        <!-- ALL DOCTORS BUTTON -->
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="all-doctors mb-40">
                    <a href="all-doctors.html" class="btn btn-lime black-hover">Meet All Doctors</a>
                </div>
            </div>
        </div>


    </div>	   <!-- End container -->
</section>	<!-- END DOCTORS-1 -->




<!-- TESTIMONIALS-2
============================================= -->
<section id="reviews-2" class="bg-lightgrey wide-100 reviews-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1 section-title">

                <!-- Section ID -->
                <span class="section-id lime-color">Our Testimonials</span>

                <!-- Title 	-->
                <h3 class="h3-md steelblue-color">What Our Patients Say</h3>

                <!-- Text -->
                <p>Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis libero at tempus,
                    blandit posuere ligula varius congue cursus porta feugiat
                </p>

            </div>
        </div>	 <!-- END SECTION TITLE -->


        <!-- TESTIMONIALS CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme reviews-holder">


                    <!-- TESTIMONIAL #1 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-1.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Etiam sapien sem at sagittis congue an augue massa varius egestas a suscipit
                                magna undo tempus aliquet porta vitae
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Scott Boxer</h5>
                                <span>Programmer</span>
                            </div>

                        </div>
                    </div>	<!--END  TESTIMONIAL #1 -->


                    <!-- TESTIMONIAL #2 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-2.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Mauris donec ociis magnisa a sapien etiam sapien congue augue egestas et ultrice
                                vitae purus diam integer congue magna ligula egestas
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Penelopa Peterson</h5>
                                <span>Project Manager</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #2 -->


                    <!-- TESTIMONIAL #3 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-3.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>At sagittis congue augue an egestas magna ipsum vitae purus ipsum primis undo cubilia
                                laoreet augue
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">M.Scanlon</h5>
                                <span>Photographer</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #3 -->


                    <!-- TESTIMONIAL #4 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-4.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Mauris donec ociis magnis sapien etiam sapien congue augue pretium ligula
                                a lectus aenean magna mauris
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Jeremy Kruse</h5>
                                <span>Graphic Designer</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #4 -->


                    <!-- TESTIMONIAL #5 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-5.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>An augue in cubilia laoreet magna suscipit egestas magna ipsum at purus ipsum
                                primis in augue ulta ligula egestas
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Evelyn Martinez</h5>
                                <span>Senior UX/UI Designer</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #5 -->


                    <!-- TESTIMONIAL #6 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-6.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>An augue cubilia laoreet undo magna at risus suscipit egestas magna an ipsum ligula
                                vitae and purus ipsum primis
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Dan Hodges</h5>
                                <span>Internet Surfer</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #6 -->


                    <!-- TESTIMONIAL #7 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-7.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Augue egestas volutpat egestas augue in cubilia laoreet magna suscipit luctus
                                and dolor blandit vitae
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Isabel M.</h5>
                                <span>SEO Manager</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #7 -->


                    <!-- TESTIMONIAL #8 -->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-8.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Augue egestas volutpat egestas augue in cubilia laoreet magna suscipit luctus
                                and dolor blandit vitae
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Alex Ross</h5>
                                <span>Patient</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #8 -->


                    <!-- TESTIMONIAL #9-->
                    <div class="review-2">
                        <div class="review-txt text-center">

                            <!-- Quote -->
                            <div class="quote"><img src="<?php echo Url::base(true) ?>/images/quote.png" alt="quote-img" /></div>

                            <!-- Author Avatar -->
                            <div class="testimonial-avatar">
                                <img src="<?php echo Url::base(true) ?>/images/review-author-9.jpg" alt="testimonial-avatar">
                            </div>

                            <!-- Testimonial Text -->
                            <p>Augue egestas volutpat egestas augue in cubilia laoreet magna suscipit luctus
                                magna dolor neque vitae
                            </p>

                            <!-- Testimonial Author -->
                            <div class="review-author">
                                <h5 class="h5-sm">Alisa Milano</h5>
                                <span>Housewife</span>
                            </div>

                        </div>
                    </div>	<!-- END TESTIMONIAL #9 -->


                </div>
            </div>
        </div>	<!-- END TESTIMONIALS CONTENT -->


    </div>	   <!-- End container -->
</section>	 <!-- END TESTIMONIALS-2 -->




<!-- VIDEO-2
============================================= -->
<section id="video-2" class="wide-60 video-section division">
    <div class="container">
        <div class="row d-flex align-items-center">


            <!-- VIDEO LINK -->
            <div class="col-lg-6">
                <div class="video-preview mb-40 text-center wow fadeInUp" data-wow-delay="0.6s">

                    <!-- Change the link HERE!!! -->
                    <a class="video-popup1" href="https://www.youtube.com/embed/SZEflIVnhH8">

                        <!-- Play Icon -->
                        <div class="video-btn play-icon-lime">
                            <div class="video-block-wrapper">
                                <i class="fas fa-play"></i>
                            </div>
                        </div>

                        <!-- Preview -->
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/video-3.png" alt="video-photo" />

                    </a>

                </div>
            </div>	<!-- END VIDEO LINK -->


            <!-- VIDEO TEXT -->
            <div class="col-lg-6">
                <div class="txt-block pc-30 mb-40 wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Section ID -->
                    <span class="section-id lime-color">Modern Medicine</span>

                    <!-- Title -->
                    <h3 class="h3-md steelblue-color">Better Technologies for Better Healthcare</h3>

                    <!-- CONTENT BOX #1 -->
                    <div class="box-list m-top-15">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales sapien undo pretium
                            purus feugiat dolor impedit
                        </p>
                    </div>

                    <!-- CONTENT BOX #2 -->
                    <div class="box-list">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Gravida quis vehicula magna luctus tempor quisque vel laoreet turpis urna augue,
                            viverra a augue eget dictum
                        </p>
                    </div>

                    <!-- CONTENT BOX #3 -->
                    <div class="box-list">
                        <div class="box-list-icon"><i class="fas fa-genderless"></i></div>
                        <p>Nemo ipsam egestas volute turpis dolores ut aliquam quaerat sodales sapien undo pretium
                            purus feugiat dolor impedit
                        </p>
                    </div>

                </div>
            </div>


        </div>	    <!-- End row -->
    </div>	    <!-- End container -->
</section>	 <!-- END VIDEO-2 -->




<!-- BANNER-7
============================================= -->
<section id="banner-7" class="bg-fixed banner-section division">
    <div class="container white-color">
        <div class="row d-flex align-items-center">


            <!-- BANNER TEXT -->
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="banner-txt wow fadeInUp" data-wow-delay="0.4s">

                    <!-- Title  -->
                    <h2 class="h2-xs">Highest Quality Medical Treatment</h2>

                    <!-- Text -->
                    <p>Egestas magna egestas magna ipsum vitae purus ipsum primis in cubilia laoreet augue
                        egestas suscipit lectus mauris a lectus laoreet gestas neque undo luctus feugiat augue
                        suscipit
                    </p>

                    <!-- Button -->
                    <a href="#" class="btn btn-lime tra-white-hover mt-20">Free Consultation</a>

                </div>
            </div>	<!-- END BANNER TEXT -->


        </div>      <!-- End row -->
    </div>	    <!-- End container -->
</section>	<!-- END BANNER-7 -->




<!-- BLOG-1
============================================= -->
<section id="blog-1" class="wide-60 blog-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1 section-title">

                <!-- Section ID -->
                <span class="section-id lime-color">From the Blog</span>

                <!-- Title 	-->
                <h3 class="h3-md steelblue-color">Our Stories, Tips & Latest News</h3>

                <!-- Text -->
                <p>Aliquam a augue suscipit, luctus neque purus ipsum neque dolor primis libero at tempus,
                    blandit posuere ligula varius congue cursus porta feugiat
                </p>

            </div>
        </div>


        <!-- BLOG POSTS HOLDER -->
        <div class="row">


            <!-- BLOG POST #1 -->
            <div class="col-lg-4">
                <div class="blog-post wow fadeInUp" data-wow-delay="0.4s">

                    <!-- BLOG POST IMAGE -->
                    <div class="blog-post-img">
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/blog/post-1-img.jpg" alt="blog-post-image" />
                    </div>

                    <!-- BLOG POST TITLE -->
                    <div class="blog-post-txt">

                        <!-- Post Title -->
                        <h5 class="h5-sm steelblue-color"><a href="single-post.html">5 Benefits Of Integrative Medicine</a></h5>

                        <!-- Post Data -->
                        <span>May 03, 2019 by <span class="lime-color">Dr.Jeremy Smith</span></span>

                        <!-- Post Text -->
                        <p>Quaerat neque purus ipsum neque dolor primis libero tempus impedit tempor blandit sapien at
                            gravida donec ipsum, at porta justo...
                        </p>

                    </div>

                </div>
            </div>	<!-- END  BLOG POST #1 -->


            <!-- BLOG POST #2 -->
            <div class="col-lg-4">
                <div class="blog-post wow fadeInUp" data-wow-delay="0.6s">

                    <!-- BLOG POST IMAGE -->
                    <div class="blog-post-img">
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/blog/post-2-img.jpg" alt="blog-post-image" />
                    </div>

                    <!-- BLOG POST TEXT -->
                    <div class="blog-post-txt">

                        <!-- Post Title -->
                        <h5 class="h5-sm steelblue-color"><a href="single-post.html">Your Health Is In Your Hands</a></h5>

                        <!-- Post Data -->
                        <span>Apr 28, 2019 by <span class="lime-color">Dr.Jonathan Barnes</span></span>

                        <!-- Post Text -->
                        <p>Quaerat neque purus ipsum neque dolor primis libero tempus impedit tempor blandit sapien at
                            gravida donec ipsum, at porta justo...
                        </p>

                    </div>

                </div>
            </div>	<!-- END  BLOG POST #2 -->


            <!-- BLOG POST #3 -->
            <div class="col-lg-4">
                <div class="blog-post wow fadeInUp" data-wow-delay="0.8s">

                    <!-- BLOG POST IMAGE -->
                    <div class="blog-post-img">
                        <img class="img-fluid" src="<?php echo Url::base(true) ?>/images/blog/post-3-img.jpg" alt="blog-post-image" />
                    </div>

                    <!-- BLOG POST TEXT -->
                    <div class="blog-post-txt">

                        <!-- Post Title -->
                        <h5 class="h5-sm steelblue-color"><a href="single-post.html">How Weather Impacts Your Health</a></h5>

                        <!-- Post Data -->
                        <span>Apr 17, 2019 by <span class="lime-color">Dr.Megan Coleman</span></span>

                        <!-- Post Text -->
                        <p>Quaerat neque purus ipsum neque dolor primis libero tempus impedit tempor blandit sapien at
                            gravida donec ipsum, at porta justo...
                        </p>

                    </div>

                </div>
            </div>	<!-- END  BLOG POST #3 -->


        </div>	<!-- END BLOG POSTS HOLDER -->


        <!-- ALL POSTS BUTTON -->
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="all-posts-btn mb-40 wow fadeInUp" data-wow-delay="1s">
                    <a href="blog-listing.html" class="btn btn-lime black-hover">Read More Posts</a>
                </div>
            </div>
        </div>


    </div>	   <!-- End container -->
</section>	<!-- END BLOG-1 -->




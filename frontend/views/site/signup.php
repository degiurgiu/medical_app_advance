<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<div class="site-signup">


    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'enableClientValidation' => true,
                'options'=>[
                        'enctype'=>'multipart/form-data',
                        'validateOnBlur' => true,
                        'class' => 'form'
                ]
            ]);?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => 'true']);  ?>

                <?= $form->field($model, 'email'); ?>

                <?= $form->field($model, 'password')->passwordInput(); ?>
            <?php $authItemsArray = ArrayHelper::map($authItems,'name','name'); ?>
            <?= $form->field($model, 'permissions')->checkboxList($authItemsArray); ?>

	        <?= $form->field($model, 'first_name')->textInput();  ?>
	        <?= $form->field($model, 'last_name')->textInput();  ?>
	        <?= $form->field($model, 'phone')->textInput();  ?>
	        <?= $form->field($model, 'speciality')->textInput();  ?>
	        <?= $form->field($model, 'avatar')->fileInput();  ?>
	        <?= $form->field($model, 'created_at')->hiddenInput(['value'=>date('d-m-y')])->label(false); ?>
	        <?= $form->field($model, 'updated_at')->hiddenInput(['value'=>date('d-m-y')])->label(false); ?>

	        <?= $form->field($model, 'about')->textarea();  ?>


                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
                </div>

            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
